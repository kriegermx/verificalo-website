#README

This document contains the basic documentation for the project.

# Requirements
All the following requirements must be installed in your host/dev computer and must be accessible from a terminal/command line.

**You don't have to install VirtualBox independently, it will be installed together with vagrant if your choose the given option during the installation process.**

+ [Git](https://git-scm.com/downloads) (2.11.0)

+ [Maven](https://maven.apache.org/download.cgi) (3.3.9)

+ [Vagrant](https://www.vagrantup.com/downloads.html) (1.9.1)

This project has been proven to work with the versions specified above

# Variables

The following variables are used as reference along this guide:

+ PROJECT_DIR: the directory that contains all the project

# Getting the project

The repository for the project is hosted in [Bitbucket](https://bitbucket.org/kriegermx/verificalo-website).

To get the code for the project run from a terminal the following command.

``` 
git clone https://bitbucket.org/kriegermx/verificalo-website.git
git checkout dev
```

# VM for the development environment

## VM Components

+ **Programming language:** Oracle's Java SE Development Kit 8u111 [download](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

+ **App Server:** Wildfly 10.1.0.Final [download](http://wildfly.org/downloads/)

+ **Non-SQL database:** Apache Cassandra 3.9 [download](http://cassandra.apache.org/download/)

## Initializing the VM
To **initialize** the VM run the following commands. This may take several minute the first time, because the VM will configure all the environment.
```
cd $PROJECT_DIR/vm
vagrant up
```
## Configuring the VM ##
If it is the __first__ time the machine is run, you have to execute the installation scripts. To do so, connect to the virtual machine using the command.
**You only have to this if this is the first time that you are running the machine** 
```
cd $PROJECT_DIR/vm
vagrant ssh
```

Once you've been logged in to the VM, execute the following commands. You'll be asked to press enter during the installation proccess, do so.
```
sudo tr -d '\r' < /shared/provisioning/scripts/bootstrap.sh > /shared/provisioning/scripts/bootstrap_encoded.sh
sudo chmod +x /shared/provisioning/scripts/bootstrap_encoded.sh
sudo /shared/provisioning/scripts/bootstrap_encoded.sh
```

## Testing the VM ##

If the VM has started correclty, you should be able to navigate to the Wildfly console in the following URL:

[http://localhost:9191](http://localhost:9191)

You should see the following console

![wildfly console](doc/img/wildfly_console.png)

The credentials to log into the server console are:

+ **user**: _ubuntu_

+ **password**: _server_pa55_

## Shutting down the VM
To **stop** the VM run
```
cd $PROJECT_DIR/vm
vagrant halt
```

# Deploying webapp
Make sure the VM is up and running, then execute the following commands
```
cd $PROJECT_DIR
mvn wildfly:deploy
```
If everything goes well, you should see the following message:

![maven deploy success image](doc/img/mvn_deploy_success.png)

Then you should be able to access the webapp from the following URL:

[http://localhost:9090/verificalo-website/](http://localhost:9090/verificalo-website/)

You can have access to the server log from the path: _vm/shared/logs/wildfly_

# Production

Inicializar el servidor y hacer deploy (no hace pull)
```
sudo /opt/starter.sh
```
Detener el servidor
```
sudo /opt/stoper.sh
```

# Architecture

## Web Elements
All web elements should be located in the directory [src/main/webapp](src/main/webapp)

## Java elements
All **java sources** must be placed in the directory [src/main/java](src/main/java)

The **package** for the project is: _mx.krieger.verificalo.website_

All the **test classes** must be located in [src/test/java](src/test/java)

The java project is composed by the following subpackages:

+ **model**: All the classes that represent entities from the database

+ **handlers**: Reusable classes that allow interaction between action classes and services and the model

+ **actions**: All the classes that represent web actions mapped by struts

* **wrappers**: All the classes that represents elements to be exchanged between the front end and the back end

+ **services**: All theh classes that contain API's to be exposed as restful web services

+ **utils**: Classes that represent utilities

## Struts elements

All the **struts properties and configurations** must be placed in [src/main/resources](src/main/resources)

# Contact
If you have any questions, get in touch with:

+ Juanjo: [juanjo@krieger.mx](juanjo@krieger.mx)

# Load Verificenters
Go to http://localhost:9090/servlets/verificenterLoader

# Git credentials
user: verificalo@krieger.mx
password: Ver1f1c4l0CDMX

# VM credentials
446033946

# Troubleshooting

## VM missing requirements
Run `vagrant plugin install vagrant-vbguest`
