package mx.krieger.verificalo.website.apis.clients;

import mx.krieger.verificalo.website.apis.clients.GetOwnershipTaxDebts;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by me on 26/12/16.
 */
public class GetOwnershipTaxDebtsTest {
    @Test
    public void getResponseOk() throws Exception {

        String validPlate="192UMM";
        GetOwnershipTaxDebts.Response expected = new GetOwnershipTaxDebts.Response("200", "vehicles#resourcesItem", "\"7afFDge4gC7DCCFPNpCublnlLbs/8PnVX_vA-T4Q5406LaV1WIiM5JY\"", validPlate, "1", "2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017");
        GetOwnershipTaxDebts.Response response = new GetOwnershipTaxDebts().getResponse(validPlate);

        System.out.println(expected.toString());
        System.out.println(response.toString());

        assertEquals(expected, response);

    }

    @Test
    public void getResponsePlateNotFound() throws Exception {

        String invalidPlate="LYN3068";
        GetOwnershipTaxDebts.Response expected = new GetOwnershipTaxDebts.Response("200", "vehicles#resourcesItem", "\"7afFDge4gC7DCCFPNpCublnlLbs/gMDUbzjX0ktt_HKUpls3rPvZ4c8\"", invalidPlate, "0", null);
        GetOwnershipTaxDebts.Response response = new GetOwnershipTaxDebts().getResponse(invalidPlate);

        System.out.println(expected.toString());
        System.out.println(response.toString());

        assertEquals(expected, response);

    }

}