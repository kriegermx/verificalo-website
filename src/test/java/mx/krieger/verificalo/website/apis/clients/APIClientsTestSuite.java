package mx.krieger.verificalo.website.apis.clients;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by me on 28/12/16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({APICallerTest.class, GetEnvironmentVerificationsTest.class, GetOwnershipTaxDebtsTest.class,
        GetTrafficTicketsTest.class})
public class APIClientsTestSuite {
}
