package mx.krieger.verificalo.website.apis.clients;

import mx.krieger.verificalo.website.utils.VerificaloLogger;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by me on 26/12/16.
 */
public class GetEnvironmentVerificationsTest {
    @Test
    public void getResponseOk() throws Exception {

        VerificaloLogger.getLogger().info("GetEnvironmentVerificationsTest.getResponseOk");
        String validPlateNumber = "192UMM";

        ArrayList<GetEnvironmentVerifications.VerificationItem> verificationList = new
                ArrayList<GetEnvironmentVerifications.VerificationItem>();
        verificationList.add(new GetEnvironmentVerifications.VerificationItem("DOBLE CERO", "2008/12/01", "NO " +
                "APLICA", "GASOLINA", "3VWXW21J67M501562", "VW", "2006/11/29", "2007", "NO", "17:57:00",
                "BEETLE_GP_2.5", validPlateNumber, "9041", "0", "4", "9806506"));
        GetEnvironmentVerifications.Response expected = new GetEnvironmentVerifications.Response("200",
                "vehicles#resourcesItem", "\"7afFDge4gC7DCCFPNpCublnlLbs/ikYGCGxODTEZ3orfG8-1xjkdc84\"",
                verificationList);

        GetEnvironmentVerifications.Response response = new GetEnvironmentVerifications().getResponse(validPlateNumber);
        VerificaloLogger.getLogger().info("Response gotten: " + response.toString());

        System.out.println(expected.toString());
        System.out.println(response.toString());

        assertEquals(expected, response);
    }

    @Test
    public void getResponsePlateNotFound() throws Exception {

        VerificaloLogger.getLogger().info("GetEnvironmentVerificationsTest.getResponsePlateNotFound");

        String invalidPlate = "LYN3068";

        ArrayList<GetEnvironmentVerifications.VerificationItem> verificationList = new
                ArrayList<GetEnvironmentVerifications.VerificationItem>();
        verificationList.add(new GetEnvironmentVerifications.VerificationItem(null, null, null, null,
        null, null, null, null, null, null,
                null, invalidPlate, null, null, null, null));
        GetEnvironmentVerifications.Response expected = new GetEnvironmentVerifications.Response("404",
                "vehicles#resourcesItem", "\"7afFDge4gC7DCCFPNpCublnlLbs/YKXI2NWD_jt9kLRxGhZHaB5IhMQ\"",
                verificationList);

        GetEnvironmentVerifications.Response response = new GetEnvironmentVerifications().getResponse(invalidPlate);
        VerificaloLogger.getLogger().info("Response gotten: " + response.toString());

        System.out.println(expected.toString());
        System.out.println(response.toString());

        assertEquals(expected, response);
    }


}