package mx.krieger.verificalo.website.apis.clients;

import mx.krieger.verificalo.website.apis.clients.GetTrafficTickets;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by me on 26/12/16.
 */
public class GetTrafficTicketsTest {
    @Test
    public void getResponseOk() throws Exception {
        String validPlate="192UMM";

        ArrayList<GetTrafficTickets.TicketElement> ticketList= new ArrayList<GetTrafficTickets.TicketElement>();
        ticketList.add(new GetTrafficTickets.TicketElement("04103165932", "5", "III", "57.46", "5", "2010-05-28", "POR NO OBEDECER LOS SEÑALAMIENTOS DE TRÁNSITO Y LAS INDICACIONES DE LOS AGENTES O PERSONAL DE APOYO VIAL"));
        ticketList.add(new GetTrafficTickets.TicketElement("03016872289", "5", "V", "57.46", "5", "2010-06-23", "POR NO RESPETAR LOS LÍMITES DE VELOCIDAD ESTABLECIDOS EN VÍAS PRIMARIAS, EN CASO DE NO HABER SEÑALAMIENTO   LA VELOCIDAD MÁXIMA SERÁ DE 70 KILÓMETROS POR HORA"));
        GetTrafficTickets.Response expected = new GetTrafficTickets.Response("200", "vehicles#resourcesItem", "\"7afFDge4gC7DCCFPNpCublnlLbs/9AcsaaR0BAPTPKOIxcL9OIX3qoI\"", validPlate, ticketList, "1");
        GetTrafficTickets.Response response = new GetTrafficTickets().getResponse(validPlate);

        System.out.println(expected.toString());
        System.out.println(response.toString());

        assertEquals(expected, response);
    }

    @Test
    public void getResponsePlateNotFound() throws Exception {
        String invalidPlate="LYN3068";

        GetTrafficTickets.Response expected = new GetTrafficTickets.Response("200", "vehicles#resourcesItem", "\"7afFDge4gC7DCCFPNpCublnlLbs/Iy0ooznvLtU8T8CZC0AAdU47d8E\"", invalidPlate, null, "0");
        GetTrafficTickets.Response response = new GetTrafficTickets().getResponse(invalidPlate);

        System.out.println(expected.toString());
        System.out.println(response.toString());

        assertEquals(expected, response);
    }

}