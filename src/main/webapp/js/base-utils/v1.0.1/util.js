function configureHeightOfContainers(containerIds){
    var minHeight = $(window).height();
    var padding = 32*3;

    for (var index = 0, len = containerIds.length; index < len; ++index) {
        var id = containerIds[index];
        resizeContainer($(id), $(id + "-contents"), minHeight, padding);
    }
}

function resizeContainer(container, contents, minHeight, padding){
    var contentsHeight = contents.height();
    var desiredHeight = contentsHeight + (padding * 2);

    if(desiredHeight < minHeight)
        container.css({"height": minHeight + "px"});
    else
        container.css({"height": desiredHeight + "px"});
}

function hasParent(el, id){
    if (el) {
        do {
            if (el.id === id)
                return true;
            if (el.nodeType === 9)
                break;
        }
        while((el = el.parentNode));
    }
    return false;
}

function configInnerWrapper(){
    document.addEventListener('click', function(e) {
            var $body = $("body");
            if(e.target.id === 'nav-open-btn')
                return;
            if ($body.hasClass(menuOpened) && !hasParent(e.target, 'nav-menu')) {
                e.preventDefault();
                toggleMenu();
            }
        },
        true);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$.fn.removeClassPrefix = function(prefix) {
    this.each(function(i, el) {
        var classes = el.className.split(" ").filter(function(c) {
            return c.lastIndexOf(prefix, 0) !== 0;
        });
        el.className = $.trim(classes.join(" "));
    });
    return this;
};

function doPostRequestAsForm(path, params) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}