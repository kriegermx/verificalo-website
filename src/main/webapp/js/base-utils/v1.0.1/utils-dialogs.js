dialogs = {};

function createDialog(isLoader, content){
    dialogs.removeAllDialogs();

    var overlay = document.createElement("div");
    overlay.className = "dialog-overlay";
    overlay.id = "overlay";

    var screen = document.createElement("div");
    screen.id = "screen";

    var dialog = document.createElement("div");
    dialog.className = "dialog";
    if(isLoader) {
        dialog.id = "dialog-loading";
        dialog.appendChild(content);
    } else {
        dialog.id = "dialog-info";
        dialog.appendChild(content);

        overlay.addEventListener('click', dialogs.removeAllDialogs);
        dialog.addEventListener('click', function (event) {
            event.stopPropagation();
        });
    }
    screen.appendChild(dialog);
    overlay.appendChild(screen);
    document.getElementsByTagName('body')[0].appendChild(overlay);
}

dialogs.showProgressDialog = function(title, loaderResUrl){
    var loader = document.createElement("img");
    loader.src = loaderResUrl;
    createDialog(true, createCardWithTitle(title, loader));
};

dialogs.removeAllDialogs = function(){
    var overlay = $('#overlay');
    overlay.remove();
};

dialogs.showErrorDialog = function(title, msg, shouldBlockUI){
    var content = document.createElement("div");
    content.className = "mdl-card__supporting-text";
    content.id = "dialog-info-content";
    content.innerHTML = msg;
    var buttons = [{type:BUTTON_CLOSE}];
    createDialog(shouldBlockUI, createCardWithTitle(title, content, buttons));
};

dialogs.showOptionSelectorDialog = function(title, options, clickListener){
    var content = document.createElement("div");
    content.className = "option-container";

    var list = document.createElement("ul");
    list.className = "mdl-menu dialog-menu";
    for(var i = 0, size = options.length; i < size; i++) {
        var element = document.createElement("li");
        element.className = "mdl-menu__item";
        element.innerHTML = options[i];
        (function(e) {
            element.onclick = function () {
                clickListener(e, options[e]);
            }
        })(i);
        list.appendChild(element);
    }
    content.appendChild(list);
    var buttons = [{type:BUTTON_CLOSE}];
    createDialog(false, createCardWithTitle(title, content, buttons));
};

dialogs.showCustomDialog = function(title, content, buttons, shouldBlockUI){
    createDialog(shouldBlockUI, createCardWithTitle(title, content, buttons));
};