BUTTON_CLOSE    = 0;
BUTTON_SEND     = 1;
BUTTON_SAVE     = 2;
BUTTON_APPROVE  = 3;

function createCardWithTitle(title, content, buttons){
    var div0 = document.createElement("div");
    div0.className = "mdl-card";

    var div1 = document.createElement("div");
    div1.className = "mdl-card__title";

    var title0 = document.createElement("h2");
    title0.className = "mdl-card__title-text";
    title0.innerHTML = title;

    div1.appendChild(title0);
    div0.appendChild(div1);
    div0.appendChild(content);
    if(buttons)
        div0.appendChild(buildButtonContainer(buttons));
    return div0;
}

function buildButtonContainer(buttons) {
    var div = document.createElement("div");
    div.className = "mdl-card__actions mdl-card--border dialog-container-btn";
    for(var i = 0, size = buttons.length; i < size; i++){
        var btn = document.createElement("button");
        btn.className = "mdl-button mdl-js-button mdl-button--primary";
        if(buttons[i].callback)
            btn.addEventListener('click', buttons[i].callback);
        switch (buttons[i].type){
            case BUTTON_CLOSE:
                btn.id = "dialog-info-btn-close";
                btn.innerHTML = "Cerrar";
                btn.addEventListener('click', dialogs.removeAllDialogs);
                break;
            case BUTTON_SEND:
                btn.id = "dialog-info-btn-send";
                btn.innerHTML = "Enviar";
                break;
            case BUTTON_SAVE:
                btn.id = "dialog-info-btn-save";
                btn.innerHTML = "Guardar";
                break;
            case BUTTON_APPROVE:
                btn.id = "dialog-info-btn-approve";
                btn.innerHTML = "Aprobar";
                break;
        }
        div.appendChild(btn);
    }
    return div;
}

function buildMaterialTextField(id, hint, shouldFillParent, initialValue, type){
    var field = document.createElement("div");
    field.className = "mdl-textfield mdl-js-textfield" + (shouldFillParent ? " fill-parent-horizontally" : "");

    var input = document.createElement("input");
    input.className = "mdl-textfield__input";
    input.id = id;
    if(type === undefined)
        type = "text";
    input.type = type;
    if(initialValue != null)
        input.value = initialValue;
    field.appendChild(input);

    var label = document.createElement("label");
    label.className = "mdl-textfield__label";
    label.innerHTML = hint;
    label.htmlFor = id;
    field.appendChild(label);
    componentHandler.upgradeElement(field);
    return field;
}

function buildMaterialRadioButtons(id, elements, selectedElement){
    var container = document.createElement("div");
    container.className = "flex-centering-container";

    for(var i = 0, size = elements.length; i < size; i++){
        var label = document.createElement("label");
        label.className = "mdl-radio mdl-js-radio mdl-js-ripple-effect";
        label.htmlFor = "js-generated-radio-" + id + "-" + i;

        var input = document.createElement("input");
        input.className = "mdl-radio__button";
        input.id = "js-generated-radio-" + id + "-" + i;
        input.type = "radio";
        input.name = id;
        input.value = elements[i].id;
        input.checked = ((selectedElement == null) ? (i == 0) : (selectedElement == elements[i].id));
        label.appendChild(input);

        var span = document.createElement("span");
        span.className = "mdl-radio__label";
        span.innerHTML = elements[i].name;
        label.appendChild(span);

        componentHandler.upgradeElement(label);
        container.appendChild(label);
    }
    return container;
}