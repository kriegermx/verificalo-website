(function($) {
    $(function() {
        isModernBrowser();
        ///// Calendar configuration ////
        calendarInit();
        createECDataFlow();
        addEnterEventListener();
    });
})(jQuery);

function toggleContent(element){
    var content = $(element).siblings();
    var caret = $(element).find("i");
    if (content.is(":visible")) {
        caret.removeClass("fa-chevron-up");
        caret.addClass("fa-chevron-down");
    } else {
        caret.removeClass("fa-chevron-down");
        caret.addClass("fa-chevron-up");
    }
    content.toggle();
}

//////// Calendar functions  ////////////

var calendar;
var spans = [];

function calendarInit(){
    //Change lang
    moment.locale("es");
    //Obtain date information
    calendar = moment();
    //Obtain month - year and display
    $("#showed-month").text(calendar.format('MMMM YYYY'));
    //Get weekday
    var weekday = calendar.weekday();
    var calendarArray = [0,1,2,3,4,5,6];
    var offset = weekday + 1;
    if (offset == 7)
        offset = 0;
    calendarArray[offset] = calendar.format('DD');
    spans =  $('#calendar-days').find('span');
    spans[offset].className += "active";

    //Calendar array fill
    while(offset > 0){
        offset--;
        calendarArray[offset] = calendar.subtract(1,'days').format('DD');
    }
    //Calendar array refresh to default
    calendar = moment();
    weekday = calendar.weekday();
    offset = weekday + 1;
    if (offset == 7)
        offset = 0;

    //Calendar array fill
    while(offset < 6){
        offset++;
        calendarArray[offset] = calendar.add(1,'days').format('DD');
    }

    //Refresh values for end-user
    for (var i = 0; i < spans.length; ++i) {
        spans[i].innerHTML = calendarArray[i];
    }
    addGummedColorClass();
}

//Add class for right gummed color

function addGummedColorClass(){
    //Check if hologram != CERO or DOBLE CERRO - Allowed to drive everyday
    if (userCarDetails.hologram == "CERO" || userCarDetails.hologram == "DOBLE CERO") {
        $("#no-drive-days-calendar").hide();
        $("#allow-to-drive-warning").show();
    }
    else{
        switch (userCarDetails.gummedColor){
            case "yellow":
                spans[1].className +=" disabled ";
                break;

            case "pink":
                spans[2].className +=" disabled ";
                break;

            case "red":
                spans[3].className +=" disabled ";
                break;

            case "green":
                spans[4].className +=" disabled ";
                break;

            case "blue":
                spans[5].className +=" disabled ";
                break;

        }
    }
    if (userCarDetails.hologram == "DOS"){
        spans[6].className +=" disabled ";
        return;
    }
    else if (userCarDetails.hologram == "UNO"){
        noDriveSaturdayColorClass();
    }
}

function calendarNavigationClicked(itemClicked){
    //Clean CSS styles
    removeCalendarClasses();

    //Fill days of the week and display to user
    if (itemClicked == 0){
        calendar.add(-14,'days').format('DD');
        for (var i = 0; i < spans.length; ++i) {
            spans[i].innerHTML = calendar.add(1,'days').format('DD');
        }
    }
    else{
        for (var i = 0; i < spans.length; ++i) {
            spans[i].innerHTML = calendar.add(1,'days').format('DD');
        }
    }

    var calendarDefault = moment();
    var actualDay = calendarDefault.format('DD');
    var actualMonth = calendarDefault.format('MMMM YYYY');

    //Check if month, week and day corresponds to actual day

    if (actualMonth == calendar.format('MMMM YYYY')){
        for (var i = 0; i < spans.length; ++i) {
            if (spans[i].innerHTML == actualDay){
                spans[i].className +=" active ";
            }
        }
    }
    //Refresh Month displayed
    $("#showed-month").text(calendar.format('MMMM YYYY'));

    //If applies add default class for no-drive days
    addGummedColorClass();
}

var noDriveDaySaturdaysArray = [];

function noDriveSaturdayColorClass(){
    noDriveDaySaturdaysArray = [];
    var noDriveCalendar = calendar.clone();
    var contextMonth = calendar.month();
    noDriveCalendar.startOf('month');

    switch(noDriveCalendar.day()){
        case 6: {   //First day of month is Saturday
            break;
        }
        case 0: { //First day of month is Sunday - can't subtract 1
            noDriveCalendar.add(6,'day'); //First Saturday
            break;
        }
        default:{
            noDriveCalendar.endOf('week');
            noDriveCalendar.subtract(1,'day'); //First saturday
            break;
        }
    }
    while (contextMonth === noDriveCalendar.month()){
        noDriveDaySaturdaysArray.push(noDriveCalendar.format('DD'));
        noDriveCalendar.add(7, 'day'); //Pushing all saturdays
    }

    switch (userCarDetails.hologramParity){
        case "even":
            if (spans[6].innerHTML == noDriveDaySaturdaysArray[1]){
                spans[6].className +=" disabled ";
            }
            else  if (spans[6].innerHTML == noDriveDaySaturdaysArray[3]){
                spans[6].className +=" disabled ";
            }
            break;

        case "odd":
            if (spans[6].innerHTML == noDriveDaySaturdaysArray[0]){
                spans[6].className +=" disabled ";
            }
            else  if (spans[6].innerHTML == noDriveDaySaturdaysArray[2]){
                spans[6].className +=" disabled ";
            }
            break;
    }

}

function removeCalendarClasses(){
    for (var i = 0; i < spans.length; ++i) {
        spans[i].className = "";
    }
}

//////// Environmental checks functions  ////////////
var EC_REQUIREMENTS = {
    first_time: {
        base_requirements: [
            "Tarjeta de circulación"
        ],
        if_new: [
            "Copia Factura y/o contrato de arrendamiento (si requiere holograma Doble Cero por 1era, 2nda o 3era vez)"
        ],
        if_local: [
            "Constancia de verificación próxima anterior",
            "Baja por cambio de placa"
        ],
        if_plate_change: [
            "Baja por cambio de placa",
            "Si deriva de rechazo PIREC: Garantía de cambio de convertidor"
        ],
        if_late: [
            "Pago de multa por verificación extemporánea"
        ],
        if_taxi: [
            "Trámite y pago de la sustitución de la unidad"
        ]
    },
    regular: [
        "Tarjeta de circulación",
        "Constancia de verificación próxima anterior",
        "Si deriva de rechazo PIREC: Garantía de cambio de convertidor"
    ],
    late: [
        "Tarjeta de circulación",
        "Pago de multa por verificación extemporánea",
        "Si deriva de rechazo PIREC: Garantía de cambio de convertidor"
    ]
};
var questionnaire = {
    isTaxi: false,
    isNew: false,
    isLocal: false,
    late: false
};
function createECDataFlow(){
    var container = $("#ec-container");
    var title = $("#ec-title");
    var subtitle = $("#ec-subtitle");
    if(window.environmentalChecks.length == 0){
        title.addClass("blue");
        subtitle.html("Primera vez");

        questionnaire.isTaxi = userCarDetails.isTaxi;

        //Ask user if car is new
        var text = "¡Bienvenido a Verifícalo! No existen registros de verificación para las placas ingresadas. Para poder listar que documentos requieres para realizar su primera verificación, necesitamos que respondas algunas preguntas:";
        var question = "¿El automóvil es nuevo?";
        var ans1 = {
            text: "Sí",
            action: function() {
                questionnaire.isNew = true;
                buildIsInTimeframeQuestion();
            }
        };
        var ans2 = {
            text: "No",
            action: buildIsLocalQuestion
        };

        buildQuestion(text, question, ans1, ans2, container);
    } else {
        //Change lang
        moment.locale("es");
        var last = null, now = moment().format("YYYY-MM-DD");
        for(var i = 0, size = window.environmentalChecks.length; i < size; i++){
            var current = window.environmentalChecks[i].vExpiration.replace("/", "-");
            var date = moment(current, "YYYY-MM-DD");
            //var date = new Date(current);
            if (last == null)
                last = date;
            else if (date.isAfter(last))
                last = date;
        }
        if(last.isSameOrAfter(now)){ //Env. check is current
            title.addClass("blue");
            subtitle.html("");

            var checkStartDate = moment(last, "YYYY-MM-DD").startOf('month').subtract(1, 'months');

            var dateFormat = "DD [de] MMMM [de] YYYY";
            var btnContainer_0 = document.createElement("div");
            btnContainer_0.className = "button-container";

            if(checkStartDate.isAfter(now)) {
                title.addClass("green");
                subtitle.html("En regla");
                var p_1 = document.createElement("p");
                p_1.innerHTML = "El automóvil cuenta con una verificación vigente y su siguiente periodo de verificación es entre " + moment(checkStartDate).format(dateFormat) + " y " + moment(last).format(dateFormat) + ". Recuerda que para que puedas realizar tu próxima verificación sin complicaciones, deberás tener tu pago de tenencia al corriente y haber realizado el pago de cualquier infracción de tránsito pendiente. En tu siguiente verificación, necesitarás: ";
                container.append(p_1);
                container.append(buildECRequirements("regular"));
                if(window.hasPendingTaxes || window.hasPendingTickets) {
                    container.append(buildRequirementWarning(window.hasPendingTickets, window.hasPendingTaxes));
                }
                btnContainer_0.appendChild(buildVerificationRecordButton());
                btnContainer_0.append(buildVerificenterButton());
                container.append(btnContainer_0);

            } else {
                title.addClass("blue");
                subtitle.html("A realizar");
                var p_0 = document.createElement("p");
                p_0.innerHTML = "Tu vehículo se encuentra dentro del período de verificación y tienes hasta el " + moment(last).format(dateFormat) + " para llevar a cabo el proceso. Para realizarlo deberás presentarte en un verificentro con la siguiente documentación:";

                container.append(p_0);
                container.append(buildECRequirements("regular"));
                if(window.hasPendingTaxes || window.hasPendingTickets)
                    container.append(buildRequirementWarning(window.hasPendingTickets, window.hasPendingTaxes));
                btnContainer_0.appendChild(buildVerificationRecordButton());
                btnContainer_0.append(buildVerificenterButton());
                container.append(btnContainer_0);
            }
        } else { //Env. check is out of date
            title.addClass("red");
            subtitle.html("Extemporánea");

            $("#no-drive-days-calendar").hide();
            $("#allow-to-drive-warning").hide();
            $("#no-drive-warning").show();

            var p = document.createElement("p");
            p.innerHTML = "Tu verificación ya no es vigente y has superado el tiempo disponible para hacerlo. Tu automóvil no puede circular y debes pagar una multa por verificación extemporánea.<br><br>Antes de pagar tu multa por verificación extemporánea, asegurate de no tener adeudos de tenencia y/o infracciones de tránsito, con el objeto de evitar que se venza la vigencia del pago de tu multa y tener la obligación de volver a pagarla.<br><br>Si ya realizaste tu pago, puedes validar tu línea de captura para confirmar que se ha reflejado en el sistema; una vez que esto suceda, puedes realizar tu verificación llevando la siguiente documentación:";

            var btnContainer = document.createElement("div");
            btnContainer.className = "button-container";
            btnContainer.appendChild(buildLateFeeButton());
            btnContainer.appendChild(buildValidateLateFeeButton());
            btnContainer.appendChild(buildVerificationRecordButton());
            btnContainer.appendChild(buildVerificenterButton());

            container.append(p);
            container.append(buildECRequirements("late"));
            if(window.hasPendingTaxes || window.hasPendingTickets)
                container.append(buildRequirementWarning(window.hasPendingTickets, window.hasPendingTaxes));
            container.append(btnContainer);
        }
    }
    if(window.hasPendingTaxes){
        ga('send', 'event', "Tenencia", userCarDetails.plateNumber, "0" ,window.numberOfTaxDebts);
    }
    if(window.hasPendingTickets){
        ga('send', 'event', "Infracciones",  userCarDetails.plateNumber, "0", window.numberOfTicketDebts);
    }
}

function buildECRequirements(type, answers){
    var ul = document.createElement("ul");

    var data = EC_REQUIREMENTS[type];
    switch (type){
        case "first_time":
            data = data.base_requirements;
            if(answers.isTaxi)
                data = data.concat(EC_REQUIREMENTS.first_time.if_taxi);
            if(answers.isNew)
                data = data.concat(EC_REQUIREMENTS.first_time.if_new);
            else {
                if(answers.isLocal)
                    data = data.concat(EC_REQUIREMENTS.first_time.if_local);
                else
                    data = data.concat(EC_REQUIREMENTS.first_time.if_plate_change);
            }
            if(answers.late)
                data = data.concat(EC_REQUIREMENTS.first_time.if_late);
            ul = buildRequirementList(data, ul);
            break;
        case "regular":
            ul = buildRequirementList(data, ul);
            break;
        case "late":
            ul = buildRequirementList(data, ul);
            break;
    }
    return ul;
}

function buildRequirementList(list, ul){
    for(var i = 0, size = list.length; i < size; i ++){
        var li = document.createElement("li");
        li.innerHTML = list[i];
        ul.appendChild(li);
    }
    return ul;
}

function buildRequirementWarning(hasTickets, owesTaxes) {
    var text = "Nota: tu automóvil tiene adeudos de ";
    if(hasTickets){
        text += "infracciones";

        if(owesTaxes)
            text += " y de tenencia"
    } else
        text += "tenencia";
    text += " por lo que no podrás realizar tu verificación hasta no cumplir con dichas obligaciones.";
    var pText = document.createElement("p");
    pText.innerHTML = text;
    return pText;
}

function buildVerificenterButton(){
    var btn = document.createElement("button");
    btn.innerHTML = "Agendar Cita";
    btn.className = "inline-element";
    btn.title = "Agenda tu cita en un verificentro";
    btn.onclick = function () {
        ga('send', 'event', "Click", "Agendar Cita", "0", "1");
        location.href = "/dashboard/map.jsp";
    };
    return btn;
}

function buildLateFeeButton(){
    var btn = document.createElement("button");
    btn.className = "inline-element";
    btn.innerHTML = "Pagar multa";
    btn.title = "Pagar multa por verificación extemporánea";
    btn.onclick = function () {
        ga('send', 'event', "Click", "Pagar Multa", "0", "1");
        window.open("http://data.finanzas.cdmx.gob.mx/formato_lc/vehicular/medioambiente/medio_vehiculo.php", '_blank');
    };
    return btn;
}

function buildValidateLateFeeButton(){
    var btn = document.createElement("button");
    btn.className = "inline-element";
    btn.innerHTML = "Validar línea";
    btn.title = "Valida tu pago en finanzas";
    btn.onclick = function () {
        ga('send', 'event', "Click", "Validar línea", "0", "1");
        window.open("http://data.finanzas.cdmx.gob.mx/consultas_pagos/", '_blank');
    };
    return btn;
}

function buildVerificationRecordButton(){
    var btn = document.createElement("button");
    btn.className = "inline-element";
    btn.innerHTML = "Verificaciones";
    btn.title = "Consulta el historial de verificaciones";
    btn.onclick = function () {
        ga('send', 'event', "Click", "Historial de Verificación", "0", "1");
        showVerificationRecordFunction();
    };
    return btn;
}

function buildQuestion(text, question, ans1, ans2, $container){
    var pText = document.createElement("p");
    pText.innerHTML = text;

    var divQuestion = document.createElement("div");
    divQuestion.className = "question-wrapper";

    var qTitle = document.createElement("h6");
    var b1 = document.createElement("button");
    var b2 = document.createElement("button");
    qTitle.innerHTML = question;
    b1.innerHTML = ans1.text;
    b2.innerHTML = ans2.text;
    b1.onclick = ans1.action;
    b2.onclick = ans2.action;

    divQuestion.appendChild(qTitle);
    divQuestion.appendChild(b1);
    divQuestion.appendChild(b2);

    $container.append(text);
    $container.append(divQuestion);
}

function buildIsInTimeframeQuestion(){
    var container = $("#ec-container");
    container.empty();

    //Ask user if he's still within the EC timeframe
    var text = "Excelente, ahora bien, por ley tienes un período de 180 días para verificar tu automóvil.";
    var question = "¿Aún estás dentro de ese período?";
    var ans1 = {
        text: "Sí",
        action: buildRequirementsAfterQuestionnaire
    };
    var ans2 = {
        text: "No",
        action: function () {
            questionnaire.late = true;
            buildRequirementsAfterQuestionnaire();
        }
    };
    buildQuestion(text, question, ans1, ans2, container);
}

function buildIsLocalQuestion(){
    var container = $("#ec-container");
    container.empty();

    //Ask user if he's still within the EC timeframe
    var text = "Si tu automóvil no es nuevo, significa que cambió de placa. Nota: responder 'NO' implica que el automóvil tenía placas de la Ciudad de México y ya había sido verificado previamente.";
    var question = "¿El automóvil tenía placas de otro estado?";
    var ans1 = {
        text: "Sí",
        action: buildIsInTimeframeQuestion
    };
    var ans2 = {
        text: "No",
        action: function () {
            questionnaire.isLocal = true;
            buildIsInTimeframeQuestion();
        }
    };
    buildQuestion(text, question, ans1, ans2, container);
}

function buildRequirementsAfterQuestionnaire(){
    var container = $("#ec-container");
    container.empty();
    var p = document.createElement("p");
    p.innerHTML = "Para llevar a cabo tu proceso de verificación, deberás presentarte en un verificentro con la siguiente documentación:";

    container.append(p);
    container.append(buildECRequirements("first_time", questionnaire));

    if(window.hasPendingTaxes || window.hasPendingTickets)
        container.append(buildRequirementWarning(window.hasPendingTickets, window.hasPendingTaxes));

    var btnContainer = document.createElement("div");
    btnContainer.className = "button-container";
    if(questionnaire.late) {
        btnContainer.appendChild(buildLateFeeButton());
        btnContainer.appendChild(buildValidateLateFeeButton());
    }
    btnContainer.appendChild(buildVerificenterButton());

    container.append(btnContainer);
}

//////// Verification Record function ////////////

function showVerificationRecordFunction(){
    var content = document.createElement("div");
    content.className = "option-container";

    for (var i=0; i<window.environmentalChecks.length; i++){
        var subtitle = document.createElement("p");
        subtitle.className = "dialog-subtitle bold with-margin color-purple";
        subtitle.innerHTML = "Verificación: " + window.environmentalChecks[i].certificate;

        var verificationContainer = document.createElement("div");
        verificationContainer.className = "dialog-text-container";
        var verificationDate = document.createElement("p");
        verificationDate.innerHTML = "Fecha de verificación: " + window.environmentalChecks[i].vDate + " - " + window.environmentalChecks[i].verificationTime;
        var expirationDate = document.createElement("p");
        expirationDate.innerHTML = "Vigencia: " + window.environmentalChecks[i].vExpiration;
        var verificationCenter = document.createElement("p");
        verificationCenter.innerHTML = "Verificentro: " + window.environmentalChecks[i].verificationCenter;
        var equipment = document.createElement("p");
        equipment.innerHTML = "Equipo GDF: " + window.environmentalChecks[i].equipment;
        var line = document.createElement("p");
        line.innerHTML = "Línea: " + window.environmentalChecks[i].line;
        var result = document.createElement("p");
        result.innerHTML = "Resultado: " + window.environmentalChecks[i].result;
        var cancelled = document.createElement("p");
        cancelled.innerHTML = "Cancelado: " + window.environmentalChecks[i].cancelled;
        var rejected = document.createElement("p");
        rejected.innerHTML = "Causa de rechazo: " + window.environmentalChecks[i].rejectionCause;

        content.appendChild(subtitle);
        verificationContainer.appendChild(verificationDate);
        verificationContainer.appendChild(expirationDate);
        verificationContainer.appendChild(verificationCenter);
        verificationContainer.appendChild(equipment);
        verificationContainer.appendChild(line);
        verificationContainer.appendChild(result);
        verificationContainer.appendChild(cancelled);
        verificationContainer.appendChild(rejected);
        content.appendChild(verificationContainer);
    }

    var button = [{type:BUTTON_CLOSE}];
    dialogs.showCustomDialog("Historial de Verificaciones",content,button,false);
}

//////// Reminder functions  ////////////

function checkboxAllToggle(checkbox){
    var checkNoDrive = document.getElementById("NotificacionesHoyNoCircula");
    var checkTax = document.getElementById("RecordatorioPagoTenencia");
    var checkVerification = document.getElementById("RecordatorioVerificacion");
    var checkAll = document.getElementById("Todas");

    if(checkbox.checked){
        checkNoDrive.checked = true;
        checkNoDrive.setAttribute("checked","true");
        checkTax.checked = true;
        checkTax.setAttribute("checked","true");
        checkVerification.checked = true;
        checkVerification.setAttribute("checked","true");
        checkAll.checked = true;
        checkAll.setAttribute("checked","true");

    }
    else{
        checkNoDrive.checked = false;
        checkNoDrive.removeAttribute("checked");
        checkTax.checked = false;
        checkTax.removeAttribute("checked");
        checkVerification.checked = false;
        checkVerification.removeAttribute("checked");
        checkAll.checked = false;
        checkAll.removeAttribute("checked");
    }
}

function checkboxToggle(checkbox){
    var checkNoDrive = document.getElementById("NotificacionesHoyNoCircula");
    var checkTax = document.getElementById("RecordatorioPagoTenencia");
    var checkVerification = document.getElementById("RecordatorioVerificacion");
    var checkAll = document.getElementById("Todas");

    switch (checkbox.id){
        case "NotificacionesHoyNoCircula":
            if(checkbox.checked){
                checkNoDrive.checked = true;
                checkNoDrive.setAttribute("checked","true");
            }
            else{
                checkNoDrive.checked = false;
                checkNoDrive.removeAttribute("checked");
            }
            break;
        case "RecordatorioPagoTenencia":
            if(checkbox.checked){
                checkTax.checked = true;
                checkTax.setAttribute("checked","true");
            }
            else{
                checkTax.checked = false;
                checkTax.removeAttribute("checked");
            }
            break;
        case "RecordatorioVerificacion":
            if(checkbox.checked){
                checkVerification.checked = true;
                checkVerification.setAttribute("checked","true");
            }
            else{
                checkVerification.checked = false;
                checkVerification.removeAttribute("checked");
            }
            break;
    }

    if (checkNoDrive.checked && checkTax.checked && checkVerification.checked){
        checkAll.checked = true;
        checkAll.setAttribute("checked","true");
    }else{
        checkAll.checked = false;
        checkAll.setAttribute("checked","false");
    }
}

function getCheckboxStatus(id){
    var checkbox = document.getElementById(id);
    return checkbox.getAttribute("checked");
}

function prepareSubscription(){
    var types = [];
    if(getCheckboxStatus("Todas") == true){
        types.push("ALL");
        return types;
    }
    else {
        if(getCheckboxStatus("NotificacionesHoyNoCircula") == true){
            types.push("HNC");
        }
        if(getCheckboxStatus("RecordatorioPagoTenencia") == true){
            types.push("PTV");
        }
        if(getCheckboxStatus("RecordatorioVerificacion") == true){
            types.push("VPV");
        }
        return types
    }
}

function suscribeEmail(plate){
    var email = document.getElementById('email').value;
    if(isRFC822ValidEmail(email) == true) {
        var types = prepareSubscription();
        var url = "/apis/notifications/suscribe";
        var responses = [];
        for (var i = 0; i < types.length; i++) {
            var data = {plateNumber: plate, type: types[i], email: email};
            postData(url, data).always(function (response) {
                switch (response.status) {
                    case 200:
                        ga('send', 'event', "Suscripción exitosa", types[i], "0", "1");
                        responses.push(true);
                        break;
                    default:
                        ga('send', 'event', "Suscripción no exitosa", types[i], "0", "0");
                        responses.push(false);
                        break;
                }
            });
        }
        var responseBool = true;
        for (var j = 0; j < responses.length; j++) {
            responseBool = responseBool && responses[j];
        }
        if (responseBool) {
            dialogs.showErrorDialog("Gracias", "Tu correo ahora recibirá notificaciones para la placa: " + plate, false);
        }
        else {
            dialogs.showErrorDialog("Lo sentimos", "Algo salió, mal intenta más tarde", false);
        }
    }
    else
    {
        dialogs.showErrorDialog("Lo sentimos","Tu correo electrónico parece no ser válido, intenta nuevamente",false);
        return;
    }

}

//////////// Payment Click event handler ////////////

function onPaymentClick(id){
    ga('send', 'event', "Pagar Infraccion", id, "0", "0");
    window.open("http://data.finanzas.cdmx.gob.mx/formato_lc/infracciones/index.php?folio_envia=" + id,"_blank");
}

function onPaymentTaxClick(){
    ga('send', 'event', "Pagar Adeudo", "Tenencia", "0", "0");
    window.open("http://data.finanzas.cdmx.gob.mx/formato_lc/tenencia_2017/","_blank");
}