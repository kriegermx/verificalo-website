function onLicensePlateSearch(id) {
    var lp = $(id).val().toUpperCase();
    if(/^[1-9A-Z]\d{2}[A-Z]{3}$/.test(lp) || /^(([ABLS]\d{3})|([M][0]\d{2})|([T][P]\d[A-B]))\d{2}$/.test(lp)){
        ga('send', 'event', "Búsqueda", lp, "Placa", "1");
        location.href = "/getPlateInfo?plateNumber=" + lp;
    }
    else
        dialogs.showErrorDialog("Lo sentimos", "Las placas que introdujiste son inválidas o no corresponden a un automóvil registrado en la Ciudad de México", false);
}

function addEnterEventListener() {
    document.getElementById("license-plate")
        .addEventListener("keyup", function(event) {
            event.preventDefault();
            if (event.keyCode == 13) {
                document.getElementById("license-plate-button").click();
            }
        });
}