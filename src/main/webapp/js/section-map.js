(function($) {
    $(function() {
        isModernBrowser();

        var body = $(document.body);
        body.addClass("with-loader");
        body.addClass("is-loading");
        //Get information from APIs & then fill panel & mapInit()
        getVerificationCenters();
        var plate = getUrlParameter("plate");
        if (typeof plate !== "undefined"){
            getPendingCommentsfor(plate);
        }
    });
})(jQuery);

var markers = [];

var mymap;
//var neighborhoodArray = ["Todas","Álvaro Obregón","Azcapotzalco","Benito Juárez","Coyoacán","Cuauhtémoc","Gustavo A. Madero","Iztacalco","Iztapalapa","Magdalena Contreras","Miguel Hidalgo","Tláhuac","Tlalpan","Venustiano Carranza","Xochimilco"];
var neighborhoodArray = ["TODAS","ALVARO OBREGON","AZCAPOTZALCO","BENITO JUAREZ","COYOACAN","CUAUHTEMOC","GUSTAVO A. MADERO","IZTACALCO","IZTAPALAPA","MAGDALENA CONTRERAS","MIGUEL HIDALGO","TLAHUAC","TLALPAN","VENUSTIANO CARRANZA","XOCHIMILCO"];

var verificationCenterTypeArray = ["TODOS","SOLO GASOLINA", "DIESEL VEHICULOS LIGEROS Y PESADOS"];
var verificationCenters = [];
var userNeighborhoodFilter = "TODAS";
var userTypeFilter = "TODOS";
var centerDetailId;
var pendingCenterForComment;
var plateForComment;

//// Get rankings before filling panel table ////

function getVerificationCenters(){
    var url = "/apis/verificenter/list/all";

    getData(url).always(function(response){
        var body = $(document.body);
        if((!!response.ratingList) && Object.keys(response.ratingList).length > 0){
            verificationCenters = response.ratingList;
            mapInit(verificationCenters);
            showCenters();
            showTableDate(response.lastUpdate);
            body.removeClass("is-loading");
            return true;
        }
        else{
            console.log("API not available");
            showReloadDialog();
            body.removeClass("is-loading");
            return false;
        }
    });
}

//// Get pending comments for certain plate ////

function getPendingCommentsfor(plate){
    var url = "/apis/verificenter/feedback/pending/";
    plateForComment = plate;
    url += plateForComment;

    getData(url).always(function(response){
        if (response.length > 0){
            pendingCenterForComment = response[0];
            dialogs.showReviewDialog();
        }
    });
}


//// Map functions ////

function mapInit(centers){
    mymap = L.map('mapid').setView([19.342313, -99.134812], 10);

    // Add Tile Layer

    var OpenStreetMap_Mapnik = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(mymap);
    mymap.attributionControl.setPrefix('');

    // Show all markers

    for (var i=0; i < centers.length; i++){
        pushMarkers(centers[i]);
    }

    var group = L.featureGroup(markers);
    mymap.fitBounds(group.getBounds());
}

function createPopUp(center) {
    var popupContent = document.createElement("div");
    var popupTitle = document.createElement("h4");
    popupTitle.innerHTML = center.Nombre;

    popupContent.appendChild(popupTitle);

    var popupSubtitle = document.createElement("p");
    popupSubtitle.innerHTML = center.Direccion;

    var popupAvailability = document.createElement("h4");
    popupAvailability.className = "popup-availability";
    popupAvailability.innerHTML = "CITAS DISPONIBLES: <span>" + center.CitasDisp + "</span>";

    popupContent.appendChild(popupSubtitle);
    popupContent.appendChild(popupAvailability);

    var popupButtons = document.createElement("div");
    popupButtons.className = "popupButtons";

    var popupDetailButton = document.createElement("div");
    popupDetailButton.className = "popupDetailButton";

    var detailButton = document.createElement("button");
    detailButton.className = "generic-button";
    detailButton.type = "submit";


    detailButton.addEventListener('click',function(){
        centerDetailId = center.Centro;
        ga('send', 'event', "Click", centerDetailId, "0", "1");
        dialogs.showDetailDialog(centerDetailId, center.Nombre,center.Direccion, center.Delegacion, center.Telefonos, center.Tipo, center.rating);
    });

    detailButton.innerHTML = "DETALLES";

    popupDetailButton.appendChild(detailButton);

    var popupAppointmentButton = document.createElement("div");
    popupAppointmentButton.className = "popupAppointmentButton";

    var appointmentButton = document.createElement("button");
    appointmentButton.className = "generic-button";
    appointmentButton.type = "submit";
    appointmentButton.addEventListener('click',function(){
        ga('send', 'event', "Click", "Cita", centerDetailId, "1");
        window.open(center.Link,"_blank");
    });

    appointmentButton.innerHTML = "AGENDAR CITA";

    popupAppointmentButton.appendChild(appointmentButton);

    popupButtons.appendChild(popupDetailButton);
    popupButtons.appendChild(popupAppointmentButton);
    popupContent.appendChild(popupButtons);
    return popupContent;
}

function removeAllMarkers(){
    for(var i = 0; i < markers.length; i++){
        mymap.removeLayer(markers[i]);
    }

    markers = [];
}

function pushMarkers(selectedMarker){
    var customOptions =
        {
            'maxWidth': '500',
            'className' : 'custom'
        };
    var popupText = createPopUp(selectedMarker);
    var marker = new L.marker([selectedMarker.Latitud, selectedMarker.Longitud],{title: selectedMarker.Centro});
    marker.bindPopup(popupText,customOptions);
    marker.on('click', function (e) {});
    marker.addTo(mymap);
    markers.push(marker);
}

/// Panel filters and table functions ////

function fillTable(item, content) {
        var tableRow = document.createElement("tr");
        tableRow.addEventListener('click',function(){
            ga('send', 'event', "Click", item.Centro, "Tabla", "1");

            for (var i=0; i < markers.length; i++){
                if(markers[i].options.title == item.Centro){
                    mymap.setZoom(10);
                    markers[i].openPopup();
                    mymap.setView(L.latLng(markers[i]._latlng.lat,markers[i]._latlng.lng),15);

                    var panel = $("#panel-container");
                    if(panel.hasClass("is-active")){
                        togglePanelContent();
                        $("#panel-container").animate({ scrollTop: 0 }, "fast");
                    }
                    return;
                }
            }
        });
        var tableCode = document.createElement("td");
        tableCode.innerHTML = item.Centro;
        var tableName = document.createElement("td");
        tableName.innerHTML = item.Nombre;
        var tableScore = document.createElement("td");
        tableScore.className = "font-pink";

        var ranking = item.rating;
        if(ranking == 0){
            ranking += 5;
        }

        tableScore.innerHTML = ranking + ",0 ";
        var stars = document.createElement("i");
        stars.className = "fa fa-star";
        tableScore.appendChild(stars);

        tableRow.appendChild(tableCode);
        tableRow.appendChild(tableName);
        tableRow.appendChild(tableScore);
        content.appendChild(tableRow);
}

function filterClicked(option) {
    if(option){
        dialogs.showOptionSelectorDialog("Selecciona una delegación",neighborhoodArray,neighborhoodFilterSelected);
        if($( window ).width() <= "360") {
            var card = $("#dialog-info").find(".mdl-card__title-text");
            card.css("font-size", "0.8em");
        }
    }else{
        dialogs.showOptionSelectorDialog("Selecciona un tipo",verificationCenterTypeArray,typeFilterSelected);
    }
}

function showCenters(){
    var content = document.getElementById("centers-table-content");
    var itemMatchCounter = 0;

    if (userNeighborhoodFilter == "TODAS" && userTypeFilter == "TODOS"){
        filterCase = 0;
    }
    else if (userNeighborhoodFilter != "TODAS" && userTypeFilter == "TODOS"){
        filterCase = 1;
    }
    else if (userNeighborhoodFilter == "TODAS" && userTypeFilter != "TODOS"){
        filterCase = 2;
    }
    else if (userNeighborhoodFilter != "TODAS" && userTypeFilter != "TODOS"){
        filterCase = 3;
    }
    content.innerHTML = "";
    switch (filterCase){
        case 0:
            itemMatchCounter++;
            for (var i=0; i < verificationCenters.length; i++){
                pushMarkers(verificationCenters[i]);
                fillTable(verificationCenters[i], content);
            }
            break;
        case 1:
            for (var i=0; i < verificationCenters.length; i++){
                if (verificationCenters[i].Delegacion == userNeighborhoodFilter){
                    pushMarkers(verificationCenters[i]);
                    fillTable(verificationCenters[i], content);
                    itemMatchCounter++;
                }
            }
            break;
        case 2:
            for (var i=0; i < verificationCenters.length; i++){
                if (verificationCenters[i].Tipo == userTypeFilter){
                    pushMarkers(verificationCenters[i]);
                    fillTable(verificationCenters[i], content);
                    itemMatchCounter++;
                }
            }
            break;
        case 3:
            for (var i=0; i < verificationCenters.length; i++){
                if (verificationCenters[i].Delegacion == userNeighborhoodFilter && verificationCenters[i].Tipo == userTypeFilter){
                    pushMarkers(verificationCenters[i]);
                    fillTable(verificationCenters[i], content);
                    itemMatchCounter++;
                }
            }
            break;
    }
    if (itemMatchCounter == 0){
        var tableBody = $("#centers-table-content");
        var tr = document.createElement("tr");
        tr.style.cursor = "inherit";
        var td = document.createElement("td");
        td.colSpan = "3";
        td.innerHTML = "No hay resultados";
        tr.appendChild(td);
        tableBody.html(tr);
    }
    if (markers.length > 0){
        var group = L.featureGroup(markers);
        mymap.fitBounds(group.getBounds());
    }
}

function showTableDate(date){
    var div = document.getElementById("update-date");
    var p = document.createElement("p");
    p.className = "date";
    p.innerHTML = "Última actualización: " + date;
    div.appendChild(p);
}


function neighborhoodFilterSelected(e,string){
    dialogs.removeAllDialogs();
    userNeighborhoodFilter = string;
    removeAllMarkers();

    showCenters();

    //tableContentInit(geojsonLayer,userNeighborhoodFilter,userTypeFilter);
    var caret = "<i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>";
    var userNeiborhoodButton = $("#neighborhoodButton");
    userNeiborhoodButton.html(userNeighborhoodFilter + " " + caret);
}

function typeFilterSelected(e,string){
    dialogs.removeAllDialogs();
    userTypeFilter = string;
    removeAllMarkers();

    showCenters();

    var caret = "<i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>";
    var userTypeButton = $("#typeButton");
    userTypeButton.html(userTypeFilter + " " + caret);

}

//// Panel translate functions /////

function togglePanelContent(){
    var panel = $('#panel-container');
    if (panel.hasClass('is-active')){
        togglePanelContentCompletion();
        setTimeout(function () {
            panel.removeClass('is-active');
        }, 100);
    } else {
        togglePanelContentCompletion();
        panel.addClass('is-active');
    }
}

function togglePanelContentCompletion() {
    var panelToggleButton = $("#panel-toggle");
    var toggleButton = $("#button-toggle");
    if(panelToggleButton.hasClass("is-translated")){
        panelToggleButton.removeClass("is-translated");
        toggleButton.html('VER VERIFICENTROS <i class="fa fa-chevron-up" aria-hidden="true"></i></span>');

    }
    else{
        panelToggleButton.addClass("is-translated");
        toggleButton.html('VER MAPA <i class="fa fa-chevron-down" aria-hidden="true"></i></span>');
    }
}

//// Review Functions ////

function sendReview() {
    var comment = $("textarea#comment-text-area").val();
    var dialog = $("#errorMessages");
    dialog.html("");

    if (dialogStarRating != false && comment != "") {
        var data = {verificenterId:pendingCenterForComment,plate:plateForComment,feedback:comment,rating:dialogStarRating};
        var url = "/apis/verificenter/feedback/send";

        postData(url, data).always(function(response){
            switch (response.status){
                case 200:
                    ga('send', 'event', "Enviar", centerDetailId, "Retroalimentación enviada", "1");
                    var card = $("#dialog-info").find(".option-container");
                    var greetings = document.createElement("h3");
                    greetings.className = "dialog-subtitle";
                    greetings.style.marginBottom = "18pt";
                    greetings.innerHTML = "Muchas gracias por tu retroalimentación.";

                    card.html(greetings);

                    var buttons = $("#dialog-info").find(".dialog-container-btn");

                    var closebtn = document.createElement("button");
                    closebtn.className = "mdl-button mdl-js-button mdl-button--primary";
                    closebtn.id = "dialog-info-btn-close";
                    closebtn.innerHTML = "Cerrar";
                    closebtn.addEventListener('click', dialogs.removeAllDialogs);

                    buttons.html(closebtn);
                    break;
                default:
                    ga('send', 'event', "Enviar", centerDetailId, "Retroalimentación no enviada", "0");
                    var card = $("#dialog-info").find(".option-container");
                    var greetings = document.createElement("h3");
                    greetings.className = "dialog-subtitle";
                    greetings.style.marginBottom = "18pt";
                    greetings.innerHTML = "Lo sentimos, hubo un error al enviar tu comentario. Intenta más tarde.";

                    card.html(greetings);

                    var buttons = $("#dialog-info").find(".dialog-container-btn");

                    var closebtn = document.createElement("button");
                    closebtn.className = "mdl-button mdl-js-button mdl-button--primary";
                    closebtn.id = "dialog-info-btn-close";
                    closebtn.innerHTML = "Cerrar";
                    closebtn.addEventListener('click', dialogs.removeAllDialogs);

                    buttons.html(closebtn);
                    break;
            }
        });
    }
    else if (comment == "" &&  dialogStarRating != false) {
        var errorCommentMessage = document.createElement("p");
        errorCommentMessage.className = "dialog-error";
        errorCommentMessage.innerHTML = "- El campo de retroalimentación está vacío.";
        dialog.html(errorCommentMessage);
    }
    else if (dialogStarRating == false && comment != "") {
        var errorRatingMessage = document.createElement("p");
        errorRatingMessage.className = "dialog-error";
        errorRatingMessage.innerHTML = "- Olvidaste calificar el verificentro.";
        dialog.html(errorRatingMessage);
    }
    else if (dialogStarRating == false && comment == "") {
        var errorCommentMessage = document.createElement("p");
        errorCommentMessage.className = "dialog-error";
        errorCommentMessage.innerHTML = "- El campo de retroalimentación está vacío.";
        dialog.html(errorCommentMessage);

        var errorRatingMessage = document.createElement("p");
        errorRatingMessage.className = "dialog-error";
        errorRatingMessage.innerHTML = "- Olvidaste calificar el verificentro.";
        dialog.append(errorRatingMessage);
    }
}

function getReviewForCenter(centerId){
    var url = "/apis/verificenter/feedback/list/";
    url += centerId;

    $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json",
        processData: false})
        .always(function(response){
            if((!!response.feedbacks)) {
                var card = $("#dialog-info").find(".dialog-review-container");
                var userReviews = document.createElement("ul");
                userReviews.className = "dialog-review-table";
                if (response.feedbacks.length > 0) {
                    for (var i = 0; i < response.feedbacks.length; i++) {
                        var singleReview = document.createElement("li");
                        singleReview.className = "dialog-single-review";
                        var reviewElement = document.createElement("p");
                        reviewElement.className = "dialog-single-review-container";

                        var s = 0;
                        while (s < response.feedbacks[i].rating) {
                            var fullStars = document.createElement("i");
                            fullStars.className = "fa fa-star";
                            reviewElement.appendChild(fullStars);
                            s++;
                        }
                        while (s < 5) {
                            var emptyStars = document.createElement("i");
                            emptyStars.className = "fa fa-star-o";
                            reviewElement.appendChild(emptyStars);
                            s++;
                        }
                        var date = response.feedbacks[i].date.substr(0,response.feedbacks[i].date.indexOf(' '));
                        date = date.replace(/-/g , "/");
                        reviewElement.innerHTML += " " + date;
                        singleReview.appendChild(reviewElement);

                        var userReviewString = document.createElement("p");
                        userReviewString.id = "comment-" + i;
                        var longUserReviewString = document.createElement("p");
                        longUserReviewString.id = "long-comment-" + i;

                        if(response.feedbacks[i].feedback.length > 224){
                            var maxLength = 224;
                            var trimmedString = response.feedbacks[i].feedback.substr(0, maxLength);
                            trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
                            trimmedString += "...";
                            userReviewString.className = "dialog-single-review-string";
                            userReviewString.innerHTML = trimmedString;
                            singleReview.appendChild(userReviewString);

                            longUserReviewString.className = "dialog-single-review-string hidden";
                            longUserReviewString.innerHTML = response.feedbacks[i].feedback;
                            singleReview.appendChild(longUserReviewString);

                            var expandString = document.createElement("p");
                            expandString.className = "link";
                            expandString.innerHTML = "ver más";
                            expandString.id = "link-" + i;
                            singleReview.appendChild(expandString);
                            expandString.addEventListener('click', function(i){
                                var index = i.toElement.id.substr(i.toElement.id.length - 1);
                                var longText = $("#long-comment-" + index);
                                var shortText = $("#comment-" + index);
                                var linkText = $("#link-" + index);
                                if (longText.hasClass("hidden")){
                                    shortText.addClass("hidden");
                                    longText.removeClass("hidden");
                                    linkText.html("ver menos");
                                }else{
                                    longText.addClass("hidden");
                                    shortText.removeClass("hidden");
                                    linkText.html("ver más");
                                }
                            });
                        }
                        else{
                            userReviewString.className = "dialog-single-review-string";
                            userReviewString.innerHTML = response.feedbacks[i].feedback;
                            singleReview.appendChild(userReviewString);
                        }

                        userReviews.appendChild(singleReview);
                    }
                    card.html(userReviews);
                    return true;
                }

                else if (response.feedbacks.length == 0) {
                    var noCommentsLine = document.createElement("li");
                    noCommentsLine.className = "dialog-single-review";
                    var noCommentsText = document.createElement("p");
                    noCommentsText.className = "dialog-single-review-string";
                    noCommentsText.innerHTML = "Todavía no hay comentarios";
                    noCommentsLine.appendChild(noCommentsText);
                    userReviews.appendChild(noCommentsLine);
                    card.html(userReviews);
                    return true;
                }
            }
            else{
                console.log("API not available");
                showReloadDialog();
                return false;
            }
        });
}

/////// Dialog functions ///////
function showReloadDialog(){
    var content = document.createElement("div");
    content.className = "option-container";

    var subtitle = document.createElement("h3");
    subtitle.className = "dialog-subtitle";
    subtitle.innerHTML = "Vuelve a recargar la página o intenta más tarde.";
    subtitle.style.marginBottom = "18pt";
    content.appendChild(subtitle);

    var title = "Lo sentimos, ocurrió un error";

    dialogs.showCustomDialog(title, content,false ,true);

    var div = document.createElement("div");
    div.className = "mdl-card__actions mdl-card--border dialog-container-btn";

    var createReloadwbtn = document.createElement("button");
    createReloadwbtn.className = "mdl-button mdl-js-button mdl-button--primary";
    createReloadwbtn.id = "dialog-reload-btn";
    createReloadwbtn.innerHTML = "Recargar";
    createReloadwbtn.addEventListener('click', function(){
        window.location.reload();
    });
    div.appendChild(createReloadwbtn);

    $("#dialog-loading").find(".mdl-card").append(div);
}

