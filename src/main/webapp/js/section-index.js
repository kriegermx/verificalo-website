var containersToResize = [
    "#intro",
    "#how",
    "#who"
];

(function($) {
	$(function() {
		$('.scrolly')
			.scrolly({
				speed: 1500
			});

        $(window).on('resize', toDoOnResize);
        toDoOnResize();
        addEnterEventListener();
        isModernBrowser();
    });
})(jQuery);

function toDoOnResize(){
    configureHeightOfContainers(containersToResize);
}

function showErrorDialog(text) {
    dialogs.showErrorDialog("Lo sentimos", text, false);
}