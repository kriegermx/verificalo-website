//// Dialog to show all the details for a verification center ////
dialogs.showDetailDialog = function(title, subtitleContent, detailsContent, neighborhoodContent, phoneContent, typeContent, rating){
    var content = document.createElement("div");
    content.className = "option-container";

    var subtitle = document.createElement("h3");
    subtitle.className = "dialog-subtitle";
    subtitle.innerHTML = subtitleContent;
    content.appendChild(subtitle);

    var neighborhood = document.createElement("p");
    neighborhood.className = "dialog-details bold";
    neighborhood.innerHTML = neighborhoodContent + " - " + typeContent;
    content.appendChild(neighborhood);

    var details = document.createElement("p");
    details.className = "dialog-details";
    details.innerHTML = detailsContent;
    content.appendChild(details);

    var phone = document.createElement("p");
    phone.className = "dialog-details";
    phone.innerHTML = phoneContent;
    content.appendChild(phone);

    var userAverageReview = document.createElement("p");
    userAverageReview.className = "dialog-review";

    var ranking = rating;
    if(ranking == 0){
        ranking += 5;
    }
    userAverageReview.innerHTML = ranking + ",0 ";
    var addStar = 0;
    while(addStar<ranking){
        var averageStars = document.createElement("i");
        averageStars.className = "fa fa-star";
        userAverageReview.appendChild(averageStars);
        addStar++;
    }

    content.appendChild(userAverageReview);

     var reviewTitle = document.createElement("h4");
     reviewTitle.className = "dialog-review-title";
     reviewTitle.innerHTML = "RETROALIMENTACIONES";
     content.appendChild(reviewTitle);

    var reviewContainer = document.createElement("div");
    reviewContainer.className = "dialog-review-container with-loader-inside is-loading-inside";

    var loaderContainer =  document.createElement("div");
    loaderContainer.className = "loader-inside";

    var itemContainer =  document.createElement("div");
    itemContainer.className = "item-container";

    var spin =  document.createElement("i");
    spin.className = "fa fa-circle-o-notch fa-spin";

    itemContainer.appendChild(spin);
    loaderContainer.appendChild(itemContainer);
    reviewContainer.appendChild(loaderContainer);

    content.appendChild(reviewContainer);
    var header = "VERIFICENTRO " + title;
    createDialog(false, createCardWithTitle(header, content));

    var div = document.createElement("div");
    div.className = "mdl-card__actions mdl-card--border dialog-container-btn";

    var closebtn = document.createElement("button");
    closebtn.className = "mdl-button mdl-js-button mdl-button--primary";
    closebtn.id = "dialog-info-btn-close";
    closebtn.innerHTML = "Cerrar";
    closebtn.addEventListener('click', dialogs.removeAllDialogs);
    div.appendChild(closebtn);

    $("#dialog-info").find(".mdl-card").append(div);

    getReviewForCenter(title);
};

//// Dialog to let users leave a review ////
var dialogStarRating = false;
dialogs.showReviewDialog = function(){
    ga('send', 'event', "Retroalimentar", pendingCenterForComment, "Pendiente", "1");
    dialogStarRating = false;
    var content = document.createElement("div");
    content.className = "option-container";

    var subtitleContainer = document.createElement("div");
    subtitleContainer.id = "subtitle-container";
    subtitleContainer.className = "dialog-subtitle lowercase with-long-margin";

    var subtitle = document.createElement("p");
    subtitle.innerHTML = "Sabemos que fuiste recientemente a verificar tu automóvil. Nos gustaría que nos ayudaras a calificar el servicio del verificentro " + pendingCenterForComment + ".";
    subtitleContainer.appendChild(subtitle);
    content.appendChild(subtitleContainer);

    var errorContainer = document.createElement("div");
    errorContainer.id = "errorMessages";
    errorContainer.innerHTML = "";
    content.appendChild(errorContainer);

    var textArea = document.createElement("textarea");
    textArea.id = "comment-text-area";
    textArea.className = "form-control";
    textArea.placeholder = "Escribe aquí tu retroalimentación";
    textArea.rows = "1";
    content.appendChild(textArea);

    var starTitle = document.createElement("h4");
    starTitle.className = "dialog-review-title";
    starTitle.innerHTML = "Califica el verificentro";

    content.appendChild(starTitle);

    var starRating = document.createElement("div");
    starRating.className = "rating";

    var s = 0;
    while(s<5){
        var stars = document.createElement("span");
        stars.className = "star";
        stars.addEventListener('click', function(){
            var total=$(this).parent().children().length;
            var clickedIndex=$(this).index();
            dialogStarRating = 5 - clickedIndex;
            var starRating = $('.rating').find('span');
            starRating.removeClass('filled');
            for(var i=clickedIndex;i<total;i++){
                starRating.eq(i).addClass('filled');
        }});
        starRating.appendChild(stars);
        s++;
    }

    content.appendChild(starRating);

    var title = "Deja tu comentario";

    var buttons = [{type:BUTTON_CLOSE},{type:BUTTON_SEND,callback:sendReview}];
    createDialog(false, createCardWithTitle(title, content,buttons));
};


//// API functions /////

function getData(url){
    return $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json",
        processData: false});
}

function postData(url, data){
    return $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json",
        processData: false});
}

/////// Check if modern browser functions ///////
function isModernBrowser(){
    //February 2017 - TODO: update method whenever a browser is updated to a new version.
    //Flexbox compatibility list http://caniuse.com/#feat=flexbox

    //Allow browser from minimum version
    var operaVersion = 42;
    var firefoxVersion = 49;
    var safariVersion = 7;
    var chromeVersion = 49;

    //Get browser from user agent
    var browser = getBrowser();

    //Confirm browser - duck-typing
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || !isChrome && !isOpera && window.webkitAudioContext !== undefined;

    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    if (isOpera && browser.name == "Opera"){
        if(browser.version >= operaVersion){
            return true;
        }else{
            showBrowserDialog(false);
            return false;
        }
    }
    // Firefox 1.0+
    else if (isFirefox && browser.name == "Firefox"){
        if(browser.version >= firefoxVersion){
            return true;
        }else{
            showBrowserDialog(false);
            return false;
        }
    }
    // Safari 7+
    else if (isSafari && browser.name == "Safari"){
        if(browser.version >= safariVersion){
            return true;
        }else{
            showBrowserDialog(true);
            return false;
        }
    }
    // Internet Explorer 6-11 - We don't support Internet Explorer - Flexbox bugs
    else if (isIE && browser.name == "IE"){
        showBrowserDialog(false);
        return false;
    }
    // Edge 20+ - We support all versions
    else if(isEdge && browser.name == "Edge"){
        return true;
    }
    // Chrome 1+
    else if (isChrome && browser.name == "Chrome"){
        if(browser.version >= chromeVersion){
            return true;
        }else{
            showBrowserDialog(false);
            return false;
        }
    }
    // Blink engine detection - Chromium - We don't support this
    else if(isBlink){
        showBrowserDialog(false);
        return false;
    }
}

function getBrowser() {
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name:'IE',version:(tem[1] || '')};
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return {name:tem[1].replace('OPR', 'Opera'),version:tem[2]};
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return {name: M[0], version: M[1]};
}

function showBrowserDialog(isMacOS){
    var browsers = [
        ["../images/img_browser_chrome.svg","https://www.google.com/chrome/browser/", "Chrome"],
        ["../images/img_browser_firefox.svg","https://www.mozilla.org/en-US/firefox/products/", "Firefox"],
        ["../images/img_browser_safari.svg","https://support.apple.com/en-us/HT204416", "Safari"]];

    var content = document.createElement("div");
    content.className = "option-container";

    var subtitle = document.createElement("p");
    subtitle.className = "dialog-subtitle lowercase justified with-margin-bottom";
    subtitle.innerHTML = "Verifícalo fue desarrollado para darte la mejor experiencia posible con los exploradores de internet más nuevos." +
        " Te recomendamos actualizar tu navegador o utilizar alguno de los exploradores que recomendamos a continuación:";
    content.appendChild(subtitle);

    var browserContainer = document.createElement("div");
    browserContainer.className = "browsers-div";

    var length = browsers.length - 1;
    if(isMacOS){
        length = browsers.length;
    }

    for (var i=0; i<length;i++){
        var singleBrowserContainer = document.createElement("div");
        singleBrowserContainer.className = "dialog-browser";
        var imageLink = document.createElement("a");
        imageLink.href = browsers[i][1];
        var image = document.createElement("img");
        image.className = "dialog-browser-image";
        image.src = browsers[i][0];
        imageLink.appendChild(image);
        singleBrowserContainer.appendChild(imageLink);

        var downloadLink = document.createElement("a");
        downloadLink.className = "dialog-browser-link";
        downloadLink.href = browsers[i][1];
        downloadLink.innerHTML = "Descargar " + browsers[i][2];
        singleBrowserContainer.appendChild(downloadLink);


        browserContainer.appendChild(singleBrowserContainer);
    }
    content.appendChild(browserContainer);

    var continueText = document.createElement("p");
    continueText.className = "dialog-details with-margin-bottom";
    continueText.innerHTML = "De lo contrario puedes cerrar la ventana entendiendo que verifícalo puede no funcionar adecuadamente.";
    continueText.style.fontWeight = "bold";
    content.appendChild(continueText);

    var button = [{type:BUTTON_CLOSE}];
    dialogs.showCustomDialog("Recomendaciones de navegación",content,button,false);
}


//// Get paramter from URL /////

function getUrlParameter(parameter){
    var pageUrl = decodeURIComponent(window.location.search.substring(1)),
        pageUrlParameters = pageUrl.split('&'),
        parameterValue;

    for (var i = 0; i < pageUrlParameters.length; i++) {
        parameterValue = pageUrlParameters[i].split('=');

        if (parameterValue[0] === parameter) {
            return parameterValue[1] === undefined ? true : parameterValue[1];
        }
    }
}

///// Validate email ///////

function isRFC822ValidEmail(sEmail) {
    var sQtext = '[^\\x0d\\x22\\x5c\\x80-\\xff]';
    var sDtext = '[^\\x0d\\x5b-\\x5d\\x80-\\xff]';
    var sAtom = '[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+';
    var sQuotedPair = '\\x5c[\\x00-\\x7f]';
    var sDomainLiteral = '\\x5b(' + sDtext + '|' + sQuotedPair + ')*\\x5d';
    var sQuotedString = '\\x22(' + sQtext + '|' + sQuotedPair + ')*\\x22';
    var sSubDomain = '(' + sAtom + '|' + sDomainLiteral + ')';
    var sWord = '(' + sAtom + '|' + sQuotedString + ')';
    var sDomain = sSubDomain + '(\\x2e' + sSubDomain + ')*';
    var sLocalPart = sWord + '(\\x2e' + sWord + ')*';
    var sAddrSpec = sLocalPart + '\\x40' + sDomain; // complete RFC822 email address spec
    var sValidEmail = '^' + sAddrSpec + '$'; // as whole string

    var reValidEmail = new RegExp(sValidEmail);
    var atPosition = sEmail.indexOf('@');
    var dotPosition = sEmail.indexOf('.', atPosition);
    var emailSize = sEmail.length;

    var charValidation = !((atPosition==-1) || (dotPosition<(atPosition+2)) || (dotPosition+2>emailSize));

    return (reValidEmail.test(sEmail) && charValidation);
}


