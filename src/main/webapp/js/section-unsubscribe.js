(function($) {
    $(function() {
        var email = getUrlParameter("email");
        var placa = getUrlParameter("plate");
        if(isRFC822ValidEmail(email) && placa.length == 6){
            unsubscribeEmail(email,placa);
        }
        else{
            showRedirectDialog("Algo salió mal, intenta más tarde.")
        }
    });
})(jQuery);

function getData(url){
    return $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json",
        processData: false});
}

function unsubscribeEmail(email,placa){
    var url = "/apis/notifications/unsuscribe/" + email + "/" + placa;

    getData(url).always(function(response){
        switch (response.status){
            case 200:
                ga('send', 'event', "Baja de notificación", email, placa, "1");
                generateSucessText(email,placa);
                return true;
            break;
            case 404:
                ga('send', 'event', "Baja de notificación", email, placa, response.error);
                showRedirectDialog("No encontramos ninguna coincidencia en nuestra base de datos.");
                return false;
                break;
            default:
                ga('send', 'event', "Baja de notificación", email, placa, response.error);
                console.log(response);
                if(response == ""){
                    generateSucessText(email,placa);
                }
                else{
                    showRedirectDialog("Algo salió mal, intenta más tarde.");
                }
                return false;
            break;
         }
    });
}

function generateSucessText(email,placa){
    var content = document.getElementById("sucessful-text");
    var header = document.createElement("h2");
    header.innerHTML = "HAS ELIMINADO LA SUSCRIPCIÓN SATISFACTORIAMENTE";
    var paragraph = document.createElement("p");
    paragraph.innerHTML = "El correo <b>" + email + "</b> ya no cuenta con notificaciones para la placa <b>" + placa + "</b>";
    var button = document.createElement("button");
    button.innerHTML = "Regresar al inicio";
    button.addEventListener('click', function(){
        window.location.href = "/";
    });

    content.appendChild(header);
    content.appendChild(paragraph);
    content.appendChild(button);
}

function showRedirectDialog(text){
    var content = document.createElement("div");
    content.className = "option-container";

    var subtitle = document.createElement("h3");
    subtitle.className = "dialog-subtitle";
    subtitle.innerHTML = text;
    subtitle.style.marginBottom = "18pt";
    subtitle.style.textTransform = "normal";
    content.appendChild(subtitle);

    var title = "Lo sentimos";

    dialogs.showCustomDialog(title, content,false ,true);

    var div = document.createElement("div");
    div.className = "mdl-card__actions mdl-card--border dialog-container-btn";

    var createReloadwbtn = document.createElement("button");
    createReloadwbtn.className = "mdl-button mdl-js-button mdl-button--primary";
    createReloadwbtn.id = "dialog-reload-btn";
    createReloadwbtn.innerHTML = "Cerrar";
    createReloadwbtn.addEventListener('click', function(){
        window.location.href = "/";
    });
    div.appendChild(createReloadwbtn);

    $("#dialog-loading").find(".mdl-card").append(div);
}

