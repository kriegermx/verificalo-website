<%@taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <title><decorator:title /></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="description" content="Verifícalo" />
    <meta name="keywords" content="Verifícalo" />
    <link rel="stylesheet" href="/js/base-utils/v1.0.1/dialog-bootstrap.css"/>
    <link rel="icon" href="/favicon.ico">
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@SEDEMA_CDMX"/>
    <meta name="twitter:creator" content="@SEDEMA_CDMX"/>
    <meta name="image" content="http://verificalo.sedema.cdmx.gob.mx:9090/images/Portada.jpg"/>
    <meta property="og:url" content="http://verificalo.sedema.cdmx.gob.mx"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="<decorator:title />"/>
    <meta property="og:description" content="Ingresa la placa de tu auto y consulta el estado de su verificaci&oacute;n, el programa hoy no circula, infracciones y tenencias"/>
    <meta property="og:image" content="http://verificalo.sedema.cdmx.gob.mx:9090/images/Portada.jpg"/>
    <meta property="og:image:type" content="image/jpeg"/>
    <meta property="og:image:width" content="1920"/>
    <meta property="og:image:height" content="1080"/>
    <!-- Scripts -->
    <script src="/js/base-utils/v1.0.1/jquery-3.1.1.min.js"></script>
    <script src="/js/base-utils/v1.0.1/jquery.scrolly.min.js"></script>
    <script src="/js/base-utils/v1.0.1/bootstrap-3.3.7.min.js"></script>
    <script src="/js/base-utils/v1.0.1/utils-material-ui.js"></script>
    <script src="/js/base-utils/v1.0.1/utils-dialogs.js"></script>
    <script src="/js/base-utils/v1.0.1/util.js"></script>
    <script src="/js/search-utils.js"></script>
    <script src="/js/ga.js"></script>
    <decorator:head />
</head>

<body>
<div>
    <div class="upper-bar">
        <div class="upper-bar-elements">
            <a href="/"><img src="/images/img_logo_2.svg"></a>
            <div class="right-side">
                <p><b>PLACAS</b></p>
                <input class="input" type="text" id="license-plate" placeholder="e.g 999 ZZZ" maxlength="6" onkeyup="this.value = this.value.replace(/[^0-9a-z]/gi, '');">
                <button class="generic-button" id="license-plate-button" onclick="onLicensePlateSearch('#license-plate')"><i class="fa fa-search"></i><span class="text">CONSULTAR</span></button>
            </div>
        </div>
    </div>

    <decorator:body />
</div>
</body>
</html>

