<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>Verifícalo</title>
    <link rel="stylesheet" href="css/index.css"/>
</head>
<body>
<div class="content">
    <div class="section-wrapper" id="intro">
        <div class="middle-wrapper">
            <div class="inner-wrapper">
                <div class="fixed-width-container" id="intro-contents">
                    <img class="image-big" src="images/img_logo.svg">
                    <div class="start">
                        <p>Ingresa la placa de tu auto y consulta el estado de su verificación, el programa hoy no circula, infracciones y tenencias.</p>
                        <div class="input-button">
                            <input class="input" type="text" id="license-plate" placeholder="e.g 999 ZZZ" maxlength="6" onkeyup="this.value = this.value.replace(/[^0-9a-z]/gi, '');">
                            <button class="button" id="license-plate-button" onclick="onLicensePlateSearch('#license-plate')"><i class="fa fa-search"></i><span class="text">CONSULTAR</span></button>
                        </div>
                    </div>
                    <div class="icon-text">
                        <p class="text">CONOCE MÁS</p>
                    </div>
                    <div class="arrow-icon">
                        <a href="#how" class="goto-next scrolly">
                            <svg width="2em" viewBox="0 0 204.775 116.297">
                                <path class="arrow" d="M102.386,116.297c-3.682,0-7.226-1.452-9.844-4.07L4.072,23.754c-5.429-5.426-5.429-14.239,0-19.678c5.436-5.435,14.253-5.423,19.688,0l78.626,78.632l78.632-78.632c5.436-5.435,14.246-5.435,19.682,0c5.436,5.439,5.436,14.243,0,19.678l-88.477,88.473C109.625,114.845,106.08,116.297,102.386,116.297"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-wrapper" id="how">
        <div class="middle-wrapper">
            <div class="inner-wrapper">
                <div class="fixed-width-container" id="how-contents">
                    <div class="instructions row">
                        <h2>¿CÓMO FUNCIONA VERIFÍCALO?</h2>
                        <div class="line"></div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <img class="image-big" src="images/img_decor_index_1.svg">
                            <p>INGRESA TUS PLACAS</p>
                            <p>No es necesario crear una cuenta</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <img class="image-big" src="images/img_decor_index_2.svg">
                            <p>INFÓRMATE</p>
                            <p>Entérate del estado de la verificación de tu vehículo y más</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <img class="image-big" src="images/img_decor_index_3.svg">
                            <p>PONTE AL CORRIENTE</p>
                            <p>Consulta requisitos y recibe recordatorios directo a tu mail</p>
                        </div>
                    </div>
                    <div class="arrow-icon">
                        <a href="#who" class="goto-next scrolly">
                            <svg width="2em" viewBox="0 0 204.775 116.297">
                                <path class="arrow" d="M102.386,116.297c-3.682,0-7.226-1.452-9.844-4.07L4.072,23.754c-5.429-5.426-5.429-14.239,0-19.678c5.436-5.435,14.253-5.423,19.688,0l78.626,78.632l78.632-78.632c5.436-5.435,14.246-5.435,19.682,0c5.436,5.439,5.436,14.243,0,19.678l-88.477,88.473C109.625,114.845,106.08,116.297,102.386,116.297"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-wrapper" id="who">
        <div class="middle-wrapper">
            <div class="inner-wrapper">
                <div class="fixed-width-container" id="who-contents">
                    <div class="align-center">
                        <h2>VERIFÍCALO ES UN PRODUCTO DESARROLLADO POR LA SECRETARÍA DE MEDIO AMBIENTE</h2>
                        <div class="line"></div>
                        <p><b>Av. Tlaxcoaque No. 8, Planta Baja, Col. Centro, CP 06090<br>Delegación Cuauhtémoc, Ciudad de México.</b></p>
                        <h3><span class="purple-text">ATENCIÓN CIUDADANA</span></h3>
                        <p><b>Teléfonos: 52-78-99-31 ext. 1690 | Horario: Lunes a Viernes 9:00 a 15:00 horas<br>hoynocircula@sedema.cdmx.gob.mx</b></p>
                        <div class="link-container">
                            <a href="dashboard/faq.jsp"><b>PREGUNTAS FRECUENTES</b></a>
                            <p class="column-char left-padding">|</p>
                            <a class="left-padding" href="dashboard/map.jsp"><b>VERIFICITAS CDMX</b></a>
                        </div>
                        <div class="social-icons">
                            <a href="https://www.youtube.com/channel/UCzYbjrNZgUZ16NXvzoehAuQ" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/Secretar%C3%ADa-del-Medio-Ambiente-120208740359/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/SEDEMA_CDMX" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                        <img src="images/img_logo_CDMX_1.svg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/search-utils.js"></script>
<script src="/js/utils.js"></script>
<script src="js/section-index.js"></script>

<s:if test="message != null">
<script>
    showErrorDialog("<s:property value="message"/>");
</script>
</s:if>

</body>
</html>