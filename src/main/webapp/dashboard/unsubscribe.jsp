<!DOCTYPE html>
<html lang="es">
<head>
    <title>Desuscribir notificaciones</title>
    <link rel="stylesheet" href="/css/unsubscribe.css"/>
    <script src="/js/utils.js"></script>
</head>
<body>
<div class="content">
    <div class="wrapper">
        <div id="sucessful-text" class="inner"></div>
    </div>
</div>

<script src="/js/section-unsubscribe.js"></script>

</body>