<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Verifícalo</title>
    <link rel="stylesheet" href="/css/map.css"/>
    <link rel="stylesheet" href="/css/leaflet.css"/>
    <script src="/js/leaflet.js"></script>
    <script src="/js/leaflet.ajax.min.js"></script>
    <script src="/js/utils.js"></script>
    <link rel="stylesheet" href="/js/base-utils/v1.0.1/dialog-bootstrap.css"/>
</head>
<body class="with-loader is-loading">
<div class="loader">
    <div class="item-container">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>
</div>
<div class="content">
    <div class="content-wrapper">
        <div class="panel-container" id="panel-container">
            <div class="panel-toggle" id="panel-toggle">
                <button id="button-toggle" class="" onclick="togglePanelContent()">VER VERIFICENTROS  <i class="fa fa-chevron-up" aria-hidden="true"></i></button>
            </div>
            <div class="panel-title">
                <div class="panel-title-text">
                    <a href="/"><img src="/images/img_logo_CDMX_2.svg"></a>
                    <h4>SISTEMA VERIFICITAS CDMX</h4></div>
            </div>
            <div class="panel-filters">
                <div class="panel-filters-text">
                    <div class="filter-neighborhood-text"><h5>Delegación</h5></div>
                    <div class="filter-type-text"><h5>Tipo</h5></div>
                </div>
                <div class="panel-filters-buttons">
                    <div class="filter-neighborhood-button"><button class="generic-button" id="neighborhoodButton" type="submit" onclick="filterClicked(1)">TODAS <i class="fa fa-caret-down" aria-hidden="true"></i></button></div>
                    <div class="filter-type-button"><button class="generic-button" id="typeButton" type="submit" onclick="filterClicked(0)">TODOS <i class="fa fa-caret-down" aria-hidden="true"></i></button></div>
                </div>
            </div>
            <div class="panel-list">
                <table class="centers-table">
                    <thead>
                        <th>CLAVE</th>
                        <th>NOMBRE</th>
                        <th>CALIFICACIÓN</th>
                    </thead>
                    <tbody id="centers-table-content"></tbody>
                </table>
            </div>
            <div id="update-date" class="update-date"></div>
        </div>
        <div class="map-container" id="map-container">
            <div id="mapid" class="map"></div>
        </div>
    </div>
</div>


<script src="/js/section-map.js"></script>

</body>