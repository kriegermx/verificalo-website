<!DOCTYPE html>
<html lang="es">
<head>
    <title>Preguntas Frecuentes</title>
    <link rel="stylesheet" href="/css/faq.css"/>
    <script src="/js/utils.js"></script>
</head>
<body>
<div class="content">
    <div class="wrapper">
        <div class="inner">
            <div>
                <h2>PREGUNTAS FRECUENTES</h2>
                <p><b>Verifícalo</b></p>
                <p>Verifícalo es una aplicación web que te informa cómo y cuándo debes cumplir con tus obligaciones de automovilista, como la verificación vehicular y el pago de impuestos e infracciones.</p>
                <p>Esta iniciativa nació en la primera edición del programa Código CDMX gracias al trabajo realizado por el equipo del hacker cívico Manuel Rábade.</p>
                <p><b>Objetivo y aplicación del programa de Verificación Vehicular</b></p>
                <p><a href="http://www.sedema.cdmx.gob.mx/storage/app/media/programas/hoy-no-circula/decreto-programa-hoy-no-circula-segundo-semestre-2016.pdf" target="_blank">Decreto por el que se expide el Programa Hoy No Circula en el Distrito Federal​​</a></p>
                <p><b>Aplicación del programa - Hoy no circula</b></p>
                <p>En la Zona Metropolitana del Valle de México, que incluye las 16 delegaciones de la Ciudad de México y en 18 municipios conurbados del Estado de México (Atizapán de Zaragoza, Coacalco de Berriozábal, Cuautitlán, Cuautitlán Izcalli, Chalco, Chicoloapan, Chimalhuacán, Ecatepec de Morelos, Huixquilucan, Ixtapaluca, La Paz, Naucalpan de Juárez, Nezahualcóyotl, Nicolás Romero, Tecámac, Tlalneplantla de Baz, Tultitlán y Valle de Chalco).</p>
                <p>Asimismo, quedan obligados a observar el presente Programa, los responsables de operar los Centros de Verificación de Emisiones Vehiculares comúnmente conocidos como “Verificentros”, los proveedores de equipo de verificación de emisiones vehiculares, los responsables de operar Talleres de Diagnóstico y Reparación Automotriz e Instalación de Convertidores Catalíticos de tres vías para el Programa Integral de Reducción de Emisiones Contaminantes comúnmente conocidos como talleres PIREC, así como los propietarios, poseedores o conductores de vehículos automotores matriculados fuera de la Ciudad de México que circulan en vialidades de la misma.</p>
                <p>El presente Programa aplica a todos los vehículos automotores matriculados y/o que circulen en el territorio de la Ciudad de México, y los que porten placas metropolitanas, con excepción de los tractores agrícolas, la maquinaria dedicada a las industrias de la construcción y minera, las motocicletas, los vehículos eléctricos, los vehículos híbridos con motores de propulsión a gasolina y eléctrico, los vehículos con matrícula de auto antiguo y/o clásico, automotores con matrícula de demostración y/o traslado y aquellos cuya tecnología impida la aplicación de la Norma Oficial Mexicana correspondiente, mismos que podrán circular todos los días.</p>
                <p><b>Calendario de verificación</b></p>
                <p>Los vehículos deberán realizar y aprobar la verificación de emisiones vehiculares cada semestre, salvo para el caso de los que obtengan un holograma doble cero “00”, en cuyo caso la unidad estará exenta de la obligación de verificar sus emisiones hasta por tres semestres de verificación vehicular posteriores al semestre en que se obtuvo.</p>
                <p>Los vehículos matriculados en la Ciudad de México y que ya han sido verificados en sus emisiones vehiculares en su período próximo anterior, deberán continuar verificando conforme al color del engomado o al último dígito numérico de las placas de circulación del vehículo en los siguientes términos:</p>
                <p><b>Calendario de Verificación para el primer semestre de 2017</b></p>
                <img src="../images/tabla_Engomados.svg"><br><br>
                <p><a href="/dashboard/map.jsp" target="_blank">Para agendar una cita click aquí</a></p>
                <p><b>¿Los hologramas de Hidalgo, Morelos, Puebla y Tlaxcala serán válidos?</b></p>
                <p>Sí, todos los hologramas emitidos hasta el 30 de junio de 2016 serán reconocidos por la CDMX y válidos hasta concluir su vigencia.</p>
                <p>Ejemplo 1: Realizó su última verificación el día 11 de enero de 2016 obteniendo un holograma tipo cero “0”, el cual tiene una vigencia de 6 meses. Por lo anterior, será válido hasta el 31 de agosto de 2016, y podrá realizar una verificación voluntaria y circular de acuerdo al holograma obtenido.</p>
                <p>Ejemplo 2: Realizó su última verificación el día 30 de junio de 2014 obteniendo un holograma tipo doble cero “00”, el cual tiene una vigencia de 2 años, será válido hasta el 30 de junio de 2016. Por lo anterior, su holograma no está vigente y le aplica el Programa Hoy No Circula como vehículo foráneo.</p>
                <p><b>¿Cómo aplica el Programa Hoy No Circula para vehículos foráneos?</b></p>
                <p>Los vehículos foráneos no circulan de lunes a viernes en un horario de 5:00 a 11:00 horas, no circulan ningún sábado en un horario de 5.00 a 22:00 horas y de acuerdo a la terminación de su placa no circulan un día entre semana en un horario de 5.00 a 22:00 horas.</p>
                <p><b>¿Vehículos matriculados en Hidalgo, Morelos, Puebla y Tlaxcala pueden realizar verificación voluntaria en la Ciudad de México?</b></p>
                <p>Sí, cualquier vehículo puede realizar una verificación voluntaria en los Centros de Verificación Vehicular Autorizados para operar en la Ciudad de México con excepción de los vehículos matriculados en el Estado de México.</p>
                <p><b>¿Vehículos matriculados en el Estado de México pueden realizar verificación voluntaria en la Ciudad de México?</b></p>
                <p>No, por mutuo acuerdo los Centros de Verificación Vehicular Autorizados para operar en el Estado de México no verifican el parque vehicular de la CDMX y viceversa.</p>
                <p><b>¿Qué vigencia tiene el holograma tipo doble cero “00”?</b></p>
                <p>El holograma tipo doble cero “00” tienen una vigencia de dos años, los hologramas tipo cero “0”, tipo uno “1”y tipo dos “2” tienen una vigencia de 6 meses.</p>
                <p><b>¿Cuántas veces se puede obtener el holograma doble cero “00”?</b></p>
                <p>El holograma tipo doble cero “00” podrá obtenerse hasta por dos ocasiones. Al término de la vigencia de los primero dos años, se podrá otorgar por única ocasión un nuevo holograma por 2 años más, siempre y cuando se lleve a cabo una prueba por el método de Sistema de Diagnóstico a Bordo, no presente falla en los monitores correspondientes y cumpla con los requisitos establecidos en programa de verificación vigente.</p>
                <p><b>Si mi vehículo cumple con los límites de emisiones establecidos para la obtención de un holograma tipo cero “0” pero no paso la medición del Sistema de Diagnóstico a Bordo (SDB) ¿qué holograma me corresponde?</b></p>
                <p>Cuando resulte con código de fallas en la prueba SDB en el segundo intento pero sus emisiones cumplen con los límites establecidos en el numeral 7.4.1, se emitirá holograma 0. Los vehículos automotores a gasolina o a gas natural como combustible original de fábrica que conforme a la NOM-EM-167-SEMARNAT-2016 cuenten con convertidor catalítico de 3 vías y Sistema de Diagnóstico a Bordo (SDB) y que no puedan realizar la prueba del Sistema de Diagnóstico a Bordo (SDB) por carecer de los conectores correspondientes o por no tener debidamente soportados, disponibles o habilitados los monitores especificados se les realizará la prueba dinámica o estática, según corresponda a los límites máximos de emisión, para obtener el holograma correspondiente, avisando al usuario que para poder acceder al holograma 0 en el primer semestre del 2017, el sistema SDB deberá estar habilitado y no presentar código fallas.</p>
                <p><b>Si mi vehículo aprueba la medición OBDII pero no cumple con los límites de emisiones establecidos para la obtención de un holograma ¿qué holograma me corresponde?</b></p>
                <p>En caso de no cumplir con los límites de emisiones establecidos en el Programa se emitirá Rechazo.</p>
                <p><b>¿Los vehículos matriculados en Hidalgo, Morelos, Puebla y Tlaxcala pueden obtener pase turístico?</b></p>
                <p>No, los vehículos matriculados en estas entidades pueden realizar una verificación voluntaria en la CDMX.</p>
                <p><b>¿Podrá circular un vehículo durante los 15 días de prórroga que otorgue el sistema cuando sea rechazado por SDB?</b></p>
                <p>Cuando el vehículo acuda a verificar en los últimos 15 días naturales del periodo que le corresponde, siendo este su primer intento y obtenga rechazo por SDB, podrá verificar dentro de los 15 días naturales posteriores a la fecha de dicho rechazo respetando las limitaciones a la circulación establecidas en el Programa Hoy No Circula.</p>
                <p><b>¿Cómo funcionará el proceso de verificación vehicular para el segundo semestre 2016?</b></p>
                <p>Durante el proceso de verificación de emisiones vehiculares, los vehículos podrán obtener, con base en las especificaciones del Programa de Verificación Vehicular Obligatorio para el Segundo Semestre 2016, las Constancias de Verificación tipo “00”, “0”, “1” o “2”, en caso de aprobar el proceso de revisión visual de componentes vehiculares, revisión de monitores del Sistema de Diagnóstico a Bordo (SDB), revisión visual de humo, así como presentar niveles de emisión iguales o menores conforme a lo establecido en la Norma NOM-EM-167-SEMARNAT 2016 y del Programa.</p>
                <p><b>¿Qué es el Sistema de Diagnóstico a Bordo (SDB)?</b></p>
                <p>Es un sistema electrónico integrado al vehículo, diseñado para diagnosticar el funcionamiento de los monitores relacionados con el control de emisiones vehiculares.</p>
                <p><b>¿Qué modelos cuentan con el Sistema de Diagnóstico a Bordo (SDB)?</b></p>
                <p>De acuerdo a la norma NOM-EM-167-SEMARNAT 2016 los modelos 2006 y posteriores cuentan con Sistema de Diagnóstico a Bordo.</p>
                <p><b>¿Qué pasa si el Sistema de Diagnóstico a Bordo (SDB) de mi automóvil no funciona?</b></p>
                <p>Se emitirá un rechazo por SDB. El vehículo podrá repetir la prueba después del rechazo. Cuando resulte con código de fallas en la prueba SDB en el segundo intento pero si sus emisiones cumplen con los límites establecidos en el programa de verificación vehicular para segundo semestre 2016, se emitirá holograma 0. Cuando el vehículo regrese por su segundo intento y resulte sin código de fallas en SDB pero sus emisiones no correspondan a los límites establecidos para este holograma, se emitirá holograma 1 o rechazo, de acuerdo a sus emisiones.</p>
                <p><b>¿Si mi vehículo cuenta con placas de alguno de los Estados que integran la CAMe: Hidalgo, Morelos, Puebla y Tlaxcala o alguno con los que se tiene firmado convenio como Guanajuato, Michoacán y Querétaro, y del Extranjero, podre circular de acuerdo al holograma obtenido en mi estado?</b></p>
                <p>El Gobierno de la Ciudad de México reconocerá los hologramas que hayan sido emitidos durante el Programa de Verificación Vehicular Obligatoria Vigente hasta el Primer Semestre de 2016 a los vehículos automotores matriculados en entidades federativas integrantes de la CAMe, así como aquellas que hayan celebrado convenios específicos para el reconocimiento de dichos hologramas, hasta que concluya sus vigencias.</p>
                <p>El Gobierno de la Ciudad de México reconocerá los hologramas de verificación vehicular otorgados por el Gobierno del Estado de México y por verificación voluntaria en la ZMVM.</p>
                <p><b>¿Se puede verificar nuevamente para mejorar el holograma obtenido?, de ser así, ¿Tendrá algún costo y exime de la verificación del periodo?</b></p>
                <p>El propietario podrá verificar su vehículo antes de su periodo de verificación, con el fin de acceder a un holograma con mayores beneficios de circulación, debiendo pagar la verificación respectiva. El hecho de obtener la Constancia de Verificación aprobatoria de manera anticipada no exime al propietario del vehículo de cumplir con su verificación en el periodo que le corresponda.</p>
                <p><b>Sanciones</b></p>
                <p>Los vehículos y conductores de fuentes móviles o vehículos que circulen en las vialidades y calles del Distrito Federal que infrinjan las medidas previstas en este Programa, se harán acreedores a las sanciones establecidas en el Reglamento de la Ley Ambiental del Distrito Federal en Materia de Verificación Vehicular; así como, en el Reglamento de Tránsito Metropolitano o el que le sustituya y demás disposiciones aplicables, sin perjuicio de que sean retirados de circulación y remitidos al depósito vehicular, en el que deberán permanecer hasta que se pague la multa correspondiente, y en el caso de los vehículos detenidos durante Contingencia Ambiental, también habrá que esperar a que la misma concluya.</p>
                <p>El Reglamento de la Ley Ambiental del Distrito Federal en Materia de Verificación Vehicular establece:</p>
                <p>“(…) Artículo 45. -La multa por circular con vehículos automotores en un día que tengan restringida la circulación conforme a lo dispuesto en la Ley, en este Reglamento, en acuerdos, programas o cualquier otra disposición jurídica aplicable, será de 24 días de Salario Mínimo vigente en el Distrito Federal. (…)”</p>
                <p><b>Denuncia en caso de abusos en los Verificentros</b></p>
                <p>En caso de algún abuso en Verificentros de la Ciudad de México, podrá llamar desde el Verificentro ya que todos cuentan con línea directa al Centro de Inspección y Vigilancia Remota (CIVAR), el teléfono es gratuito y se encuentra en una caseta identificada con el nombre de “VERIFICATEL”. La llamada podrá ser atendida y vigilada a través del CIVAR.</p>
                <p>También puede llamar al teléfono 52789931 ext 4550 desde cualquier lugar, en un horario de 8:00 a 20:30 horas y/o levantar una denuncia ante la Dirección General de Vigilancia Ambiental de la Secretaría del Medio Ambiente, de forma personal por Oficialía de Partes en el Edificio Juana de Arco ubicado en Tlaxcoaque No.8, Col. Centro Histórico, Delegación Cuauhtémoc, en planta baja, de lunes a viernes, en un horario de 9:00 am a 1:30 pm.</p>
                <p>Los datos necesarios para dar seguimiento a una denuncia son:</p>
                <li>Nombre completo y datos de ubicación del denunciante. (Indicar dirección, teléfono y/o dirección de correo electrónico del denunciante para poder contactarlo de ser necesario).</li>
                <li>Lugar de los hechos. (Señalar calle, número, colonia, Delegación, Código Postal o datos que permitan ubicar el predio).</li>
                <li>Hechos denunciados. (Describir las obras o actividades que se denuncian y las afectaciones al medio ambiente en su caso).</li>
                <li>Momento en que se suscitan los hechos. (Indicar cuando ocurrieron los hechos, o en su caso cuando iniciaron en caso de que sean continuos o permanentes).</li>
                <li>Personas presuntas responsables. (Señalar el nombre de las personas responsables de los hechos).</li>
                <li>Información complementaria. (referir si se tienen información respecto de permisos, licencias o autorizaciones que en su caso se hayan expedido por las autoridades).</li>
                <li>Elementos probatorios. (Referir y anexar todas las pruebas que se tengan para acreditar los hechos, como son: documentos, fotografías y videos, entre otros).</li>
                <p>​A quien acudir en caso de abusos por parte de la policía, Sistema de denuncia ciudadana de la Contraloría General de la Ciudad de México:</p>
                <p><a href="http://www.anticorrupcion.cdmx.gob.mx/index.php/sistema-de-denuncia-ciudadana" target="_blank">http://www.anticorrupcion.cdmx.gob.mx/index.php/sistema-de-denuncia-ciudadana</a></p>
                <p>Denuncia de irregularidades al 5627 9739</p>
                <p>En caso de algún abuso por personal de tránsito de la Ciudad de México, comunícate a:</p>
                <p>Secretaría de Seguridad Pública, Dirección General de Inspección Policíaca 01 (55) 5242 5000 Ext. 1120, 1121, 1122 y 1171.</p>
                <p>En caso de algún abuso por personal de tránsito del Estado de México, comunícate a:</p>
                <p>Sistema de Atención Mexiquense (SAM) <a href="http://www.secogem.gob.mx/SAM/sit_atn_mex.asp" target="_blank">www.secogem.gob.mx/SAM/sit_atn_mex.asp</a>, o ingresando, desde el portal del Gobierno del Estado de México <a href="http://www.edomex.gob.mx/" target="_blank">www.edomex.gob.mx</a></p>
                <p>Centro de Atención Telefónica del Gobierno del Estado de México (CATGEM), disponible las 24 horas del día, los 365 días del año por:</p>
                <p>Chat en línea por <a href="http://www.edomex.gob.mx/" target="_blank">www.edomex.gob.mx</a>, ingresando en Ventanilla Electrónica Única de Trámites y Servicios,</p>
                <p>Teléfono, lada sin costo 01 800 696 9696, y Valle de Toluca al 070.</p>
                <p><b>Pase Turístico</b></p>
                <p>El Pase se otorga de manera gratuita, pudiendo obtenerse un Pase Turístico Metropolitano por 7 días continuos (2 veces al semestre) o por 14 días continuos (una vez al semestre).</p>
                <p>Para obtenerlo deberá entrar a la página de Pase Turístico a través del portal electrónico de la Secretaría del Medio Ambiente de la Ciudad de México <a href="http://www.paseturistico.cdmx.gob.mx/pasetur/" target="_blank">http://www.paseturistico.cdmx.gob.mx/pasetur/</a>. Es necesario que cuente con una dirección de correo electrónico personal y tener a la mano la tarjeta de circulación de su vehículo.</p>
                <p>El procedimiento para tramitar un Pase Turístico es:</p>
                <li>Entrar a la página de Pase Turístico - <a href="http://www.paseturistico.cdmx.gob.mx/pasetur/" target="_blank">http://www.paseturistico.cdmx.gob.mx/pasetur/</a></li>
                <li>Iniciar su registro al capturar su correo electrónico.</li>
                <li>El Sistema de Pase Turístico le enviará un correo con una dirección electrónica a su correo registrado.</li>
                <li>Ingresar a la liga electrónica y llenar el registro con sus datos generales. Cuando envíe esta información, el sistema le enviará a su vez otro correo (a su correo registrado) con una clave de acceso.</li>
                <li>Con su correo electrónico y su clave de acceso podrá ingresar al sistema.</li>
                <li>En la pestaña Registro de Automóviles deberá registrar los datos de su vehículo que se encuentran en la tarjeta de circulación. Al capturar la placa solo utilizar números y letras, sin dejar espacios en blanco.</li>
                <li>Cuando haya registrado los datos de su vehículo podrá generar su pase, indicando si lo requiere por 7 ó 14 días. Es necesario considerar la fecha en que el vehículo circulará en algún municipio de la Zona Metropolitana del Valle de México, para que el periodo del pase contemple esta fecha.</li>
                <li>Con los datos ingresados al sistema se generarán dos pases turísticos, uno emitido por la Ciudad de México y otro por el Estado de México. Una vez tramitado el Pase, lo deberá imprimir (los dos oficios generados) y pegarlos en un lugar visible en el vehículo durante el periodo de vigencia de los mismos.</li>
            </div>
            <div id="footer">
                <div class="footer-logo-container">
                    <img src="/images/img_logo_CDMX_1.svg">
                </div>
                <div class="footer-info-container">
                    <p>Av. Tlaxcoaque No. 8, Planta Baja, Col. Centro, CP 06090. Delegación Cuauhtémoc, Ciudad de México.</p>
                    <p>Teléfonos: 52-78-99-31 ext. 1690 | Horario: Lunes a Viernes 9:00 a 15:00 horas</p>
                    <p>hoynocircula@sedema.cdmx.gob.mx</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>