<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Verifícalo</title>
    <link rel="stylesheet" href="/css/dashboard.css"/>
    <script src="/js/moment.js"></script>
</head>
<body>
<div class="content">
    <s:if test="verifications.size()>0">
    <div class="wrapper" id="car-information">
        <div class="inner">
            <div class="info-container">
                <div>
                    <h6>Modelo del vehículo</h6>
                    <p><s:property value="carDetails.model"></s:property></p>
                </div>
                <div>
                    <h6>No. de Serie</h6>
                    <p><s:property value="carDetails.serialNumber"></s:property></p>
                </div>
                <div>
                    <h6>Holograma</h6>
                    <p><s:property value="carDetails.hologram"></s:property></p>
                </div>
                <div>
                    <h6>Engomado</h6>
                    <div class="color-container <s:property value="carDetails.gummed"></s:property>">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
    </s:if>
    <div class="wrapper" id="main-container">
        <div class="inner">
            <div id="no-drive-days-calendar" <s:if test="verifications.size()==0">style="display: none;"</s:if>>
                <div class="calendar-indicators">
                    <h4>Programa hoy no circula</h4>
                    <div id="no-drive-day-indicator" class="drive-legend-type">
                        <div class="no-drive-indicator-circle"></div>
                        <div class="no-drive-indicator-text"><p>NO CIRCULA</p></div>
                    </div>
                    <div id="today-indicator" class="drive-legend-type">
                        <div class="today-indicator-circle"></div>
                        <div class="today-indicator-text"><p>DÍA ACTUAL</p></div>
                    </div>
                </div>
                <div class="weekly-calendar">
                    <div class="month">
                        <ul>
                            <li class="prev" onclick="calendarNavigationClicked(0)">&#10094;</li>
                            <li class="next" onclick="calendarNavigationClicked(1)">&#10095;</li>
                            <li id="showed-month">Error</li>
                        </ul>
                    </div>
                    <ul class="weekdays">
                        <li>DO</li>
                        <li>LU</li>
                        <li>MA</li>
                        <li>MI</li>
                        <li>JU</li>
                        <li>VI</li>
                        <li>SA</li>
                    </ul>
                    <ul class="days" id="calendar-days">
                        <li><span>0</span></li>
                        <li><span>0</span></li>
                        <li><span>0</span></li>
                        <li><span>0</span></li>
                        <li><span>0</span></li>
                        <li><span>0</span></li>
                        <li><span>0</span></li>
                    </ul>
                </div>
            </div>

            <div id="no-drive-warning" style="display: none">
                <span>ESTE AUTOMÓVIL NO PUEDE CIRCULAR</span>
            </div>

            <div id="allow-to-drive-warning" style="display: none">
                <span>ESTE AUTOMÓVIL PUEDE CIRCULAR TODOS LOS DÍAS</span>
            </div>

            <div class="status-card">
                <div class="title" onclick="toggleContent(this)" id="ec-title">
                    <h3>VERIFICACIÓN</h3>
                    <span id="ec-subtitle"></span>
                    <i class="fa fa-chevron-up" aria-hidden="true"></i>
                </div>
                <div class="content" id="ec-container">
                </div>
            </div>

            <div class="status-card">
                <div class="title <s:if test="tickets.size()>0">red</s:if><s:else>green</s:else>" onclick="toggleContent(this)">
                    <h3>INFRACCIONES</h3>
                    <span><s:if test="tickets.size()>0">Tienes  <s:property value ="tickets.size()"/> infracciones</s:if>
                        <s:else>Al corriente</s:else></span>
                    <i class="fa fa-chevron-up" aria-hidden="true"></i>
                </div>
                <div class="content with-list">
                    <s:if test="tickets.size()>0">
                        <p>Tu automóvil tiene las siguientes infracciones, recuerda que debes realizar el pago correspondiente antes de continuar con tu proceso de verificacion:</p>
                        <s:iterator value="tickets">
                            <div class="list-element">
                                <div class="list-element-details">
                                    <div>
                                        <div>
                                            <p class="description"><span class="simple-label">Motivo:</span><s:property value="reason"/></p>
                                        </div>
                                    </div>
                                    <div class="flex-1">
                                        <div>
                                            <p class="description"><span class="simple-label">Folio:</span><s:property value="id"/></p>
                                        </div>
                                        <div>
                                            <p class="description"><span class="simple-label">Fecha:</span><s:property value="date"/></p>
                                        </div>
                                    </div>
                                    <div class="flex-1">
                                        <div>
                                            <p class="description"><span class="simple-label">Fundamento:</span>Artículo: <s:property value="article"/>, Fracción: <s:property value="fraction"/></p>
                                        </div>
                                        <div>
                                            <p class="description"><span class="simple-label">Sanción:</span><s:property value="penalty"/></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="list-element-button">
                                    <div class="button-container">
                                        <button onclick="onPaymentClick(<s:property value="id"/>)">Pagar</button>
                                    </div>
                                </div>
                            </div>
                        </s:iterator>
                    </s:if>
                    <s:else>
                        <p>Tu automóvil no tiene infracciones pendientes de pago</p>
                    </s:else>
                </div>
            </div>

            <div class="status-card">
                <div class="title <s:if test="taxes.hasPendingTaxes">red</s:if><s:else>green</s:else>" onclick="toggleContent(this)">
                    <h3>TENENCIA</h3>
                    <s:if test="taxes.hasPendingTaxes">
                        <span><s:property value="taxes.numberOfPendingYears"/> pagos pendientes</span>
                    </s:if>
                    <s:else>
                        <span>Al corriente</span>
                    </s:else>
                    <i class="fa fa-chevron-up" aria-hidden="true"></i>
                </div>
                <div class="content">
                    <s:if test="taxes.hasPendingTaxes">
                        <p>Tu pagos de tenencia no están al corriente. Debes pagar tu tenencia de los años <b><s:property value="taxes.years"/></b>.</p>
                        <div class="button-container"><button onclick="onPaymentTaxClick()">Pagar</button></div>
                    </s:if>
                    <s:else>
                        <p>Tu pago de tenencia está al corriente, tu siguiente pago está programado para el primer trimestre de cada año.</p>
                    </s:else>
                </div>
            </div>

            <div class="status-card">
                <div class="title" onclick="toggleContent(this)">
                    <h3>RECORDATORIOS</h3>
                    <i class="fa fa-chevron-up" aria-hidden="true"></i>
                </div>
                <div class="content notifications-content">
                    <p>Suscríbete a las notificaciones para la placa <b><s:property value="plateNumber"/></b>:</p>
                    <div class="notification-input-container">
                        <input type="checkbox" name="NotificacionesHoyNoCircula" id="NotificacionesHoyNoCircula" value="NotificacionesHoyNoCircula" onchange="checkboxToggle(this)" checked="true">
                        <label for="NotificacionesHoyNoCircula">Hoy no Circula</label>
                        <input type="checkbox" name="RecordatorioPagoTenencia" id="RecordatorioPagoTenencia" value="RecordatorioPagoTenencia" onchange="checkboxToggle(this)" checked="true">
                        <label for="RecordatorioPagoTenencia">Pago de tenencia</label>
                        <input type="checkbox" name="notifications" id="RecordatorioVerificacion" value="RecordatorioVerificacion" onchange="checkboxToggle(this)" checked="true">
                        <label for="RecordatorioVerificacion">Verificación</label>
                        <input type="checkbox" name="notifications" id="Todas" value="Todas" onchange="checkboxAllToggle(this)" checked="true">
                        <label for="Todas">Todas</label>
                    </div>
                    <div class="email-notification-container">
                        <p><b>Correo electrónico</b></p>
                        <input class="input" type="email" id="email" placeholder="e.g sedema@cdmx.gob.mx">
                        <button id="email-notification-button" onclick="suscribeEmail('<s:property value="plateNumber"/>')">SUSCRIBIRSE</button>
                    </div>
                    <div class="privacy-container">
                        <a href="http://sedema.cdmx.gob.mx/terminos-y-condiciones#Datos-personales" target="_blank">Política de privacidad</a>
                    </div>
                </div>
            </div>
            <div id="footer">
                <div class="footer-logo-container">
                    <img src="/images/img_logo_CDMX_1.svg">
                </div>
                <div class="footer-info-container">
                    <p>Av. Tlaxcoaque No. 8, Planta Baja, Col. Centro, CP 06090. Delegación Cuauhtémoc, Ciudad de México.</p>
                    <p>Teléfonos: 52-78-99-31 ext. 1690 | Horario: Lunes a Viernes 9:00 a 15:00 horas</p>
                    <p>hoynocircula@sedema.cdmx.gob.mx</p>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    var userCarDetails = {
        hologram: "<s:property value="carDetails.hologram"/>",
        gummedColor: "<s:property value="carDetails.gummed"/>",
        hologramParity: "<s:property value="carDetails.hologramParity"/>",
        isTaxi: <s:property value="isTaxi"/>,
        plateNumber: "<s:property value="plateNumber"/>"
    };

    window.hasPendingTaxes = <s:if test="taxes.hasPendingTaxes">true</s:if><s:else>false</s:else>;
    window.hasPendingTickets = <s:if test="tickets.size()>0">true</s:if><s:else>false</s:else>;
    window.numberOfTaxDebts = <s:property value="taxes.numberOfPendingYears"/>;
    window.numberOfTicketDebts = <s:property value="tickets.size()"/>;

    window.environmentalChecks = [];
    <s:iterator value="verifications">
        window.environmentalChecks.push({
                vDate: "<s:property value="verificationDate"/>",
                vExpiration: "<s:property value="validity"/>",
                rejectionCause: "<s:property value="rejectionCause"/>",
                cancelled: "<s:property value="cancelled"/>",
                verificationTime: "<s:property value="verificationTime"/>",
                certificate: "<s:property value="certificade"/>",
                verificationCenter: "<s:property value="verificenter"/>",
                equipment: "<s:property value="GDFteam"/>",
                line: "<s:property value="line"/>",
                result: "<s:property value="result"/>"
        });
    </s:iterator>
</script>
<script src="/js/utils.js"></script>
<script src="/js/section-dashboard.js"></script>
</body>