package mx.krieger.verificalo.website.exceptions;

/**
 * Created by me on 06/01/17.
 */
public class FeedbackAPIException extends Exception {

    public FeedbackAPIException(String message) {
        super(message);
    }
}
