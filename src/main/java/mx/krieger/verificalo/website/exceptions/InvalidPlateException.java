package mx.krieger.verificalo.website.exceptions;

/**
 * Created by me on 06/01/17.
 */
public class InvalidPlateException extends Exception {

    public InvalidPlateException(String message) {
        super(message);
    }
}
