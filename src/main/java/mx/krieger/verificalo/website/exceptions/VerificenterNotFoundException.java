package mx.krieger.verificalo.website.exceptions;

/**
 * Created by me on 06/01/17.
 */
public class VerificenterNotFoundException extends Exception {

    public VerificenterNotFoundException(String message) {
        super(message);
    }
}
