package mx.krieger.verificalo.website.exceptions;

/**
 * Created by me on 06/01/17.
 */
public class PlateNotFoundException extends Exception {

    public PlateNotFoundException(String message) {
        super(message);
    }
}
