package mx.krieger.verificalo.website.exceptions;

/**
 * Created by me on 06/01/17.
 */
public class PlatesAPIException extends Exception {

    public PlatesAPIException(String message) {
        super(message);
    }
}
