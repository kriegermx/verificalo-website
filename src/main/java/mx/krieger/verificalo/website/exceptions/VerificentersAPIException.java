package mx.krieger.verificalo.website.exceptions;

/**
 * Created by me on 06/01/17.
 */
public class VerificentersAPIException extends Exception {

    public VerificentersAPIException(String message) {
        super(message);
    }
}
