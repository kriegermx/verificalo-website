package mx.krieger.verificalo.website.servlets;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import mx.krieger.verificalo.website.model.daos.VerificenterRating;
import mx.krieger.verificalo.website.utils.DataManager;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by me on 04/01/17.
 */
public class VerificenterLoader extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String message = "";
        PrintWriter out = response.getWriter();
        HashSet<String> codes = null;
        try {
            codes = getCodes();
            while (registeredCodes() < codes.size()) {
                processCodes(codes);
            }

            out.println("<h1>All (" + codes.size() + ") verificenters were successfully stored: " + registeredCodes()
                    + "</h1>");
        } catch (IOException e) {
            e.printStackTrace();
            message = "Error while reading codes: " + e.getMessage();
            out.println(message);
        }


    }

    private int registeredCodes() {
        EntityManager entityManager = new DataManager().getEntityManager();
        Query query = entityManager.createNativeQuery("SELECT COUNT(*) FROM \"VerificenterRating\"");
        HashMap<String, Long> result = (HashMap) query.getSingleResult();
        int count = Math.toIntExact(result.get("count"));
        System.out.println("=========================================================================================");
        System.out.println("============================== RESULT Number of verificenters stored: " + result.toString
                ());
        System.out.println("============================== COUNT Number of verificenters stored: " + count);
        System.out.println("=========================================================================================");
        entityManager.close();
        return count;
    }

    private String processCodes(HashSet<String> codes) {
        String message = "";
        EntityManager entityManager = new DataManager().getEntityManager();
        entityManager.getTransaction().begin();

        int i = 0;
        for (String code : codes) {
            VerificenterRating rating = null;
            try {
                Query query = entityManager.createQuery("SELECT r FROM VerificenterRating r WHERE r.verificenterId="
                        + code);
                rating = (VerificenterRating) query.getSingleResult();
            } catch (NoResultException e) {
                rating = new VerificenterRating(code, 0, 0, 0);
                entityManager.persist(rating);
                message += "<p>" + (++i) + ".-" + code + "</p>";
            }

        }
        entityManager.getTransaction().commit();
        entityManager.clear();
        entityManager.close();
        return message;
    }

    public HashSet<String> getCodes() throws IOException {
        HashSet<String> codes = new HashSet<String>();

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> verificenters = mapper.readValue(
                new File(getServletContext().getRealPath("/files/verificentros.geojson")),
                new TypeReference<Map<String, Object>>() {
                });

        @SuppressWarnings("unchecked")
        ArrayList<Map<String, Object>> features = (ArrayList<Map<String, Object>>) verificenters.get("features");

        for (Map<String, Object> entry : features) {
            Map<String, Object> properties = (Map<String, Object>) entry.get("properties");
            String code = (String) properties.get("code");
            codes.add(code);
        }

        return codes;

    }

}
