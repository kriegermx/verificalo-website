package mx.krieger.verificalo.website.apis.clients;

import com.google.api.client.util.Key;
import mx.krieger.verificalo.website.exceptions.PlatesAPIException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.lang.reflect.Type;

/**
 * Esta clase se usa como cliente para llamar al servicio
 * <a href="https://api-datos.appspot.com/_ah/api/vehicles/v2/getOwnershipTaxDebts/192UMM">vehicles
 * .getOwnershipTaxDebts</a>.
 * Este servicio regresa información de pagos pendientes (tenencia), y sus respectivos años.
 * Para obtener la respuesta del servicio, cree una nueva instancia de esta clase, y llame al m&eacute;todo
 * {@link #getResponse(String...)}.
 * Use el n&uacute;mero de placa que desea consultar como par&aacute;metro. Por ejemplo:
 * <code>
 * GetOwnershipTaxDebts.Response response = new GetOwnershipTaxDebts().getResponse("192UMM");
 * </code>
 * <p>
 * Creado por juanjo@krieger.mx el 26/12/16.
 */
public class GetOwnershipTaxDebts extends APIMethod {

    @Override
    protected String getUrl() {
        return "https://verificalo-dot-api-datos.appspot.com/_ah/api/vehicles/v2/getOwnershipTaxDebts/%s";
    }

    @Override
    protected Type getResponseType() {
        return Response.class;
    }

    @Override
    public Response getResponse(String... params) throws PlatesAPIException {
        return (Response) execute(params);
    }

    /**
     * La respuesta para este servicio.
     */
    public static class Response extends APIResponse {

        @Key("placa_response")
        String plate;
        @Key("tiene_adeudos")
        String hasPendingPayments;
        @Key("adeudos")
        String pendingPayments;

        public Response() {
        }

        public Response(String externalServerResponse, String kind, String etag, String plate, String
                hasPendingPayments, String pendingPayments) {
            super(externalServerResponse, kind, etag);
            this.plate = plate;
            this.hasPendingPayments = hasPendingPayments;
            this.pendingPayments = pendingPayments;
        }

        public String getPlate() {
            return plate;
        }

        public String getHasPendingPayments() {
            return hasPendingPayments;
        }

        public String getPendingPayments() {
            return pendingPayments;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "plate='" + plate + '\'' +
                    ", hasPendingPayments='" + hasPendingPayments + '\'' +
                    ", pendingPayments='" + pendingPayments + '\'' +
                    "} " + super.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof Response)) return false;

            Response response = (Response) o;

            return new EqualsBuilder()
                    .appendSuper(super.equals(o))
                    .append(plate, response.plate)
                    .append(hasPendingPayments, response.hasPendingPayments)
                    .append(pendingPayments, response.pendingPayments)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .appendSuper(super.hashCode())
                    .append(plate)
                    .append(hasPendingPayments)
                    .append(pendingPayments)
                    .toHashCode();
        }
    }
}
