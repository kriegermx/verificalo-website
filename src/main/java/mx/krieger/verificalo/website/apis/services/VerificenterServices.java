package mx.krieger.verificalo.website.apis.services;

import mx.krieger.verificalo.website.apis.clients.GetEnvironmentVerifications;
import mx.krieger.verificalo.website.apis.clients.SpAppDatosVerificentro;
import mx.krieger.verificalo.website.exceptions.*;
import mx.krieger.verificalo.website.model.daos.VerificenterFeedback;
import mx.krieger.verificalo.website.model.daos.VerificenterRating;
import mx.krieger.verificalo.website.utils.Checkers;
import mx.krieger.verificalo.website.utils.DataManager;
import mx.krieger.verificalo.website.utils.Utils;
import mx.krieger.verificalo.website.utils.VerificaloLogger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Esta clase contiene los servicios web expuestos como una API REST para administrar la retroalimentación para los
 * verificentros.
 * Creado por juanjo@krieger.mx el 29/12/16.
 */
@Path("/verificenter")
public class VerificenterServices {

    private static final int VALID_DAYS_PERIOD = 30;
    private EntityManager entityManager = new DataManager().getEntityManager();


    /**
     * Este servicio obtiene la lista de todos los verificentros registrados, junto con su respectivos ratings
     *
     * @return La lista de c&oacute;digos de verificentro y su ratig promedio
     */
    @GET
    @Path("list/all")
    @Produces("application/json")
    public VerificenterList getVerificenterList() {

        try {
            List<VerificenterList.VerificenterRating> ratings = new ArrayList<>();

            SpAppDatosVerificentro.JSONResponse verificentersList = new SpAppDatosVerificentro().listVerificanters();

            for (SpAppDatosVerificentro.Verificenter v : verificentersList.getVerificenters()) {
                VerificenterList.VerificenterRating pivot = new VerificenterList.VerificenterRating(v);

                VerificenterRating rating = getVerificenterRating(v.getCenter());
                if (rating == null) {//If the verificenter does not have any rating previously registered.
                    pivot.setRating(0);
                } else {
                    pivot.setRating(rating.getPrettyRating());
                }

                ratings.add(pivot);
            }

            VerificenterList result = new VerificenterList(ratings, verificentersList.getDate()[0].getDate());
            return result;
        } catch (VerificentersAPIException e) {
            e.printStackTrace();
            throw new WebApplicationException(e.getMessage(), Response.Status.SERVICE_UNAVAILABLE);
        }
    }

    /**
     * Este método sirve para enviar al servidor retroalimentación para un verificentro.
     *
     * @param feedback El objeto que econtiene la información de la reetroalimentación, incluyendo el comentario, la
     *                 calificación, el identificador único del verificentro, y opcionalmente la placa del vehículo
     *                 que fue verificado. Para mayores detalles, referirse a la clase {@link Feedback}
     * @return una respusta estandar con código 200 si el registro se realizó correctamente.
     */
    @POST
    @Path("feedback/send")
    @Consumes("application/json")
    @Context
    public Response sendFeedback(@Context HttpServletRequest req, Feedback feedback) {
        VerificaloLogger.getLogger().debug("Receiving verificenter feedback: " + feedback.toString());

        /*
        try {
            checkFeedbackRequirments(feedback);
        } catch (PlatesAPIException | VerificenterNotFoundException | PlateNotFoundException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_FOUND);
        } catch (FeedbackAPIException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.BAD_REQUEST);
        } catch (VerificentersAPIException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.SERVICE_UNAVAILABLE);
        } */


        entityManager.getTransaction().begin();

        //Storing feedback
        VerificenterFeedback storable = new VerificenterFeedback(feedback);
        entityManager.persist(storable);

        //Updating rating
        VerificenterRating rating;
        try {
            rating = getVerificenterRating(feedback.getVerificenterId());
            if (rating == null) {
                rating = new VerificenterRating(feedback.getVerificenterId(), feedback.getRating(), 1, feedback
                        .getRating());
            } else {
                rating.setRatingsCount(rating.getRatingsCount() + 1);
                rating.setRatingsSum(rating.getRatingsSum() + feedback.getRating());
                rating.setRating(rating.getRatingsSum() / rating.getRatingsCount());
                rating.setId(rating.getId());
            }
        } catch (NoResultException e) {
            rating = new VerificenterRating(feedback.getVerificenterId(), feedback.getRating(), 1, feedback.getRating
                    ());
        }
        entityManager.persist(rating);
        entityManager.getTransaction().commit();
        entityManager.clear();
        entityManager.close();

        String result = "request url: " + req.getRequestURL().toString() + "\n";
        result += "context path: " + req.getContextPath().toString() + "\n";
        result += "path info: " + req.getPathInfo().toString() + "\n";
        result += "request uri : " + req.getRequestURI().toString() + "\n";
        result += "path translated : " + req.getPathTranslated().toString() + "\n";
        result += "servlet path : " + req.getServletPath() + "\n";


        return Response.status(200).entity(result).build();

    }

    /**
     * Este método se utiliza para obtener la lista de retroalimentaciones para un veirificentro en específico.
     *
     * @param verificenterId el identificador único del verificentro que se desea consultar.
     * @return La lista de retroalimentaciones y el promedio de calificación. Ver {@link FeedbackList}
     */
    @GET
    @Path("feedback/list/{verificenterId}")
    @Produces("application/json")
    public FeedbackList listVerificenterFeedback(@PathParam("verificenterId") String verificenterId) {

        try {
            checkVerificenterExists(verificenterId);
        } catch (VerificentersAPIException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.SERVICE_UNAVAILABLE);
        } catch (VerificenterNotFoundException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_FOUND);
        }

        try {


            ArrayList<Feedback> feedback = new ArrayList<Feedback>();

            Query q = entityManager.createQuery("SELECT r FROM VerificenterFeedback r WHERE r.verificenterId=" +
                    verificenterId);
            List<VerificenterFeedback> results = q.getResultList();

            VerificenterRating rating= new VerificenterRating(0);



            if(results.size()>0){
                for (VerificenterFeedback element : results) {
                    feedback.add(new Feedback(element));
                }

                q = entityManager.createQuery("SELECT r FROM VerificenterRating r WHERE r.verificenterId=" +
                        verificenterId);

                rating = (VerificenterRating) q.getSingleResult();

            }

            entityManager.close();
            FeedbackList result = new FeedbackList(feedback, Math.floor(rating.getRating()));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException("Lo sentimos, ocurrió un error al cargar los comentarios para el " +
                    "verificentro " + verificenterId, Response.Status.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * Este método regresa la lista de vericentros que pueden ser comentados por una placa.
     *
     * @param plate La placa a ser usada para identificar el comentario.
     * @return La lista de los identificadores de verificentro que necesitan comentarios.
     * @throws PlatesAPIException Si no se puede obtener la lista de verificaciones anteriores.
     * @throws ParseException     Si la fecha de las verificaciones anteriores es inválida.
     */
    @GET
    @Path("feedback/pending/{plate}")
    @Produces("application/json")
    public LinkedHashSet<String> getPendingComments(@PathParam("plate") String plate){

        Checkers checkers = new Checkers();
        if(!checkers.isValidPlate(plate)){
            throw new WebApplicationException(checkers.getMessage(), Response.Status.NOT_FOUND);
        }

        LinkedHashSet<String> pendingVerificenters = new LinkedHashSet<String>();
        GetEnvironmentVerifications.Response verifications = null;
        try {
            verifications = new GetEnvironmentVerifications().getResponse
                    (plate);
        } catch (PlatesAPIException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.SERVICE_UNAVAILABLE);
        }
        for (GetEnvironmentVerifications.VerificationItem verification : verifications.getVerificationList()) {
            try {
                if (validCommentPeriod(verification.getVerificationDate()) && !previousCommentExist(plate, verification
                        .getVerificenter())) {
                    pendingVerificenters.add(verification.getVerificenter());
                }
            } catch (ParseException e) {
                throw new WebApplicationException("Lo sentimos, no es posible validar la fecha de su verificación.", Response.Status.BAD_REQUEST);
            }

        }

        return pendingVerificenters;

    }

    /**
     * Este método revisa que un verificentro efectivamente exista.
     *
     * @param verificenterId El id del verificentro a corroborar, solamente los 4 dígitos.
     * @throws VerificentersAPIException     Si no se puede conectar a la API de verificentros
     * @throws VerificenterNotFoundException Si el id de verificnetro no es válido.
     */
    public void checkVerificenterExists(String verificenterId) throws VerificentersAPIException,
            VerificenterNotFoundException {
        LinkedHashSet<String> verificenterIds = new LinkedHashSet<>();
        for (SpAppDatosVerificentro.Verificenter v : new SpAppDatosVerificentro().listVerificanters().getVerificenters()) {
            verificenterIds.add(v.getCenter().substring(2));
        }
        if (!verificenterIds.contains(verificenterId)) {
            throw new VerificenterNotFoundException("El verificentro " + verificenterId + " no es válido.");
        }
    }

    /**
     * Esta clase sirve para representar los datos de la retroalimentación del verificentro.
     */
    public static class Feedback {
        @NotNull
        private String verificenterId;
        @NotNull
        private String plate;
        private String feedback;
        private int rating;
        private String date;


        public Feedback() {
        }

        public Feedback(String verificenterId, String feedback, int rating, String date) {
            this.verificenterId = verificenterId;
            this.feedback = feedback;
            this.rating = rating;
            this.date = date;
        }

        public Feedback(VerificenterFeedback feedback) {
            this.verificenterId = feedback.getVerificenterId();
            this.feedback = feedback.getComment();
            this.plate = feedback.getPlate();
            this.rating = feedback.getRating();
            DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm a z");
            this.date = df.format(feedback.getCreationDate());
        }

        public String getVerificenterId() {
            return verificenterId;
        }

        public void setVerificenterId(String verificenterId) {
            this.verificenterId = verificenterId;
        }

        public String getPlate() {
            return plate;
        }

        public void setPlate(String plate) {
            this.plate = plate;
        }

        public String getFeedback() {
            return feedback;
        }

        public void setFeedback(String feedback) {
            this.feedback = feedback;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "Feedback{" +
                    "verificenterId='" + verificenterId + '\'' +
                    ", plate='" + plate + '\'' +
                    ", feedback='" + feedback + '\'' +
                    ", rating=" + rating +
                    ", date=" + date +
                    '}';
        }
    }

    /**
     * Esta clase sirve para ecapsular la lista de comentarios de un verificentro, junto con el promedio de
     * calificación.
     */
    public static class FeedbackList {
        private ArrayList<Feedback> feedbacks;
        private double averageRating;

        public FeedbackList() {
        }

        public FeedbackList(ArrayList<Feedback> feedbacks, double averageRating) {
            this.feedbacks = feedbacks;
            this.averageRating = averageRating;
        }

        public ArrayList<Feedback> getFeedbacks() {
            return feedbacks;
        }

        public void setFeedbacks(ArrayList<Feedback> feedbacks) {
            this.feedbacks = feedbacks;
        }

        public double getAverageRating() {
            return averageRating;
        }

        public void setAverageRating(double averageRating) {
            this.averageRating = averageRating;
        }

        @Override
        public String toString() {
            return "FeedbackList{" +
                    "feedbacks=" + feedbacks +
                    ", averageRating=" + averageRating +
                    '}';
        }
    }

    public static class RankingListParameters {
        private boolean descending = true;
        private int pageNumber;
        private int pageSize;

        public boolean isDescending() {
            return descending;
        }

        public void setDescending(boolean descending) {
            this.descending = descending;
        }

        public int getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }
    }

    public static class VerificenterList {
        private List<VerificenterRating> ratingList;
        private String lastUpdate;

        public VerificenterList() {
        }

        public VerificenterList(List<VerificenterRating> ratingList, String lastUpdate) {
            this.ratingList = ratingList;
            this.lastUpdate = lastUpdate;
        }

        public VerificenterList(List<VerificenterRating> ratingList) {
            this.ratingList = ratingList;
        }

        public List<VerificenterRating> getRatingList() {
            return ratingList;
        }

        public String getLastUpdate() {
            return lastUpdate;
        }

        @Override
        public String toString() {
            return "VerificenterList{" +
                    "ratingList=" + ratingList +
                    ", lastUpdate='" + lastUpdate + '\'' +
                    '}';
        }

        public static class VerificenterRating extends SpAppDatosVerificentro.Verificenter {
            private int rating;


            public VerificenterRating() {
            }

            public VerificenterRating(String center, String name, String address, String phones, String delegation,
                                      String type, String map, String reference, String latitude, String longitude,
                                      String link, int rating) {
                super(center, name, address, phones, delegation, type, map, reference, latitude, longitude, link);
                this.rating = rating;
            }

            public VerificenterRating(SpAppDatosVerificentro.Verificenter v) {
                this.center = v.getCenter().substring(2);
                this.name = v.getName();
                this.address = v.getAddress();
                this.phones = v.getPhones();
                this.delegation = v.getDelegation();
                this.type = v.getType();
                this.latitude = v.getLatitude();
                this.longitude = v.getLongitude();
                this.link = v.getLink();
                this.available_appointments=v.getAvailable_appointments();
            }

            public void setRating(int rating) {
                this.rating = rating;
            }

            public int getRating() {
                return rating;
            }

            @Override
            public String toString() {
                return "VerificenterRating{" +
                        "rating=" + rating +
                        "} " + super.toString();
            }
        }
    }


    private void checkFeedbackRequirments(Feedback feedback) throws PlatesAPIException, FeedbackAPIException,
            VerificentersAPIException, VerificenterNotFoundException, PlateNotFoundException {

        if (feedback.getPlate() == null || feedback.getPlate().isEmpty()) {
            throw new FeedbackAPIException("Debe introducir la placa del vehículo que ha verificado.");
        }

        if (feedback.getVerificenterId() == null || feedback.getVerificenterId().isEmpty()) {
            throw new FeedbackAPIException("Debe introducir la clave del verificentro para el cual desea dejar un " +
                    "comentario.");
        }

        checkVerificenterExists(feedback.getVerificenterId());

        checkIfCommentIsAllowed(feedback.getPlate(), feedback.getVerificenterId());
    }


    /**
     * This method queries the rating of a evrificenter in the database
     *
     * @param verificenterId The string identifier of the verificenter
     * @return null if the elements is not found, else returns the rating element
     */
    private VerificenterRating getVerificenterRating(String verificenterId) {
        try {

            Query query = entityManager.createQuery("SELECT r FROM VerificenterRating r WHERE r.verificenterId=" +
                    verificenterId);
            VerificenterRating rating = (VerificenterRating) query.getSingleResult();
            return rating;
        } catch (NoResultException e) {
            return null;
        }
    }


    /**
     * Este método valida si un usuario puede registrar un comentario para un verificentro en específico.
     * La validación se lleva a cabo considerando que no existan comentarios anteriores del usuario al veiricentro,
     * que el usuario tenga registradas veirificaciones previas en el verificentro especificado, y que la fecha sea
     * menos al limite de días del periodo especificado en la constante VALID_DAYS_PERIOD
     *
     * @param plate          la placa del auto con el que se realizó la verificación
     * @param verificenterId el identificador del verificentro, UNICAMENTE DIGITOS (sin las letras del prefijo)
     * @throws FeedbackAPIException si el usuario no puede registrar comentarios por algún motivo. El motivo se
     *                              encuentra dentro del mensaje de la excepción.
     */
    public void checkIfCommentIsAllowed(String plate, String verificenterId) throws FeedbackAPIException, PlateNotFoundException {
        try {
            if (!previousCommentExist(plate, verificenterId)) {
                GetEnvironmentVerifications.Response verifications = new GetEnvironmentVerifications().getResponse
                        (plate);
                System.out.println(verifications.toString());
                System.out.println(verifications.getVerificationList().toString());

                if(verifications.getExternalServerResponse().equals("404")){
                    throw new PlateNotFoundException("Lo sentimos, no es encontró información para la placa "+plate+".");
                }

                for (GetEnvironmentVerifications.VerificationItem verification : verifications.getVerificationList()) {
                    if (verification.getVerificenter().equals(verificenterId)) {
                        if (!validCommentPeriod(verification.getVerificationDate())) {
                            throw new FeedbackAPIException("Lo sentimos, usted sólo cuenta con " + VALID_DAYS_PERIOD
                                    + " días para enviar sus comentarios.");
                        }
                    }
                }

                throw new FeedbackAPIException("Lo sentimos, no se ha localizado su registro de verificación para la " +
                        "placa " + plate + " en el verificentro " + verificenterId + ". Por favor intenten nuevamente" +
                        " más tarde.");
            }
        } catch (PlatesAPIException e) {
            e.printStackTrace();//No hay información de las verificaciones de las placas
            throw new FeedbackAPIException("Lo sentimos, no se ha podido obtener la información de su historial de " +
                    "verificaciones.");
        } catch (ParseException e) {
            e.printStackTrace();//La fecha de la veiricación es incorrecta
            throw new FeedbackAPIException("Lo sentimos, la fecha registrada para su verificación no es válida.");
        }

    }

    public boolean previousCommentExist(String plate, String verificenterId) {
        if (getPlateVerificenterComment(plate, verificenterId) == null)
            return false;
        else
            return true;
    }

    public boolean validCommentPeriod(String verificationDate) throws ParseException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new SimpleDateFormat("yyyy/MM/dd").parse(verificationDate));
        int daysFromVerification = Utils.daysBetween(cal.getTime(), Calendar.getInstance().getTime());
        if (daysFromVerification < VALID_DAYS_PERIOD) {
            return true;
        } else {
            return false;
        }
    }

    public VerificenterFeedback getPlateVerificenterComment(String plate, String verificenterId) {
        try {
            Query query = entityManager.createQuery("SELECT r FROM VerificenterFeedback r WHERE r.verificenterId=" +
                    verificenterId + " AND r.plate=" + plate);
            VerificenterFeedback comment = (VerificenterFeedback) query.getSingleResult();
            return comment;
        } catch (NoResultException e) {
            return null;
        }
    }

}
