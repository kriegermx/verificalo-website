package mx.krieger.verificalo.website.apis.clients;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import mx.krieger.verificalo.website.exceptions.PlatesAPIException;
import mx.krieger.verificalo.website.utils.VerificaloLogger;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * Esta clase abstracta se usa como base para implementar clientes que se comuniquen con las APIs remotas.
 * Los clientes específicos deben extender esta clase e implementar los métodos {@link #getUrl()},
 * {@link #getResponseType()} y {@link #getResponse(String...)}.
 * Así mismo, cada cliente (subclase) debe tener una clase que contenga los parámetros de la respuesta esperada. Esta
 * respuesta debe extender la clase {@link APIResponse}.
 * La clase de respuesta debe ser estática, y debe tener un constructor vacío para que pueda ser mapeada a la
 * respuesta en JSON.
 * Los campos de la clase respuesta deben usar la etiqueta {@link com.google.api.client.util.Key} y el nombre del
 * campo en la respuesta, para poder ser mapeados al campo de la clase correspondiente. Para mayor referencia
 * consulte la documentación de la
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">librería de cientes
 * HTTP de Google</a>.
 *
 * @see com.google.api.client.http.HttpRequest
 * Creado por juanjo@krieger.mx el 24/12/16.
 */
public abstract class APIMethod {


    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();


    /**
     * Este método regresa la SERVICE_URL específica para cada servicio.
     * Los parámetros deben ser especificados con el comodón "%s" para que sean lleandos cuando el método se ejecuta.
     * Por ejemplo:
     * https://api-datos.appspot.com/_ah/api/vehicles/v2/getTrafficTickets/%s
     * Donde el comodín al final representa el número de placa.
     *
     * @return La SERVICE_URL especpifica del servicio a ser llamado.
     */
    abstract String getUrl();

    /**
     * Este método regresa la clase correspondiente para la respuesta del servicio. Esto es requerido para realiazar
     * el mapeo de JSON a Java. Por ejemplo:
     * <code>
     * return APIMethod.class
     * </code>
     *
     * @return La clase de la respuesta.
     */
    abstract Type getResponseType();

    /**
     * Este método es el encargado de exponer la respuesta de los servicios externos a las clases locales.
     * Para obtener la respuesta se debe ejecutar lo siguiente:
     * <code>
     * Service.Response response = new Service().getResponse("param1", "param2"... "paramN")
     * </code>
     * Donde Service es una subclase de esta clase. Response es la clase anidada dentro del servicio que contiene los
     * campos esperados; su tipo debe ser regresado en el método {@link #getResponseType()}. "param1", "param2"...
     * "paramN" son los parámetros requeridos para el servicio; esto parámetros deben indicarse en el mismo orden en
     * el que se encuentran los comodines en la SERVICE_URL regresada en {@link #getUrl()}
     *
     * @param params
     * @return
     * @throws IOException
     */
    public abstract APIResponse getResponse(String... params) throws PlatesAPIException;

    private GenericUrl getUrl(String... params) {
        VerificaloLogger.getLogger().debug("Params to use: " + params);
        GenericUrl genericUrl = new GenericUrl(String.format(getUrl(), params));
        VerificaloLogger.getLogger().debug(String.format("Service url: %s", genericUrl.toString()));
        return genericUrl;
    }

    /**
     * Este método realiza la llamada a la API remota. Para hacerlo se ha hecho uso del cliente HTTP de Google.
     * Puede consultar más detalles en la
     * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">página de
     * documentación</a>.
     *
     * @param params los parámetros específicos para cada servicio.
     * @return La respuesta del servicio invocado. Siempre es una subclase de {@link APIResponse}
     * @throws IOException Cuando hay algún problema con el servicio remoto.
     */
    APIResponse execute(String... params) throws PlatesAPIException {
        HttpRequestFactory requestFactory =
                HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
                    public void initialize(HttpRequest request) {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    }
                });
        HttpRequest request = null;
        try {
            request = requestFactory.buildGetRequest(getUrl(params));
        } catch (IOException e) {
            throw new PlatesAPIException("Los datos para la conexión al servicio remoto de placas no son válidos.");
        }
        VerificaloLogger.getLogger().debug("Executing remote service...");
        HttpResponse execute = null;
        try {
            execute = request.execute();
        } catch (IOException e) {
            throw new PlatesAPIException("No hemos podido conectarnos con el servicio remoto de placas");
        }
        APIResponse response = null;
        try {
            response = (APIResponse) execute.parseAs(getResponseType());
        } catch (IOException e) {
            throw new PlatesAPIException("No hemos podido interpretar la respueta del servicio remoto de placas");
        }
        VerificaloLogger.getLogger().debug(String.format("Service response: %s", response.toString()));
        return response;
    }


    /**
     * La clase de respuesta básica a ser extendida por las respuestas específicas para cada cliente.
     */
    public static abstract class APIResponse {

        @Key("external_server_response")
        String externalServerResponse;
        @Key
        String kind;
        @Key
        String etag;

        public APIResponse() {
        }

        public APIResponse(String externalServerResponse, String kind, String etag) {
            this.externalServerResponse = externalServerResponse;
            this.kind = kind;
            this.etag = etag;
        }

        public String getExternalServerResponse() {
            return externalServerResponse;
        }

        public String getKind() {
            return kind;
        }

        public String getEtag() {
            return etag;
        }

        @Override
        public String toString() {
            return "APIResponse{" +
                    "externalServerResponse='" + externalServerResponse + '\'' +
                    ", kind='" + kind + '\'' +
                    ", etag='" + etag + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof APIResponse)) return false;

            APIResponse that = (APIResponse) o;

            return new EqualsBuilder()
                    .append(externalServerResponse, that.externalServerResponse)
                    .append(kind, that.kind)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(externalServerResponse)
                    .append(kind)
                    .append(etag)
                    .toHashCode();
        }
    }
}
