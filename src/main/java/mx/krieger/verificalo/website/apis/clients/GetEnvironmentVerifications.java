package mx.krieger.verificalo.website.apis.clients;

import com.google.api.client.util.Key;
import mx.krieger.verificalo.website.exceptions.PlatesAPIException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Esta clase se usa como cliente para llamar al servicio
 * <a href="https://api-datos.appspot.com/_ah/api/vehicles/v2/getEnvironmentVerifications/192UMM">vehicles
 * .getEnvironmentVerifications</a>.
 * Este servicio regresa la información general del auto, incluyendo modelo, serie y holograma.
 * Para obtener la respuesta del servicio, cree una nueva instancia de esta clase, y llame al m&eacute;todo
 * {@link #getResponse(String...)}.
 * Use el n&uacute;mero de placa que desea consultar como par&aacute;metro. Por ejemplo:
 * <code>
 * GetEnvironmentVerifications.Response response = new GetEnvironmentVerifications().getResponse("192UMM");
 * </code>
 * <p>
 * Creado por juanjo@krieger.mx el 22/12/16.
 */
public class GetEnvironmentVerifications extends APIMethod {

    @Override
    String getUrl() {
        return "https://verificalo-dot-api-datos.appspot.com/_ah/api/vehicles/v2/getEnvironmentVerifications/%s";
    }

    @Override
    Type getResponseType() {
        return Response.class;
    }

    @Override
    public Response getResponse(String... params) throws PlatesAPIException {
        return (Response) execute(params);
    }

    /**
     * La respuesta para este servicio.
     */
    public static class Response extends APIMethod.APIResponse {
        public Response() {

        }

        public Response(String externalServerResponse, String kind, String etag, ArrayList<VerificationItem>
                verificationList) {
            super(externalServerResponse, kind, etag);
            this.verificationList = verificationList;
        }

        @Key("verificationList")
        ArrayList<VerificationItem> verificationList;


        public ArrayList<VerificationItem> getVerificationList() {
            return verificationList;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "verificationList=" + verificationList +
                    "} " + super.toString();
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof Response)) return false;

            Response response = (Response) o;

            return new EqualsBuilder()
                    .appendSuper(super.equals(o))
                    .append(verificationList, response.verificationList)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .appendSuper(super.hashCode())
                    .append(verificationList)
                    .toHashCode();
        }
    }


    public static class VerificationItem {
        @Key("resultado")
        String result;
        @Key("vigencia")
        String validity;
        @Key("causaRechazo")
        String rejectionCause;
        @Key("combustible")
        String fuel;
        @Key
        String vin;
        @Key("marca")
        String brand;
        @Key("fechaVerificacion")
        String verificationDate;
        @Key("modelo")
        String model;
        @Key("cancelado")
        String cancelled;
        @Key("horaVerificacion")
        String verificationTime;
        @Key("submarca")
        String subbrand;
        @Key("placa_response")
        String plate;
        @Key("verificentro")
        String verificenter;
        @Key("equipoGDF")
        String GDFteam;
        @Key("linea")
        String line;
        @Key("certificado")
        String certificade;

        public VerificationItem() {
        }

        public VerificationItem(String result, String validity, String rejectionCause, String fuel, String vin,
                                String brand, String verificationDate, String model, String cancelled, String
                                        verificationTime, String subbrand, String plate, String verificenter, String
                                        GDFteam, String line, String certificade) {
            this.result = result;
            this.validity = validity;
            this.rejectionCause = rejectionCause;
            this.fuel = fuel;
            this.vin = vin;
            this.brand = brand;
            this.verificationDate = verificationDate;
            this.model = model;
            this.cancelled = cancelled;
            this.verificationTime = verificationTime;
            this.subbrand = subbrand;
            this.plate = plate;
            this.verificenter = verificenter;
            this.GDFteam = GDFteam;
            this.line = line;
            this.certificade = certificade;
        }

        public String getResult() {
            return result;
        }

        public String getValidity() {
            return validity;
        }

        public String getRejectionCause() {
            return rejectionCause;
        }

        public String getFuel() {
            return fuel;
        }

        public String getVin() {
            return vin;
        }

        public String getBrand() {
            return brand;
        }

        public String getVerificationDate() {
            return verificationDate;
        }

        public String getModel() {
            return model;
        }

        public String getCancelled() {
            return cancelled;
        }

        public String getVerificationTime() {
            return verificationTime;
        }

        public String getSubbrand() {
            return subbrand;
        }

        public String getPlate() {
            return plate;
        }

        public String getVerificenter() {
            return verificenter;
        }

        public String getGDFteam() {
            return GDFteam;
        }

        public String getLine() {
            return line;
        }

        public String getCertificade() {
            return certificade;
        }

        @Override
        public String toString() {
            return "VerificationItem{" +
                    "result='" + result + '\'' +
                    ", validity='" + validity + '\'' +
                    ", rejectionCause='" + rejectionCause + '\'' +
                    ", fuel='" + fuel + '\'' +
                    ", vin='" + vin + '\'' +
                    ", brand='" + brand + '\'' +
                    ", verificationDate='" + verificationDate + '\'' +
                    ", model='" + model + '\'' +
                    ", cancelled='" + cancelled + '\'' +
                    ", verificationTime='" + verificationTime + '\'' +
                    ", subbrand='" + subbrand + '\'' +
                    ", plate='" + plate + '\'' +
                    ", verificenter='" + verificenter + '\'' +
                    ", GDFteam='" + GDFteam + '\'' +
                    ", line='" + line + '\'' +
                    ", certificade='" + certificade + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof VerificationItem)) return false;

            VerificationItem that = (VerificationItem) o;

            return new EqualsBuilder()
                    .append(result, that.result)
                    .append(validity, that.validity)
                    .append(rejectionCause, that.rejectionCause)
                    .append(fuel, that.fuel)
                    .append(vin, that.vin)
                    .append(brand, that.brand)
                    .append(verificationDate, that.verificationDate)
                    .append(model, that.model)
                    .append(cancelled, that.cancelled)
                    .append(verificationTime, that.verificationTime)
                    .append(subbrand, that.subbrand)
                    .append(plate, that.plate)
                    .append(verificenter, that.verificenter)
                    .append(GDFteam, that.GDFteam)
                    .append(line, that.line)
                    .append(certificade, that.certificade)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(result)
                    .append(validity)
                    .append(rejectionCause)
                    .append(fuel)
                    .append(vin)
                    .append(brand)
                    .append(verificationDate)
                    .append(model)
                    .append(cancelled)
                    .append(verificationTime)
                    .append(subbrand)
                    .append(plate)
                    .append(verificenter)
                    .append(GDFteam)
                    .append(line)
                    .append(certificade)
                    .toHashCode();
        }
    }


}

