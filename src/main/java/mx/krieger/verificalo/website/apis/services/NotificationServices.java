package mx.krieger.verificalo.website.apis.services;

import mx.krieger.verificalo.website.apis.clients.GetEnvironmentVerifications;
import mx.krieger.verificalo.website.exceptions.InvalidPlateException;
import mx.krieger.verificalo.website.exceptions.PlatesAPIException;
import mx.krieger.verificalo.website.model.daos.Notifications;
import mx.krieger.verificalo.website.model.daos.PersistentEntity;
import mx.krieger.verificalo.website.utils.DataManager;
import mx.krieger.verificalo.website.utils.MailSender;
import mx.krieger.verificalo.website.utils.VerificaloLogger;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

/**
 * Esta clase expone los servicios web para adminsitrar notificaciones.
 * Creado por juanjo@krieger.mx el 28/12/16.
 */

@Path("/notifications")
public class NotificationServices {

    private DataManager dataManager = new DataManager();

    public static class Notification {
        private String plateNumber;
        private Notifications.Type type;
        private String email;

        public Notification() {
        }

        public Notification(String plateNumber, Notifications.Type type, String email) {
            this.plateNumber = plateNumber;
            this.type = type;
            this.email = email;
        }

        public String getPlateNumber() {
            return plateNumber;
        }

        public void setPlateNumber(String plateNumber) {
            this.plateNumber = plateNumber;
        }

        public Notifications.Type getType() {
            return type;
        }

        public void setType(Notifications.Type type) {
            this.type = type;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        @Override
        public String toString() {
            return "Notification{" +
                    "plateNumber='" + plateNumber + '\'' +
                    ", type=" + type +
                    ", email='" + email + '\'' +
                    '}';
        }
    }




    @POST
    @Path("suscribe")
    @Consumes("application/json")
    public Response suscribeNotification(Notification notification) throws SystemException, javax.transaction
            .NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException, PlatesAPIException, InvalidPlateException {

        VerificaloLogger.getLogger().debug("Receving notification request: " + notification.toString());

        GetEnvironmentVerifications.Response response = new GetEnvironmentVerifications().getResponse(notification.getPlateNumber());

        String hologram = response.getVerificationList().get(0).getResult();
        Notifications storable = new Notifications(notification, hologram);
        EntityManager entityManager = dataManager.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(storable);
        entityManager.getTransaction().commit();
        entityManager.close();
        return Response.status(200).build();
    }

    @GET
    @Path("unsuscribe/{email}/{plate}")
    public Response unsuscribeNotification(@PathParam("email") String email, @PathParam("plate") String plate) {
        EntityManager entityManager = dataManager.getEntityManager();
        Query query = entityManager.createQuery("SELECT r FROM Notifications r WHERE r.email='"+email+"' AND r.plate='"+plate+"'");
        List<Notifications> ids = query.getResultList();

        System.out.println("=======================================================");
        System.out.println("Ids: "+ Arrays.toString(ids.toArray()));
        System.out.println("=======================================================");

        for(Notifications notification : ids){
            entityManager.getTransaction().begin();
            query = entityManager.createNativeQuery("UPDATE \"Notifications\" SET status='"+ PersistentEntity.Status.INACTIVE+"' WHERE id="+notification.getId());
            query.executeUpdate();
            entityManager.getTransaction().commit();
        }
        entityManager.close();
        return Response.status(200).build();
    }



    @POST
    @Path("send")
    @Consumes("application/json")
    public Response sendNotification(MailSender.NotificationMessage message) {
        try {
            if(message.getReceivers().equals("*")){
                EntityManager entityManager = dataManager.getEntityManager();
                Query query = entityManager.createQuery("SELECT r FROM Notifications r WHERE r.status='"+PersistentEntity.Status.ACTIVE+"' ALLOW FILTERING");
                List<Notifications> users = query.getResultList();
                StringBuilder receivers = new StringBuilder();
                for(Notifications user : users)
                    receivers.append(user.getEmail()).append(",");
                //receivers.replace(receivers.length()-1, receivers.length()-1, "");
                message.setReceivers(receivers.toString());
            }
            new MailSender().send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
        return Response.status(200).build();
    }


}
