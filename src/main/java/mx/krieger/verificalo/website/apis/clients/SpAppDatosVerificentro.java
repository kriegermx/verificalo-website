package mx.krieger.verificalo.website.apis.clients;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mx.krieger.verificalo.website.exceptions.VerificentersAPIException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.xml.soap.*;
import java.io.IOException;

/**
 * Created by me on 02/02/17.
 */
public class SpAppDatosVerificentro {

    private static final String SERVICE_URL = "http://148.243.232.119:1477/SIVEV.asmx";
    public static final String SERVICE_PREFIX = "LabCdMx";
    public static final String METHOD_NAME = "SpAppDatosVerificentro";

    public static void main(String[] args) throws Exception {
        new SpAppDatosVerificentro().listVerificanters();
    }

    public JSONResponse listVerificanters() throws VerificentersAPIException {
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), SERVICE_URL);
            String verificentersJson = soapResponse.getSOAPBody().getElementsByTagName
                    ("SpAppDatosVerificentroResponse").item(0).getChildNodes().item(0).getFirstChild().getNodeValue();
            System.out.println(verificentersJson);

            ObjectMapper mapper = new ObjectMapper();
            JSONResponse jsonResponse = mapper.readValue(verificentersJson, JSONResponse.class);
            return jsonResponse;
        } catch (SOAPException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("Ocurrió un problema al tratar de crear una conexión con el servicio " +
                    "de verificentros. Por favor intenten nuevamente más tarde.");
        } catch (JsonParseException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("No se pudo interpretar la respuesta del servicio de veerificentros. " +
                    "Por favor itente nuevamente más tarde.");
        } catch (JsonMappingException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("No se pudo interpretar la respuesta del servicio de veerificentros. " +
                    "Por favor itente nuevamente más tarde.");
        } catch (IOException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("No se pudo interpretar la respuesta del servicio de veerificentros. " +
                    "Por favor itente nuevamente más tarde.");
        } catch (VerificentersAPIException e) {
            e.printStackTrace();
            throw e;
        }

    }

    private static SOAPMessage createSOAPRequest() throws VerificentersAPIException {
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();

            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration(SERVICE_PREFIX, SERVICE_PREFIX);

            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyElem = soapBody.addChildElement(METHOD_NAME, "LabCdMx");

            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", SERVICE_PREFIX + "/" + METHOD_NAME);

            soapMessage.saveChanges();

            return soapMessage;
        } catch (SOAPException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("Ocurrió un problema al crear el mensaje para obtener los " +
                    "verificentros. Por favor intente nuevamente más tarde.");
        }
    }

    public static class JSONResponse {
        protected Verificenter[] verificenters;
        protected DateWrapper[] date;

        @JsonProperty("verificentros")
        public Verificenter[] getVerificenters() {
            return verificenters;
        }

        @JsonProperty("fecha")
        public DateWrapper[] getDate() {
            return date;
        }


        public static class DateWrapper {
            String date;

            @JsonProperty("Fecha")
            public String getDate() {
                return date;
            }
        }
    }

    public static class Verificenter {
        protected String center;
        protected String name;
        protected String address;
        protected String phones;
        protected String delegation;
        protected String type;
        protected String map;
        protected String reference;
        protected String latitude;
        protected String longitude;
        protected String link;
        protected int available_appointments;

        public Verificenter() {
        }

        public Verificenter(String center, String name, String address, String phones, String delegation, String
                type, String map, String reference, String latitude, String longitude, String link) {
            this.center = center;
            this.name = name;
            this.address = address;
            this.phones = phones;
            this.delegation = delegation;
            this.type = type;
            this.latitude = latitude;
            this.longitude = longitude;
            this.link = link;
        }

        @JsonProperty("Centro")
        public String getCenter() {
            return center;
        }

        @JsonProperty("Nombre")
        public String getName() {
            return name;
        }

        @JsonProperty("Direccion")
        public String getAddress() {
            return address;
        }

        @JsonProperty("Telefonos")
        public String getPhones() {
            return phones;
        }

        @JsonProperty("Delegacion")
        public String getDelegation() {
            return delegation;
        }

        @JsonProperty("Tipo")
        public String getType() {
            return type;
        }

        @JsonProperty("Latitud")
        public String getLatitude() {
            return latitude;
        }

        @JsonProperty("Longitud")
        public String getLongitude() {
            return longitude;
        }

        @JsonProperty("Link")
        public String getLink() {
            return link;
        }

        @JsonProperty("CitasDisp")
        public int getAvailable_appointments() {
            return available_appointments;
        }

        @Override
        public String toString() {              // TODO: add latitude, longitude and link
            return "Measurements{" +
                    "date='" + center + '\'' +
                    ", name='" + name + '\'' +
                    ", address='" + address + '\'' +
                    ", phones='" + phones + '\'' +
                    ", delegation='" + delegation + '\'' +
                    ", type='" + type + '\'' +
                    ", map='" + map + '\'' +
                    ", reference='" + reference + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof Verificenter)) return false;

            Verificenter that = (Verificenter) o;

            return new EqualsBuilder()
                    .append(center, that.center)
                    .append(name, that.name)
                    .append(address, that.address)
                    .append(phones, that.phones)
                    .append(delegation, that.delegation)
                    .append(type, that.type)
                    .append(map, that.map)
                    .append(reference, that.reference)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(center)
                    .append(name)
                    .append(address)
                    .append(phones)
                    .append(delegation)
                    .append(type)
                    .append(map)
                    .append(reference)
                    .toHashCode();
        }

    }


}
