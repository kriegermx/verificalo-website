package mx.krieger.verificalo.website.apis.clients;

import com.google.api.client.util.Key;
import mx.krieger.verificalo.website.exceptions.PlatesAPIException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Esta clase se usa como cliente para llamar al servicio
 * <a href="https://api-datos.appspot.com/_ah/api/vehicles/v2/getTrafficTickets/192UMM">vehicles.getTrafficTickets</a>.
 * Este servicio regresa información acerca de las multas de tráfico.
 * Para obtener la respuesta del servicio, cree una nueva instancia de esta clase, y llame al m&eacute;todo
 * {@link #getResponse(String...)}.
 * Use el n&uacute;mero de placa que desea consultar como par&aacute;metro. Por ejemplo:
 * <code>
 * GetTrafficTickets.Response response = new GetTrafficTickets().getResponse("192UMM");
 * </code>
 * <p>
 * Creado por juanjo@krieger.mx el 26/12/16.
 */
public class GetTrafficTickets extends APIMethod {

    @Override
    protected String getUrl() {
        return "https://verificalo-dot-api-datos.appspot.com/_ah/api/vehicles/v2/getTrafficTickets/%s";
    }

    @Override
    protected Type getResponseType() {
        return Response.class;
    }

    @Override
    public Response getResponse(String... params) throws PlatesAPIException {
        return (Response) execute(params);
    }

    /**
     * La respuesta para este servicio.
     */
    public static class Response extends APIResponse {
        @Key("placa_response")
        String plate;
        @Key("ticketList")
        ArrayList<TicketElement> ticketList;
        @Key("adeudos")
        String pendingPayments;

        public Response() {
        }

        public Response(String externalServerResponse, String kind, String etag, String plate,
                        ArrayList<TicketElement> ticketList, String pendingPayments) {
            super(externalServerResponse, kind, etag);
            this.plate = plate;
            this.ticketList = ticketList;
            this.pendingPayments = pendingPayments;
        }

        public String getPlate() {
            return plate;
        }

        public ArrayList<TicketElement> getTicketList() {
            return ticketList;
        }

        public String getPendingPayments() {
            return pendingPayments;
        }

        @Override
        public String toString() {
            return "Response{" +
                    "plate='" + plate + '\'' +
                    ", ticketList=" + ticketList +
                    ", pendingPayments='" + pendingPayments + '\'' +
                    "} " + super.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof Response)) return false;

            Response response = (Response) o;

            return new EqualsBuilder()
                    .appendSuper(super.equals(o))
                    .append(plate, response.plate)
                    .append(ticketList, response.ticketList)
                    .append(pendingPayments, response.pendingPayments)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .appendSuper(super.hashCode())
                    .append(plate)
                    .append(ticketList)
                    .append(pendingPayments)
                    .toHashCode();
        }
    }

    public static class TicketElement {
        @Key("folio")
        String identifier;
        @Key("multa_salarios_minimos")
        String numberOfSalaries;
        @Key("fraccion")
        String fraction;
        @Key("salario_minimo")
        String minimumSalary;
        @Key("articulo")
        String article;
        @Key("fecha_infraccion")
        String date;
        @Key("motivo")
        String reason;

        public TicketElement() {
        }

        public TicketElement(String identifier, String numberOfSalaries, String fraction, String minimumSalary,
                             String article, String date, String reason) {
            this.identifier = identifier;
            this.numberOfSalaries = numberOfSalaries;
            this.fraction = fraction;
            this.minimumSalary = minimumSalary;
            this.article = article;
            this.date = date;
            this.reason = reason;
        }

        public String getIdentifier() {
            return identifier;
        }

        public String getNumberOfSalaries() {
            return numberOfSalaries;
        }

        public String getFraction() {
            return fraction;
        }

        public String getMinimumSalary() {
            return minimumSalary;
        }

        public String getArticle() {
            return article;
        }

        public String getDate() {
            return date;
        }

        public String getReason() {
            return reason;
        }

        @Override
        public String toString() {
            return "TicketElement{" +
                    "identifier='" + identifier + '\'' +
                    ", numberOfSalaries='" + numberOfSalaries + '\'' +
                    ", fraction='" + fraction + '\'' +
                    ", minimumSalary='" + minimumSalary + '\'' +
                    ", article='" + article + '\'' +
                    ", date='" + date + '\'' +
                    ", reason='" + reason + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof TicketElement)) return false;

            TicketElement that = (TicketElement) o;

            return new EqualsBuilder()
                    .append(identifier, that.identifier)
                    .append(numberOfSalaries, that.numberOfSalaries)
                    .append(fraction, that.fraction)
                    .append(minimumSalary, that.minimumSalary)
                    .append(article, that.article)
                    .append(date, that.date)
                    .append(reason, that.reason)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(identifier)
                    .append(numberOfSalaries)
                    .append(fraction)
                    .append(minimumSalary)
                    .append(article)
                    .append(date)
                    .append(reason)
                    .toHashCode();
        }
    }


}
