package mx.krieger.verificalo.website.apis.clients;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mx.krieger.verificalo.website.exceptions.VerificentersAPIException;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.xml.soap.*;
import java.io.IOException;

/**
 * Created by me on 02/02/17.
 */
public class Verificentros {

    private static final String SERVICE_URL = "http://148.243.232.113:8002/webserviceSIMAT.asmx";
    public static final String SERVICE_PREFIX = "http://cpa.blog.mx";
    public static final String METHOD_NAME = "Verificentros";

    public static void main(String[] args) throws Exception {
        new Verificentros().getContingencies();
    }

    public Measurements getContingencies() throws VerificentersAPIException {
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), SERVICE_URL);
            //soapResponse.writeTo(System.out);

            String contingencyJSON = soapResponse.getSOAPBody().getElementsByTagName
                    ("VerificentrosResult").item(0).getFirstChild().getNodeValue();

            contingencyJSON = contingencyJSON.replaceAll(",\n" +
                    "        \n" +
                    "\t", "}");
            System.out.println("JSON: " + contingencyJSON);

            ObjectMapper mapper = new ObjectMapper();
            Measurements measurements = mapper.readValue(contingencyJSON, Measurements.class);
            System.out.println(measurements.toString());
            soapConnection.close();
            return measurements;
        } catch (SOAPException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("Ocurrió un problema al tratar de crear una conexión con el servicio " +
                    "de verificentros. Por favor intenten nuevamente más tarde.");
        } catch (JsonParseException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("No se pudo interpretar la respuesta del servicio de veerificentros. " +
                    "Por favor itente nuevamente más tarde.");
        } catch (JsonMappingException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("No se pudo interpretar la respuesta del servicio de veerificentros. " +
                    "Por favor itente nuevamente más tarde.");
        } catch (IOException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("No se pudo interpretar la respuesta del servicio de veerificentros. " +
                    "Por favor itente nuevamente más tarde.");
        } catch (VerificentersAPIException e) {
            e.printStackTrace();
            throw e;
        }

    }

    private static SOAPMessage createSOAPRequest() throws VerificentersAPIException {
        try {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();

            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();

            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyElem = soapBody.addChildElement(METHOD_NAME);

            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", SERVICE_PREFIX + "/" + METHOD_NAME);

            soapMessage.saveChanges();

            return soapMessage;
        } catch (SOAPException e) {
            e.printStackTrace();
            throw new VerificentersAPIException("Ocurrió un problema al crear el mensaje para obtener los " +
                    "verificentros. Por favor intente nuevamente más tarde.");
        }
    }

    public static class Measurements {

        private Measurement measurements;

        public Measurements() {
        }

        public Measurements(Measurement measurements) {
            this.measurements = measurements;
        }

        @JsonProperty("Measurements")
        public Measurement getMeasurements() {
            return measurements;
        }

        @Override
        public String toString() {
            return "Measurements{" +
                    "measurements=" + measurements +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (!(o instanceof Measurements)) return false;

            Measurements that = (Measurements) o;

            return new EqualsBuilder()
                    .append(measurements, that.measurements)
                    .isEquals();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder(17, 37)
                    .append(measurements)
                    .toHashCode();
        }

        public static class Measurement {
            protected String date;
            protected String time;
            protected String humidity;
            protected String temperature;
            protected String wsp;
            protected String pressure;
            protected String pca;
            protected String title;
            protected String url;

            public Measurement() {
            }

            public Measurement(String date, String time, String humidity, String temperature, String wsp, String
                    pressure, String pca, String title, String url) {
                this.date = date;
                this.time = time;
                this.humidity = humidity;
                this.temperature = temperature;
                this.wsp = wsp;
                this.pressure = pressure;
                this.pca = pca;
                this.title = title;
                this.url = url;
            }

            @JsonProperty("fecha")
            public String getDate() {
                return date;
            }

            @JsonProperty("hora")
            public String getTime() {
                return time;
            }

            @JsonProperty("humedad")
            public String getHumidity() {
                return humidity;
            }

            @JsonProperty("temperatura")
            public String getTemperature() {
                return temperature;
            }

            @JsonProperty("wsp")
            public String getWsp() {
                return wsp;
            }

            @JsonProperty("presion")
            public String getPressure() {
                return pressure;
            }

            @JsonProperty("pca")
            public String getPca() {
                return pca;
            }

            @JsonProperty("titulo")
            public String getTitle() {
                return title;
            }

            @JsonProperty("url")
            public String getUrl() {
                return url;
            }

            @Override
            public String toString() {
                return "Measurements{" +
                        "date='" + date + '\'' +
                        ", time='" + time + '\'' +
                        ", humidity='" + humidity + '\'' +
                        ", temperature='" + temperature + '\'' +
                        ", wsp='" + wsp + '\'' +
                        ", pressure='" + pressure + '\'' +
                        ", pca='" + pca + '\'' +
                        ", title='" + title + '\'' +
                        ", url='" + url + '\'' +
                        '}';
            }

        }


    }
}
