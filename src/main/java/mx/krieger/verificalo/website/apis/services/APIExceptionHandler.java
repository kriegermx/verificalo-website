package mx.krieger.verificalo.website.apis.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static org.apache.struts2.ServletActionContext.getResponse;

/**
 * Created by me on 09/02/17.
 */
@Provider
public class APIExceptionHandler implements ExceptionMapper<Exception>{


    @Override
    public Response toResponse(Exception e) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = "{message:\"No se pudo identificer el mensaje del error\"}";
        try {
            if(e instanceof WebApplicationException){
                Response response = ((WebApplicationException)(e)).getResponse();
                ExceptionWrapper exception = new ExceptionWrapper(response.getStatus(), response.getStatusInfo().toString(), e.getMessage());
                System.out.println("Exception generated: "+exception);
                jsonInString = mapper.writeValueAsString(exception);
            }else{
                e.printStackTrace();
                jsonInString = "{\"message\":\""+e.getMessage()+"\"}";
            }
        } catch (JsonProcessingException e1) {
            e1.printStackTrace();
        }catch(ClassCastException e2){
            e2.printStackTrace();
        }


        System.out.println("Exception JSON: "+jsonInString);
        return Response.serverError().entity(jsonInString).type(MediaType.APPLICATION_JSON).build();
    }

    public static class ExceptionWrapper{
        private int status;
        private String statusInfo;
        private String message;

        public ExceptionWrapper(int status, String statusInfo, String message) {
            this.status = status;
            this.statusInfo = statusInfo;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public String getStatusInfo() {
            return statusInfo;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public String toString() {
            return "ExceptionWrapper{" +
                    "status=" + status +
                    ", statusInfo='" + statusInfo + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    }
}
