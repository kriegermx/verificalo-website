package mx.krieger.verificalo.website.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by me on 04/02/17.
 */
public class Utils {

    /**
     * Este método calcula los días transcurridos entre dos fechas.
     * @param one fecha 1
     * @param two fecha 2
     * @return el número de días en enteros.
     */
    public static int daysBetween(Date one, Date two) { long difference = (one.getTime()-two.getTime())/86400000; return (int) Math.abs(difference); }

    public static void main(String[] args) throws ParseException {
        Date d1 = new Calendar.Builder().setDate(2016, 12, 28).build().getTime();
        Date d2 = new Calendar.Builder().setDate(2017, 1, 2).build().getTime();
        System.out.println(daysBetween(d1, d2));

        Calendar cal = Calendar.getInstance();
        cal.setTime(new SimpleDateFormat("yyyy/MM/dd").parse("2008/12/01"));
        System.out.println(Utils.daysBetween(cal.getTime(), Calendar.getInstance().getTime()));
    }


}
