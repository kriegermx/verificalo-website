package mx.krieger.verificalo.website.utils;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by me on 23/12/16.
 */
public class VerificaloLogger {

    private static final String LOGGER_NAME = "mx.krieger.verificalo.website";
    private static final Logger logger = LogManager.getLogger(LOGGER_NAME);

    public static Logger getLogger() {
        return logger;
    }


}
