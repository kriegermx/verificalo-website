package mx.krieger.verificalo.website.utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by me on 12/01/17.
 */
public class MailSender {

    private static final String USERNAME = "verificalomx@gmail.com";
    private static final String PASSWORD = "calo3VERI.1";
    private static final String SENDER ="verificalomx@gmail.com";
    Properties props;
    Session session;

    public MailSender(){
        props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

    }

    public void send(NotificationMessage notification) throws MessagingException {
        stablishSession();
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SENDER));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(notification.getReceivers()));
            message.setSubject(notification.getSubject());
            message.setContent(notification.getHtmlMessage(), "text/html; charset=utf-8");
            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw e;
        }
    }

    private void stablishSession() {
        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(USERNAME,PASSWORD);
                    }
                });
    }

    public static class NotificationMessage{
        public String receivers;
        public String subject;
        public String htmlMessage;

        public NotificationMessage() {
        }

        public NotificationMessage(String receivers, String subject, String htmlMessage) {
            this.receivers = receivers;
            this.subject = subject;
            this.htmlMessage = htmlMessage;
        }

        public String getReceivers() {
            return receivers;
        }

        public String getSubject() {
            return subject;
        }

        public String getHtmlMessage() {
            return htmlMessage;
        }

        public void setReceivers(String receivers) {
            this.receivers = receivers;
        }

        @Override
        public String toString() {
            return "NotificationMessage{" +
                    "receivers='" + receivers + '\'' +
                    ", subject='" + subject + '\'' +
                    ", htmlMessage='" + htmlMessage + '\'' +
                    '}';
        }
    }
}
