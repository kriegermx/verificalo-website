package mx.krieger.verificalo.website.utils;

import com.impetus.client.cassandra.common.CassandraConstants;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Esta clase se usa para obtenr una instacia del manejador de datos hacia Cassandra.
 * Creado por juanjo@krieger.mx el 29/12/16.
 */
public class DataManager {

    public static final String PERSISTENT_UNIT = "cassandra_pu";
    public static final String KEYSPACE = "verificalo";

    @PersistenceUnit
    private EntityManagerFactory emf;
    @PersistenceContext(unitName = PERSISTENT_UNIT, type = PersistenceContextType.TRANSACTION)
    private EntityManager em;

    public DataManager() {
        Map<String, String> props = new HashMap<>();
        props.put(CassandraConstants.CQL_VERSION, CassandraConstants.CQL_VERSION_3_0);
        emf = Persistence.createEntityManagerFactory(PERSISTENT_UNIT, props);
        em = emf.createEntityManager();
        em.setFlushMode(FlushModeType.COMMIT);

    }

    /**
     * Genera una isntancia del entity manager a ser usada para alamacenar datos o ejecutar consultas.
     *
     * @return el manejador de entidades.
     */
    public EntityManager getEntityManager() {
        return em;
    }

}
