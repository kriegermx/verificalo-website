package mx.krieger.verificalo.website.utils;

/**
 * Created by me on 03/02/17.
 */
public class Checkers {

    //TODO move this to test method
    /*public static void main(String[] args) {
        System.out.println(isCar("LYN3068"));
        System.out.println(isCar("LYN306A"));
        System.out.println(isCar("192UMM"));
        System.out.println(isCar("A92UMM"));
        System.out.println(isCar("aaaaa"));
        System.out.println(isCar("111111111"));
    }*/

    private String message;

    public String getMessage() {
        return message;
    }

    public boolean isValidPlate(String plateNumber) {
        return isTaxi(plateNumber) || isCar(plateNumber);
    }

    public boolean isTaxi(String plateNumber) {
        return plateNumber.matches("^(([ABLS]\\d{3})|([M][0]\\d{2})|([T][P]\\d[A-B]))\\d{2}$");
    }

    public boolean isCar(String plateNumber) {

        if(plateNumber.matches("" +
                "(([A-Z]{3})([0-9]{4}))|" +//Old Mexico
                "(([A-Z]{3})([0-9]{3})([A-Z]{1}))|"//New Mexico
        )){
            this.message="Las placas introducidas no pertenecen al Distrito Federal";
            return false;
        }else if(!plateNumber.matches(""+
                "(([0-9]{3})([A-Z]{3}))|" +//Old Mexico City
                "(([A-Z]{1})([0-9]{2})([A-Z]{3}))"//New Mexico City
        )){
            this.message="Las placas introducidad no son válidas";
            return false;
        }

        return true;
    }

}
