package mx.krieger.verificalo.website.model.daos;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Esta es una metaclase para todas las clases que representan entidades a ser almacenadas en la base de datos.
 * Creado por juanjo@krieger.mx el 28/12/16.
 */
@Entity
public abstract class PersistentEntity {
    public static enum Status {
        ACTIVE,
        INACTIVE
    }

    @Id
    Long id;
    @Column
    Date creationDate;
    @Column
    Status status;

    public PersistentEntity() {
        Random random = new Random();
        this.id = Calendar.getInstance().getTimeInMillis() + random.nextInt(1000) + random.nextInt(100) + random
                .nextInt(10);
        this.status = Status.ACTIVE;
        this.creationDate = Calendar.getInstance().getTime();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PersistentEntity{" +
                "id=" + id +
                ", creationDate=" + creationDate +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof PersistentEntity)) return false;

        PersistentEntity that = (PersistentEntity) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(creationDate, that.creationDate)
                .append(status, that.status)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(creationDate)
                .append(status)
                .toHashCode();
    }
}
