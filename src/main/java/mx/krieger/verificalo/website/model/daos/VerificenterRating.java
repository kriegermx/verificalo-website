package mx.krieger.verificalo.website.model.daos;

import com.impetus.kundera.index.Index;
import com.impetus.kundera.index.IndexCollection;
import mx.krieger.verificalo.website.utils.DataManager;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by me on 03/01/17.
 */
@Entity
@Table(schema = DataManager.KEYSPACE + "@" + DataManager.PERSISTENT_UNIT)
@IndexCollection(columns = { @Index(name = "verificenterId")})
public class VerificenterRating extends PersistentEntity {
    @Column
    private String verificenterId;
    @Column
    private float rating;
    @Column
    private int ratingsCount;
    @Column
    private long ratingsSum;

    public VerificenterRating() {
    }

    public VerificenterRating(float rating) {
        this.rating = rating;
    }

    public VerificenterRating(String verificenterId, float rating, int ratingsCount, long ratingsSum) {
        this.verificenterId = verificenterId;
        this.rating = rating;
        this.ratingsCount = ratingsCount;
        this.ratingsSum = ratingsSum;
    }

    public String getVerificenterId() {
        return verificenterId;
    }

    public void setVerificenterId(String verificenterId) {
        this.verificenterId = verificenterId;
    }

    public float getRating() {
        return rating;
    }

    public int getPrettyRating(){
        return (int) Math.ceil(rating);
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getRatingsCount() {
        return ratingsCount;
    }

    public void setRatingsCount(int ratingsCount) {
        this.ratingsCount = ratingsCount;
    }

    public long getRatingsSum() {
        return ratingsSum;
    }

    public void setRatingsSum(long ratingsSum) {
        this.ratingsSum = ratingsSum;
    }

    @Override
    public String toString() {
        return "VerificenterRating{" +
                "verificenterId='" + verificenterId + '\'' +
                ", rating=" + rating +
                ", ratingsCount=" + ratingsCount +
                ", ratingsSum=" + ratingsSum +
                "} " + super.toString();
    }
}
