package mx.krieger.verificalo.website.model.daos;

import com.impetus.kundera.index.Index;
import com.impetus.kundera.index.IndexCollection;
import mx.krieger.verificalo.website.apis.services.NotificationServices;
import mx.krieger.verificalo.website.exceptions.InvalidPlateException;
import mx.krieger.verificalo.website.utils.DataManager;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Esta clase se utiliza para almacenar las notificaciones que serán enviadas a los usuarios.
 * Creado por juanjo@krieger.mx el 28/12/16.
 */
@Entity
@Table(schema = DataManager.KEYSPACE+"@"+DataManager.PERSISTENT_UNIT)
@IndexCollection(columns = { @Index(name = "plate"), @Index(name = "email"), @Index(name="notificationType")})
public class Notifications extends PersistentEntity {


    /**
     * Este enum especifica los tipos de notificaciones disponibles
     */
    public enum Type {
        HNC,
        PTV,
        VPV,
        PCA,
        ALL;
    }

    @Column
    private String plate;
    @Column
    private Type notificationType;
    @Column
    private String email;
    @Column
    private int ending;
    @Column
    private String hologram;

    public Notifications() {
        super();
    }

    public Notifications(NotificationServices.Notification notification, String hologram) throws InvalidPlateException {
        super();
        String plateNumber = notification.getPlateNumber();
        this.plate = plateNumber;
        this.notificationType = notification.getType();
        this.email = notification.getEmail();
        this.hologram = hologram;
        try{
            if(Character.isDigit(plateNumber.charAt(plateNumber.length()-1)))
                this.ending = Integer.parseInt(""+plateNumber.charAt(plateNumber.length()-1));
            else
                this.ending = Integer.parseInt(""+plateNumber.charAt(2));
        }catch (NumberFormatException e){
            throw new InvalidPlateException("No se puede encontrar el dígito de la tercera posición de su placa: "+plateNumber);
        }
    }

    public Notifications(String plate, Type type, String email, String hologram) {
        super();
        this.plate = plate;
        this.notificationType = type;
        this.email = email;
        this.hologram = hologram;
    }

    public String getPlate() {
        return plate;
    }

    public Type getNotificationType() {
        return notificationType;
    }

    public String getEmail() {
        return email;
    }

    public int getEnding() {
        return ending;
    }

    public String getHologram() {
        return hologram;
    }

    @Override
    public String toString() {
        return "Notifications{" +
                "plate='" + plate + '\'' +
                ", notificationType=" + notificationType +
                ", email='" + email + '\'' +
                ", ending=" + ending +
                ", gummed='" + hologram + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Notifications)) return false;

        Notifications that = (Notifications) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(plate, that.plate)
                .append(email, that.email)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(plate)
                .append(email)
                .toHashCode();
    }
}
