package mx.krieger.verificalo.website.model.daos;

import com.impetus.kundera.index.Index;
import com.impetus.kundera.index.IndexCollection;
import mx.krieger.verificalo.website.apis.services.VerificenterServices;
import mx.krieger.verificalo.website.utils.DataManager;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Esta clase contiene los datos que serán almacenados para cada retroalimentación de un verificentro.
 * creado por juanjo@krieger.mx el 28/12/16.
 */
@Entity
@Table(schema = DataManager.KEYSPACE + "@" + DataManager.PERSISTENT_UNIT)
@IndexCollection(columns = { @Index(name = "verificenterId"), @Index(name="plate")})
public class VerificenterFeedback extends PersistentEntity {

    @Column
    private String verificenterId;
    @Column
    private String comment;
    @Column
    private int rating;
    @Column
    private String plate;

    public VerificenterFeedback() {
        super();
    }

    public VerificenterFeedback(String verificenterId, String comment, int rating) {
        super();
        this.verificenterId = verificenterId;
        this.comment = comment;
        this.rating = rating;
    }

    public VerificenterFeedback(VerificenterServices.Feedback feedback) {
        super();
        this.comment = feedback.getFeedback();
        this.rating = feedback.getRating();
        this.plate = feedback.getPlate();
        this.verificenterId = feedback.getVerificenterId();
    }

    public String getVerificenterId() {
        return verificenterId;
    }

    public void setVerificenterId(String verificenterId) {
        this.verificenterId = verificenterId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    @Override
    public String toString() {
        return "VerificenterFeedback{" +
                "verificenterId='" + verificenterId + '\'' +
                ", comment='" + comment + '\'' +
                ", rating=" + rating +
                ", plate='" + plate + '\'' +
                '}';
    }
}
