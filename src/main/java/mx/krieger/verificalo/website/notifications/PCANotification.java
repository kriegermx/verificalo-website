package mx.krieger.verificalo.website.notifications;

import mx.krieger.verificalo.website.apis.clients.Verificentros;
import mx.krieger.verificalo.website.exceptions.VerificentersAPIException;
import mx.krieger.verificalo.website.model.daos.Notifications;
import org.quartz.ScheduleBuilder;

import java.util.ArrayList;
import java.util.HashMap;

import static org.quartz.CronScheduleBuilder.dailyAtHourAndMinute;

/**
 * Created by me on 12/02/17.
 */
public class PCANotification extends Notification {

    public static final String PCA_JOB_NAME = "HNCJob";
    public static final String PCA_TRIGGER_NAME = "PCATrigger";
    public static final String TEMPLATE_NUMBER = "4";


    @Override
    public String getTriggerName() {
        return PCA_TRIGGER_NAME;
    }

    @Override
    public String getJobName() {
        return PCA_JOB_NAME;
    }

    @Override
    public ScheduleBuilder<?> getScheduleBuilder() {
        return dailyAtHourAndMinute(20, 30);
    }

    @Override
    public Class<? extends NotificationJob> getJobClass() {
        return PCAJob.class;
    }


    public static class PCAJob extends Notification.NotificationJob {

        @Override
        public String getUrl() {
            return "http://201.116.58.187/mail/publicidad.php?eml=&plc=&fs=&tp=" + TEMPLATE_NUMBER + "&apikey=" +
                    API_KEY;
        }

        @Override
        public ArrayList<HashMap<String, String>> buildNotifications() {

            ArrayList<HashMap<String, String>> notifications = new ArrayList<>();
            try {
                Verificentros.Measurements contigencies = new Verificentros().getContingencies();
                if (contigencies.getMeasurements().getPca().equals("1")) {
                    String stage = contigencies.getMeasurements().getTitle();
                    notifications = processReceivers(getReceivers(Notifications.Type.PCA, null), stage);
                }
            } catch (VerificentersAPIException e) {
                e.printStackTrace();
            }
            return notifications;
        }

        private ArrayList<HashMap<String, String>> processReceivers(ArrayList<Notifications> receivers, String fase) {

            ArrayList<HashMap<String, String>> result = new ArrayList<>();
            HashMap parameters;

            for (Notifications notification : receivers) {
                parameters = new HashMap();
                parameters.put("eml", notification.getEmail());
                parameters.put("plc", notification.getPlate());
                parameters.put("fs", fase);
                result.add(parameters);
            }

            return result;
        }

    }
}
