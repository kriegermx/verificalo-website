package mx.krieger.verificalo.website.notifications;

import com.google.common.collect.ImmutableMap;
import mx.krieger.verificalo.website.model.daos.Notifications;
import org.quartz.ScheduleBuilder;

import java.util.*;

import static org.quartz.CronScheduleBuilder.dailyAtHourAndMinute;

/**
 * Created by me on 12/02/17.
 */
public class HNCNotification extends Notification {

    public static final String HNC_JOB_NAME = "HNCJob";
    public static final String HNC_TRIGGER_NAME = "HNCTrigger";
    public static final String TEMPLATE_NUMBER = "1";

    static final Map<Integer, Integer[]> HNC_CALENDAR = ImmutableMap.<Integer, Integer[]>builder()
            .put(Calendar.MONDAY, new Integer[]{5, 6})
            .put(Calendar.TUESDAY, new Integer[]{7, 8})
            .put(Calendar.WEDNESDAY, new Integer[]{3, 4})
            .put(Calendar.THURSDAY, new Integer[]{1, 2})
            .put(Calendar.FRIDAY, new Integer[]{9, 0})
            .build();

    public static final String HOLOGRAMA_CERO = "CERO";
    public static final String HOLOGRAMA_DOBLE_CERO = "DOBLE CERO";
    public static final String HOLOGRAMA_UNO = "UNO";
    public static final String HOLOGRAMA_DOS = "DOS";


    @Override
    public String getTriggerName() {
        return HNC_TRIGGER_NAME;
    }

    @Override
    public String getJobName() {
        return HNC_JOB_NAME;
    }

    @Override
    public ScheduleBuilder<?> getScheduleBuilder() {
        return dailyAtHourAndMinute(4, 30);
    }


    @Override
    public Class<? extends NotificationJob> getJobClass() {
        return HNCJob.class;
    }


    public static class HNCJob extends NotificationJob {

        @Override
        public String getUrl() {
            return "http://201.116.58.187/mail/publicidad.php?eml=&plc=&tp=" + TEMPLATE_NUMBER + "&apikey=" +
                    API_KEY;
        }

        @Override
        public ArrayList<HashMap<String, String>> buildNotifications() {

            ArrayList<HashMap<String, String>> notifications;
            LinkedHashSet<Notifications> receivers = new LinkedHashSet<>();

            int dayOfTheWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

            switch (dayOfTheWeek) {
                case Calendar.SUNDAY:
                    break;
                case Calendar.SATURDAY:
                    receivers = saturdayReceivers(dayOfTheWeek);
                    break;
                default:
                    receivers = getWeekDayReceivers(dayOfTheWeek);
                    break;
            }

            System.out.println("Receivers for day " + dayOfTheWeek + ": " + Arrays.toString(receivers.toArray()));
            notifications = processReceivers(receivers);

            return notifications;
        }

        private LinkedHashSet<Notifications> saturdayReceivers(int dayOfTheWeek) {
            LinkedHashSet<Notifications> receivers = new LinkedHashSet<>();
            ArrayList<String> filters;

            int[] endings;

            if (Calendar.getInstance().get(Calendar.DAY_OF_WEEK_IN_MONTH) % 2 == 0) {
                endings = new int[]{1, 3, 5, 7, 9};
            } else {
                endings = new int[]{0, 2, 4, 6, 8};
            }

            for (int ending : endings) {
                filters = new ArrayList<>();
                filters.add("\"hologram\"='" + HOLOGRAMA_UNO + "'");
                filters.add("\"ending\"=" + ending);
                receivers.addAll(getReceivers(Notifications.Type.HNC, filters));
            }

            filters = new ArrayList<>();
            filters.add("\"hologram\"='" + HOLOGRAMA_DOS + "'");
            receivers.addAll(getReceivers(Notifications.Type.HNC, filters));

            return receivers;
        }

        private LinkedHashSet<Notifications> getWeekDayReceivers(int dayOfTheWeek) {
            LinkedHashSet<Notifications> receivers = new LinkedHashSet<>();
            ArrayList<String> filters;

            filters = getWeekdayFilters(HOLOGRAMA_UNO, dayOfTheWeek, 0);
            receivers.addAll(getReceivers(Notifications.Type.HNC, filters));

            filters = getWeekdayFilters(HOLOGRAMA_UNO, dayOfTheWeek, 1);
            receivers.addAll(getReceivers(Notifications.Type.HNC, filters));

            filters = getWeekdayFilters(HOLOGRAMA_DOS, dayOfTheWeek, 0);
            receivers.addAll(getReceivers(Notifications.Type.HNC, filters));

            filters = getWeekdayFilters(HOLOGRAMA_DOS, dayOfTheWeek, 1);
            receivers.addAll(getReceivers(Notifications.Type.HNC, filters));

            return receivers;
        }

        private ArrayList<String> getWeekdayFilters(String hologram, int dayOfTheWeek, int endingIndex) {
            ArrayList<String> filters = new ArrayList<>();
            filters.add("\"ending\"=" + HNC_CALENDAR.get(dayOfTheWeek)[endingIndex]);
            filters.add("\"hologram\"='" + hologram + "'");
            return filters;
        }

        private ArrayList<HashMap<String, String>> processReceivers(LinkedHashSet<Notifications> receivers) {

            ArrayList<HashMap<String, String>> result = new ArrayList<>();
            HashMap parameters;

            for (Notifications notification : receivers) {
                parameters = new HashMap();
                parameters.put("eml", notification.getEmail());
                parameters.put("plc", notification.getPlate());
                result.add(parameters);
            }

            return result;
        }


    }
}
