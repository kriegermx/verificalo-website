package mx.krieger.verificalo.website.notifications;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import mx.krieger.verificalo.website.model.daos.Notifications;
import mx.krieger.verificalo.website.model.daos.PersistentEntity;
import mx.krieger.verificalo.website.utils.DataManager;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by me on 12/02/17.
 */
public abstract class Notification extends HttpServlet {

    public static final String ACTION_PARAM = "action";
    public static final String START_ACTION = "start";
    public static final String STOP_ACTION = "stop";
    public static final String NOTIFICATIONS_PREFIX = "verificalo-notification";
    public static final String NOTIFICATIONS_JOB_GROUP = NOTIFICATIONS_PREFIX + "-jobs";
    public static final String NOTIFICATIONS_TRIGGER_GROUP = NOTIFICATIONS_PREFIX + "-triggers";
    protected static final String API_KEY = "6d637fce456e8928719f2bc188eeeb87";

    public abstract String getTriggerName();

    public abstract String getJobName();

    public abstract ScheduleBuilder<?> getScheduleBuilder();

    public abstract Class<? extends NotificationJob> getJobClass();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String actionRequested = request.getParameter(ACTION_PARAM);
        System.out.println("PCA Servlet action received: " + actionRequested);
        String message = "";
        switch (actionRequested) {
            case START_ACTION:
                startCron();
                message = "Se ha inicializado las notifications "+getJobClass().getSimpleName();
                break;
            case STOP_ACTION:
                stopCron();
                message = "Se ha detenido las notificaciones "+getJobClass().getSimpleName();
                break;
            default:
                break;
        }

        response.setContentType("text/html");
        response.getWriter().println("<h1>" + message + "</h1>");
    }

    private Trigger getTrigger() {
        return newTrigger()
                .withIdentity(getTriggerName(), NOTIFICATIONS_TRIGGER_GROUP)
                .startNow()
                .withSchedule(getScheduleBuilder())
                .build();
    }


    private JobDetail getJobDetail() {
        return newJob(getJobClass())
                .withIdentity(getJobName(), NOTIFICATIONS_JOB_GROUP)
                .build();
    }

    protected void startCron() throws IOException {
        try {
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(getJobDetail(), getTrigger());
        } catch (Exception de) {
            throw new IOException(de.getMessage());
        }
    }

    protected void stopCron() {
        try {
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.deleteJob(jobKey(getJobName(), NOTIFICATIONS_JOB_GROUP));
            scheduler.shutdown(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static abstract class NotificationJob implements org.quartz.Job {

        private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

        public abstract String getUrl();

        public abstract ArrayList<HashMap<String, String>> buildNotifications();

        /**
         * Este método se ejecuta cuando el job se incializa por el trigger
         * @param jobExecutionContext
         * @throws JobExecutionException
         */
        @Override
        public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
            System.out.println("Executing cron job " + this.getClass().getCanonicalName());

            for (HashMap<String, String> notification : buildNotifications())
                callNotificationService(notification);

        }

        /**
         * Este método es el cliente que se comunica con los servicios de notificaciones de locatel
         * @param params los parámetros que se usarán para comunicarse con el servicio de mensajes
         */
        protected void callNotificationService(HashMap<String, String> params) {
            HttpRequestFactory requestFactory =
                    HTTP_TRANSPORT.createRequestFactory(request -> {
                    });

            HttpRequest request = null;
            try {
                request = requestFactory.buildGetRequest(new GenericUrl(buildUrl(getUrl(), params)));
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpResponse httpResponse = null;
            try {
                httpResponse = request.execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String response = null;
            try {
                response = httpResponse.parseAsString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(response);

        }

        /**
         * Este método llena los parámetros que se pasarán en la URL
         * @param url
         * @param params
         * @return
         */
        private String buildUrl(String url, HashMap<String, String> params) {
            for (Map.Entry<String, String> param : params.entrySet()) {
                url = url.replace(param.getKey() + "=", param.getKey() + "=" + param.getValue());
            }
            System.out.println("URL built: " + url);
            return url;
        }


        /**
         * Este método ontiene los usuarios que serán notificados de la base de datos.
         *
         * @param notificationType El tipo de notificación que será recibido
         * @param conditions Las conditiones que se ocuparán para filtrar las notificaciones de la base de datos.
         * @return La lista de Notifications que recibirán la notificación
         */
        public ArrayList<Notifications> getReceivers(Notifications.Type notificationType, ArrayList<String> conditions) {
            ArrayList<Notifications> notifications;
            DataManager dataManager = new DataManager();
            EntityManager entityManager = dataManager.getEntityManager();
            String stringQuery = getQueryString(notificationType, conditions);
            System.out.println("Query to be executed: "+stringQuery);
            Query query = entityManager.createNativeQuery(stringQuery);
            notifications = new ArrayList<>(query.getResultList());
            return notifications;
        }

        /**
         * Este método crea un string con el query a ser reutilizado para obtener los usuarios que serán notificados
         * de la base de datos. Filtros adicionales pueden ser incorporados usando los campos de la clase Notifications.
         *
         * @param notificationType El tipo de notificación que será recibido.
         * @param conditions un arreglo de condiciones de tipo r.plate='ABC123' etc
         * @return El string con el query para filtrar las notificaciones dentro de la base de datos.
         */
        private String getQueryString(Notifications.Type notificationType, ArrayList<String> conditions) {
            StringBuilder stringQuery = new StringBuilder();
            stringQuery.append("SELECT * FROM \"Notifications\" ");
            stringQuery.append("WHERE \"status\"='").append(PersistentEntity.Status.ACTIVE).append("'");
            stringQuery.append(" AND \"notificationType\"='").append(notificationType).append("'");
            if (conditions != null) {
                for (String condition : conditions) {
                    stringQuery.append(" AND ").append(condition);
                }
            }
            stringQuery.append(" ALLOW FILTERING");
            return stringQuery.toString();
        }


    }

}
