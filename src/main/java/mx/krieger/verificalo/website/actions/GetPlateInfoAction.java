package mx.krieger.verificalo.website.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import mx.krieger.verificalo.website.apis.clients.GetEnvironmentVerifications;
import mx.krieger.verificalo.website.apis.clients.GetOwnershipTaxDebts;
import mx.krieger.verificalo.website.apis.clients.GetTrafficTickets;
import mx.krieger.verificalo.website.apis.services.VerificenterServices;
import mx.krieger.verificalo.website.exceptions.PlatesAPIException;
import mx.krieger.verificalo.website.utils.Checkers;
import mx.krieger.verificalo.website.utils.VerificaloLogger;

import java.text.CharacterIterator;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Esta clase de acción es responsable de recibir peticiones para obtener información de una placa, y de llenar los
 * campos del jsp que muestra la información del auto.
 * Esta clase llena los datos del auto, las multas y las tenencias.
 * Creado por juanjo@krieger.mx el 21/12/16.
 */
public class GetPlateInfoAction extends ActionSupport {

    public static final String NO_DISPONIBLE = "No disponible";
    private String message;
    private String plateNumber;
    private CarDetails carDetails;
    private ArrayList<Verification> verifications;
    private ArrayList<Ticket> tickets;
    private Taxes taxes;
    private boolean isTaxi;

    /**
     * Este método es el encargado de llenar los campos de la clase de acción que se usan para llenar el jsp que
     * muestra la información del auto.
     * La número de placa es obligatorio, y se llena a través del método {@link #setPlateNumber(String)}.
     * Si hay algún error al procesar la firma, los datos del error serán accesibles a través del método
     * {@link #getMessage()}.
     *
     * @return SUCESS si todos los datos se han lleanado con éxito, y se ridirige al jsp de información. De otro modo
     * regresa INPUT, que se redirije al jsp principal.
     */
    public String execute() {
        this.plateNumber = getPlateNumber().toUpperCase();

        VerificaloLogger.getLogger().debug("Received plate: " + getPlateNumber());
        Checkers checkers = new Checkers();

        if (!checkers.isValidPlate(getPlateNumber())) {
            return INPUT;
        }

        //TODO @c use this code to accesss the contingencies information
        /*
        try {
            Verificentros.Measurements contingencies = new Verificentros().getContingencies();
        } catch (VerificentersAPIException e) {
            e.printStackTrace();
        }
        */

        VerificaloLogger.getLogger().debug("Checking if car is taxi");
        this.isTaxi = checkers.isTaxi(getPlateNumber());

        try {
            VerificaloLogger.getLogger().debug("Getting verifications");
            this.verifications = obtainVerifications();
            VerificaloLogger.getLogger().debug("Getting tickets");
            this.tickets = obtainTickets();
            VerificaloLogger.getLogger().debug("Getting taxes");
            this.taxes = obtainTaxes();
        } catch (PlatesAPIException e) {
            this.message = e.getMessage();
            return INPUT;
        }

        return SUCCESS;
    }

    private ArrayList<Verification> obtainVerifications() throws PlatesAPIException {
        ArrayList<Verification> result = new ArrayList<>();


        GetEnvironmentVerifications.Response verifications;
        try {
            verifications = new GetEnvironmentVerifications().getResponse
                    (getPlateNumber());
        } catch (PlatesAPIException e) {
            this.carDetails = new CarDetails(NO_DISPONIBLE, NO_DISPONIBLE, NO_DISPONIBLE, NO_DISPONIBLE, NO_DISPONIBLE);
            throw new PlatesAPIException("No fue posible obtener la información de su vehículo. " + e.getMessage());
        }

        if (verifications == null || verifications.getExternalServerResponse().equals("404") || verifications
                .getVerificationList().isEmpty()) {
            return result;
        }

        GetEnvironmentVerifications.VerificationItem carEntry = verifications.getVerificationList().get(0);
        for (GetEnvironmentVerifications.VerificationItem entry : verifications.getVerificationList()) {
            result.add(new Verification(entry));
        }

        this.carDetails = new CarDetails(carEntry);
        return result;

    }


    private ArrayList<Ticket> obtainTickets() throws PlatesAPIException {
        ArrayList<Ticket> result = new ArrayList<>();
        try {
            GetTrafficTickets.Response tickets = new GetTrafficTickets().getResponse(getPlateNumber());
            if (tickets.getTicketList() != null) {
                for (GetTrafficTickets.TicketElement element : tickets.getTicketList()) {
                    result.add(new Ticket(element));
                }
            }
            return result;
        } catch (PlatesAPIException e) {
            throw new PlatesAPIException("No fue posible obtener la información de sus multas. " + e.getMessage());
        }
    }

    private Taxes obtainTaxes() throws PlatesAPIException {
        try {
            return new Taxes(new GetOwnershipTaxDebts().getResponse(getPlateNumber()));
        } catch (PlatesAPIException e) {
            throw new PlatesAPIException("No fue posible obtener la información de su tenencia. " + e.getMessage());
        }
    }

    /**
     * Este revisaa cada una de las verificaciones del auto, e dentifica aquellas que pueden ser comentadas.
     * @param verifications la lista de verificaciones regresas para el auto en el método obtainVerifications
     * @return La lista de los ids de los verificentros que tienen comentarios pendientes.
     */
    private ArrayList<String> getPendingComments(ArrayList<Verification> verifications){
        ArrayList<String> pendingVerificenters = new ArrayList<>();
        for(Verification verification:verifications){
            if(canBeCommented(verification)){
                pendingVerificenters.add(verification.getVerificenter());
            }
        }
        return pendingVerificenters;
    }

    private boolean canBeCommented(Verification verification) {

        VerificenterServices fs = new VerificenterServices();
        try {
            if (!fs.previousCommentExist(getPlateNumber(), verification.getVerificenter()) && fs.validCommentPeriod(verification.getVerificationDate())) {
                return true;
            }
        } catch (ParseException e) {
            return false;
        }
        return false;
    }

    /**
     * Método que llena el número de placa a ser procesado.
     *
     * @param plateNumber
     */
    @RequiredFieldValidator(message = "Por favor introduzca la placa del auto que desea verificar.")
    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    /**
     * @return el número de placa
     */
    public String getPlateNumber() {
        return plateNumber;
    }

    /**
     * Este método se utiliza para obtener el mensaje de error en caso de que exista alguno.
     *
     * @return el mensaje de error.
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return los detalles del auto.
     */
    public CarDetails getCarDetails() {
        return carDetails;
    }

    /**
     * @return si el auto es taxi
     */
    public boolean getIsTaxi() {
        return isTaxi;
    }

    /**
     * @return la lista de verificaciones
     */
    public ArrayList<Verification> getVerifications() {
        return verifications;
    }

    /**
     * @return los detalles de las multas.
     */
    public ArrayList<Ticket> getTickets() {
        return tickets;
    }

    /**
     * @return los detalles de la tenencia.
     */
    public Taxes getTaxes() {
        return taxes;
    }

    private enum Colours {
        green,
        red,
        yellow,
        pink,
        blue
    }

    Colours[] gummedColours = new Colours[]{Colours.blue, Colours.green, Colours.green, Colours.red, Colours.red,
            Colours.yellow, Colours.yellow, Colours.pink, Colours.pink, Colours.blue};

    /**
     * Esta clase modela los datos del auto.
     */
    public class CarDetails {

        private String model;
        private String serialNumber;
        private String hologram;
        private String gummed;
        private String hologramParity;

        public CarDetails(String model, String serialNumber, String hologram, String gummed, String hologramParity) {
            this.model = model;
            this.serialNumber = serialNumber;
            this.hologram = hologram;
            this.gummed = gummed;
            this.hologramParity = hologramParity;
        }

        public CarDetails(GetEnvironmentVerifications.VerificationItem carEntry) {
            VerificaloLogger.getLogger().debug("Car info to be wrapped: " + carEntry.toString());

            this.model = String.format("%s %s, %s", carEntry.getBrand(), carEntry.getSubbrand(), carEntry.getModel
                    ()).replace("_", " ");
            this.serialNumber = carEntry.getVin();
            this.hologram = carEntry.getResult();
            String plate = carEntry.getPlate();
            this.gummed = gummedColours[Integer.parseInt(String.valueOf(plate.charAt(plate.length() - 4)))].name();

            if(Character.isDigit(plate.charAt(plate.length()-1))){
                this.hologramParity = (((Integer.parseInt(String.valueOf(plate.charAt(plate.length()-1)))) % 2) == 0) ? "even" : "odd";
            }else{
                this.hologramParity = (((Integer.parseInt(String.valueOf(plate.charAt(plate.length() - 4)))) % 2) == 0) ? "even" : "odd";
            }

        }

        public String getModel() {
            return model;
        }

        public String getSerialNumber() {
            return serialNumber;
        }

        public String getHologram() {
            return hologram;
        }

        public String getGummed() {
            return gummed;
        }

        public String getHologramParity() {
            return hologramParity;
        }

    }

    public class Verification {

        private String result;
        private String validity;
        private String rejectionCause;
        private String verificationDate;
        private String cancelled;
        private String verificationTime;
        private String verificenter;
        private String GDFteam;
        private String line;
        private String certificade;

        public Verification() {
        }

        public Verification(GetEnvironmentVerifications.VerificationItem entry) {
            this.result = entry.getResult();
            this.validity = entry.getValidity();
            this.rejectionCause = entry.getRejectionCause();
            this.verificationDate = entry.getVerificationDate();
            this.cancelled = entry.getCancelled();
            this.verificationTime = entry.getVerificationTime();
            this.verificenter = entry.getVerificenter();
            this.GDFteam = entry.getGDFteam();
            this.line = entry.getLine();
            this.certificade = entry.getCertificade();
        }

        public String getResult() {
            return result;
        }

        public String getValidity() {
            return validity;
        }

        public String getRejectionCause() {
            return rejectionCause;
        }

        public String getVerificationDate() {
            return verificationDate;
        }

        public String getCancelled() {
            return cancelled;
        }

        public String getVerificationTime() {
            return verificationTime;
        }

        public String getVerificenter() {
            return verificenter;
        }

        public String getGDFteam() {
            return GDFteam;
        }

        public String getLine() {
            return line;
        }

        public String getCertificade() {
            return certificade;
        }

        @Override
        public String toString() {
            return "Verification{" +
                    "result='" + result + '\'' +
                    ", validity='" + validity + '\'' +
                    ", rejectionCause='" + rejectionCause + '\'' +
                    ", verificationDate='" + verificationDate + '\'' +
                    ", cancelled='" + cancelled + '\'' +
                    ", verificationTime='" + verificationTime + '\'' +
                    ", verificenter='" + verificenter + '\'' +
                    ", GDFteam='" + GDFteam + '\'' +
                    ", line='" + line + '\'' +
                    ", certificade='" + certificade + '\'' +
                    '}';
        }
    }

    /**
     * Esta clase modela los detalles de cada multa.
     */
    public class Ticket {
        private String id;
        private String reason;
        private String article;
        private String fraction;
        private String date;
        private String penalty;

        public Ticket(GetTrafficTickets.TicketElement ticket) {
            VerificaloLogger.getLogger().debug("Ticket info to be wrapped: " + ticket.toString());
            this.id = ticket.getIdentifier();
            this.reason = ticket.getReason();
            this.article = ticket.getArticle();
            this.fraction = ticket.getFraction();
            this.date = ticket.getDate();
            this.penalty = ticket.getNumberOfSalaries() + " salarios mínimos de " + ticket.getMinimumSalary();
        }

        public String getId() {
            return id;
        }

        public String getReason() {
            return reason;
        }

        public String getArticle() {
            return article;
        }

        public String getFraction() {
            return fraction;
        }

        public String getDate() {
            return date;
        }

        public String getPenalty() {
            return penalty;
        }
    }

    /**
     * Esta clase modela los datos de la tenencia.
     */
    public class Taxes {
        private boolean hasPendingTaxes;
        private String years;
        private int numberOfPendingYears;

        public Taxes(GetOwnershipTaxDebts.Response taxes) {
            VerificaloLogger.getLogger().debug("Tax info to be wrapped: " + taxes.toString());
            this.hasPendingTaxes = taxes.getHasPendingPayments().equals("1");
            if (this.hasPendingTaxes) {
                this.years = taxes.getPendingPayments().replace(",", ", ");
                numberOfPendingYears = taxes.getPendingPayments().split(",").length;
            }

        }

        public boolean isHasPendingTaxes() {
            return hasPendingTaxes;
        }

        public String getYears() {
            return years;
        }

        public int getNumberOfPendingYears() {
            return numberOfPendingYears;
        }
    }
}

