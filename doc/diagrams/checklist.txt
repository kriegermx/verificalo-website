(Faltante)- Entregar documentación técnica de las pruebas unitarias y de integración , realizadas mediante una matriz de pruebas que el LICITANTE deberá proponer y la cual deberá ser aprobada por el   área responsable del proyecto de la SEDEMA.

- Entregar el código fuente de la totalidad del software desarrollado, incluyendo librerías complementarias.

(Faltante)- Entregar memoria técnica del despliegue de la aplicación web en el servidor de la Secretaría.

(Faltante)- Documentación que de fe de las pruebas de funcionalidad.

- Documentación del sistema Verifícalo
 * Diagramas de secuencia para cada uno de los procesos.
 * Diagramas de flujo y diagramas de casos de uso.
 * Diagramas entidad relación para los procesos involucrados.
 (Faltante)* Estructura de la base de datos y diccionario de datos.
 * Diagrama de clases.

- Javadoc
