#!/usr/bin/env bash

echo *************************************
echo "Installing Wildfly..."
echo *************************************

echo =====================================
echo "Setting up environment..."
echo =====================================
INSTALLATION_DIR=/opt
WILDFLY_DIR=$INSTALLATION_DIR/wildfly-10.1.0.Final
FILES_DIR=/shared/provisioning/files/wildfly
echo INSTALLATION_DIR = $INSTALLATION_DIR
echo WILDFLY_DIR = $WILDFLY_DIR
echo Wildfly user = ubuntu password = no password

echo =====================================
echo "Downloading wildfly..."
echo =====================================
cd $INSTALLATION_DIR
sudo wget 'http://download.jboss.org/wildfly/10.1.0.Final/wildfly-10.1.0.Final.zip'

echo =====================================
echo "Installing wildfly..."
echo =====================================
sudo unzip 'wildfly-10.1.0.Final'
sudo ln -s $WILDFLY_DIR wildfly
sudo chown -R ubuntu $INSTALLATION_DIR/wildfly*
sudo cp -rf $FILES_DIR/standalone.xml $WILDFLY_DIR/standalone/configuration/standalone.xml
sudo $WILDFLY_DIR/bin/add-user.sh -u ubuntu -p server_pa55

#echo =====================================
#echo "Starting wildfly..."
#echo =====================================
#sudo /opt/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0 &
#echo "Wildfly is running."
#echo "Admin Console: http://localhost:9191"
#echo "Http: http://localhost.9090"

echo =====================================
echo "Adding wildfly to startup scripts"
echo =====================================
sudo cp -r $FILES_DIR/rc.local /etc

echo *************************************
echo "Installing Wildfly DONE!!!"
echo *************************************
