#!/usr/bin/env bash

echo *************************************
echo "Installing Dependencies..."
echo *************************************

echo =====================================
echo "Updating libraries..."
echo =====================================
sudo apt-get update
sudo apt-get upgrade -y

echo =====================================
echo "Installing unzip"
echo =====================================
sudo apt-get install -y unzip

echo =====================================
echo "Installing Java"
echo =====================================
if which java >/dev/null; then
   	echo "skip java 8 installation"
else
	echo "java 8 installation"
	apt-get install --yes python-software-properties
	add-apt-repository ppa:webupd8team/java
	apt-get update -qq
	echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
	echo debconf shared/accepted-oracle-license-v1-1 seen true | /usr/bin/debconf-set-selections
	apt-get install --yes oracle-java8-installer
	yes "" | apt-get -f install
fi

echo *************************************
echo "Installing Dependencies DONE!!!"
echo *************************************