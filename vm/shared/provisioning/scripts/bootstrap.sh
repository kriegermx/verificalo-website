#!/usr/bin/env bash

echo *************************************
echo "Provisioning VM..."
echo *************************************
SCRIPTS_DIR=/shared/provisioning/scripts
sudo chmod +x $SCRIPTS_DIR/*.sh
tr -d '\r' < $SCRIPTS_DIR/dependencies.sh > $SCRIPTS_DIR/dependencies_encoded.sh
tr -d '\r' < $SCRIPTS_DIR/wildfly.sh > $SCRIPTS_DIR/wildfly_encoded.sh
tr -d '\r' < $SCRIPTS_DIR/cassandra.sh > $SCRIPTS_DIR/cassandra_encoded.sh

echo =====================================
echo "Provisioning server and database..."
echo =====================================
sudo $SCRIPTS_DIR/dependencies_encoded.sh
sudo $SCRIPTS_DIR/wildfly_encoded.sh
sudo $SCRIPTS_DIR/cassandra.sh

echo *************************************
echo "Provisioning VM DONE!!!"
echo *************************************
