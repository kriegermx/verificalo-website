#!/usr/bin/env bash

if netstat -an | grep 9090 | grep LISTEN ; then
    echo "Running..."
else
    sudo /opt/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0 &
    echo "Started"
fi


