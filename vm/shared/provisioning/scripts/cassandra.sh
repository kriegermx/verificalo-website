#!/usr/bin/env bash

echo *************************************
echo "Installing Cassandra..."
echo *************************************

sudo rm /etc/apt/sources.list.d/*.disable
curl -L http://debian.datastax.com/debian/repo_key | sudo apt-key add -
echo "deb http://debian.datastax.com/community stable main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list
sudo apt-get update
sudo apt-get install -y dsc22 cassandra
sudo apt-get install -y cassandra-tools
sudo cp -r /shared/provisioning/files/cassandra/cassandra.yaml /etc/cassandra/

echo *************************************
echo "Installing Cassandra DONE!!!"
echo *************************************